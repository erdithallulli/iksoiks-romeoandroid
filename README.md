# Romeo Android
Progetto IntelliJ IDEA/Android Studio.

## Packages
`it.audio_guida.romeo`:

- `.activities`: Activity, Fragments e relativi oggetti per la UI.
- `.data`: Gestione database interno, ContentProvider, Contratti per i dati.
- `.media`: Gestione multimediale, per ora solo Audio.
- `.model`: Funzioni statiche utili per gestire alcuni dati.
- `.network`: Funzioni di rete, usando la libreria Android Volley.
- `.sync`: SyncAdapter, fake Authenticator. Sincronizzazione dei dati.
- `.tour_guide`: Service per la gestione della guida turistica.
- `.view_libs`: Classi UI, la maggior parte prese da internet.

### Note
- Nel package `.sync` era stato avviato lo sviluppo della sincronizzazione delle città offline.
- La classe `it.audio_guida.romeo.tour_guide.TourGuideService` è scritta a strati (uso di classi astratte).
