package it.audio_guida.romeo.tour_guide;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import it.audio_guida.romeo.ITourGuideInterface;
import it.audio_guida.romeo.media.GenericAudioPlayer;

public class TourGuideService extends AbstractLogTourGuideService {

    public static final String INTENT_ACTION = TourGuideService.class.getCanonicalName() + ".message";

    public static final String ARG_CARD_NAME = "cardName";
    public static final String ARG_CARD_ID = "cardId";
    public static final String ARG_MESSAGE = "message";
    public static final String ARG_DATA = "data";
    public static final String ARG_AUDIO_DURATION = "audio_duration";

    public static final String MESSAGE_VISITING_CITY = "visiting";
    public static final String MESSAGE_NOT_VISITING_CITY = "not_visiting";
    public static final String MESSAGE_NEAR = "near";
    public static final String MESSAGE_NOT_NEAR = "not_near";
    public static final String MESSAGE_AUDIO_READY = "audio_ready";
    public static final String MESSAGE_AUDIO_NOT_READY = "audio_not_ready";
    public static final String MESSAGE_PLAYING = "playing";
    public static final String MESSAGE_PAUSED = "paused";
    public static final String MESSAGE_STOPPED = "stopped";
    public static final String MESSAGE_SEEK = "seek";
    public static final String MESSAGE_FINISHED = "finished";

    private final ITourGuideInterface.Stub mBinder = new MyTourGuideServiceBinder(this);
    private LocalBroadcastManager mBroadcaster;


    @Override
    public void onCreate() {
        super.onCreate();
        mBroadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onLocationFinderStart() {
        super.onLocationFinderStart();
        Intent intent1 = new Intent(INTENT_ACTION);
        intent1.putExtra(ARG_MESSAGE, MESSAGE_VISITING_CITY);
        intent1.putExtra(ARG_CITY_NAME, getVisitingCityName());
        mBroadcaster.sendBroadcast(intent1);
    }

    @Override
    public void onLocationFinderStop() {
        super.onLocationFinderStop();
        Intent intent1 = new Intent(INTENT_ACTION);
        intent1.putExtra(ARG_MESSAGE, MESSAGE_NOT_VISITING_CITY);
        mBroadcaster.sendBroadcast(intent1);
    }

    @Override
    protected void onCardNotNear() {
        super.onCardNotNear();
        Intent intent = new Intent(INTENT_ACTION);
        intent.putExtra(ARG_MESSAGE, MESSAGE_NOT_NEAR);
        mBroadcaster.sendBroadcast(intent);
    }

    @Override
    protected void onCardSingleLoaded(int id, String name, String city, String textFileName, String audioFileName) {
        super.onCardSingleLoaded(id, name, city, textFileName, audioFileName);

        if (getNearCardId() == id) {
            Intent intent = new Intent(INTENT_ACTION);
            intent.putExtra(ARG_MESSAGE, MESSAGE_NEAR);
            intent.putExtra(ARG_CARD_ID, id);
            intent.putExtra(ARG_CARD_NAME, name);
            mBroadcaster.sendBroadcast(intent);
        }
    }

    @Override
    public void onAudioPlayerReady() {
        super.onAudioPlayerReady();

        Intent i = new Intent(INTENT_ACTION);
        i.putExtra(ARG_MESSAGE, MESSAGE_AUDIO_READY);
        i.putExtra(ARG_CARD_ID, getCardAudioLoadedId());
        i.putExtra(ARG_CARD_NAME, getCardAudioLoadedName());
        i.putExtra(ARG_AUDIO_DURATION, getAudioDurationMSec());
        mBroadcaster.sendBroadcast(i);
    }

    @Override
    public void onAudioPlayerStatusChange(int status, int data) {
        super.onAudioPlayerStatusChange(status, data);
        Intent i = new Intent(INTENT_ACTION);
        switch (status) {
            case GenericAudioPlayer.STATUS_PLAYING:
                i.putExtra(ARG_MESSAGE, MESSAGE_PLAYING);
                break;
            case GenericAudioPlayer.STATUS_PAUSED:
                i.putExtra(ARG_MESSAGE, MESSAGE_PAUSED);
                break;
            case GenericAudioPlayer.STATUS_STOPPED:
                i.putExtra(ARG_MESSAGE, MESSAGE_STOPPED);
                break;
            case GenericAudioPlayer.STATUS_SEEK:
                i.putExtra(ARG_MESSAGE, MESSAGE_SEEK);
                i.putExtra(ARG_DATA, data);
                break;
            case GenericAudioPlayer.STATUS_FINISHED:
                i.putExtra(ARG_MESSAGE, MESSAGE_FINISHED);
                break;
        }
        mBroadcaster.sendBroadcast(i);
    }

    @Override
    public void onAudioPlayerReset() {
        super.onAudioPlayerReset();
        Intent i = new Intent(INTENT_ACTION);
        i.putExtra(ARG_MESSAGE, MESSAGE_AUDIO_NOT_READY);
        i.putExtra(ARG_CARD_ID, getCardAudioLoadedId());
        i.putExtra(ARG_CARD_NAME, getCardAudioLoadedName());
        mBroadcaster.sendBroadcast(i);
    }
}

