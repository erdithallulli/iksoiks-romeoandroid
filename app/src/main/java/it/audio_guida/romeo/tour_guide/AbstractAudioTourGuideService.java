package it.audio_guida.romeo.tour_guide;

import android.app.Service;
import it.audio_guida.romeo.media.GenericAudioPlayer;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 12/08/16.
 */
public abstract class AbstractAudioTourGuideService extends Service implements GenericAudioPlayer.GenericAudioPlayerListener {

    private GenericAudioPlayer mAudioPlayer;

    @Override
    public void onCreate() {
        mAudioPlayer = new GenericAudioPlayer(this, this);
    }

    @Override
    public void onDestroy() {
        mAudioPlayer.close();
        super.onDestroy();
    }

    @Override
    public void onAudioPlayerReady() {

    }

    @Override
    public void onAudioPlayerStatusChange(int status, int data) {

    }

    @Override
    public void onAudioPlayerReset() {

    }

    protected GenericAudioPlayer getAudioPlayer() {
        return mAudioPlayer;
    }

    void playCardAudio() {
        mAudioPlayer.play();
    }

    void pauseCardAudio() {
        mAudioPlayer.pause();
    }

    boolean isAudioReady() {
        return mAudioPlayer.isReady();
    }

    boolean isPlaying() {
        return mAudioPlayer.isPlaying();
    }

    int getAudioDurationMSec() {
        return mAudioPlayer.getAudioDurationMSec();
    }

    void seekCardAudio(int msec) {
        mAudioPlayer.seekTo(msec);
    }

    int getCurrentPositionMSec() {
        return mAudioPlayer.getCurrentPositionMSec();
    }
}
