package it.audio_guida.romeo.tour_guide;

import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.model.Card;

import java.io.IOException;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 12/08/16.
 */
public abstract class AbstractCardLoaderTourGuideService extends AbstractAudioTourGuideService implements Loader.OnLoadCompleteListener<Cursor> {

    public static final int LOADER_CARD_LIST = 0;
    public static final int LOADER_CARD_SINGLE = 1;

    private int mLoadingAudioCardId;
    private boolean mMustPlayAudioAfterIsLoaded;
    private String mLoadingAudioCardName;

    /**
     * Load card asynchronously and eventually play audio
     *
     * @param cardId        the card id
     * @param audioAutoLoad true if i have to load audio after loading the card
     * @param audioAutoPlay true if i have to play audio after loading the card
     */
    protected void loadCardAsync(int cardId, boolean audioAutoLoad, boolean audioAutoPlay) {
        if (audioAutoLoad) {
            mLoadingAudioCardId = cardId;
            mMustPlayAudioAfterIsLoaded = audioAutoPlay;
        }
        CursorLoader cursorLoader = new CursorLoader(this,
                ContentUris.withAppendedId(CardContract.URI, cardId),
                new String[]{
                        CardContract.Entry._ID,
                        CardContract.Entry.COL_NAME,
                        CardContract.Entry.COL_CITY,
                        CardContract.Entry.COL_TEXT,
                        CardContract.Entry.COL_AUDIO,
                }, "", null, null);
        cursorLoader.registerListener(LOADER_CARD_SINGLE, this);
        cursorLoader.startLoading();
    }

    protected void loadCardListAsync(String cityName) {
        CursorLoader cursorLoader = new CursorLoader(this,
                CardContract.URI,
                new String[]{
                        CardContract.Entry._ID,
                        CardContract.Entry.COL_LAT,
                        CardContract.Entry.COL_LNG,
                        CardContract.Entry.COL_RADIUS,
                }, CardContract.Entry.COL_CITY + " = ?",
                new String[]{cityName}, null);
        cursorLoader.registerListener(LOADER_CARD_LIST, this);
        cursorLoader.startLoading();
    }

    @Override
    public void onLoadComplete(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_CARD_LIST:
                data.setNotificationUri(getContentResolver(), CardContract.URI);
                onCardListLoaded(data);
                break;
            case LOADER_CARD_SINGLE:
                if (data.moveToFirst()) onCardSingleLoaded(data);
                break;
        }
    }

    private void onCardSingleLoaded(Cursor data) {
        int id = data.getInt(data.getColumnIndex(CardContract.Entry._ID));
        String name = data.getString(data.getColumnIndex(CardContract.Entry.COL_NAME));
        String city = data.getString(data.getColumnIndex(CardContract.Entry.COL_CITY));
        String textFileName = data.getString(data.getColumnIndex(CardContract.Entry.COL_TEXT));
        String audioFileName = data.getString(data.getColumnIndex(CardContract.Entry.COL_AUDIO));

        onCardSingleLoaded(id, name, city, textFileName, audioFileName);
    }

    // TODO check
    protected void onCardSingleLoaded(int id, String name, String city, String textFileName, String audioFileName) {
        if (mLoadingAudioCardId == id) {
            mLoadingAudioCardName = name;

            Cursor query = getContentResolver().query(CardContract.URI, new String[]{CardContract.Entry._ID,
                    CardContract.Entry.COL_DATA_EXISTS},
                    CardContract.Entry._ID +" = ?", new String[]{Integer.toString(id)}, null);
            String dataExists = "";
            if(query != null) {
                query.moveToFirst();
                int index = query.getColumnIndex(CardContract.Entry.COL_DATA_EXISTS);
                dataExists = query.getString(index);
            }

            int flagPosition = 0;
            boolean audioExists = false;
            boolean textExists = false;
            if(dataExists!=null || !dataExists.isEmpty())
                for(int i=0; i<dataExists.length(); ++i){
                    if(dataExists.charAt(i)=='0'||dataExists.charAt(i)=='1'){
                        flagPosition++;
                        if(flagPosition==1 && dataExists.charAt(i)=='1')
                            audioExists = true;
                        else if(flagPosition==2 && dataExists.charAt(i)=='1')
                            textExists = true;
                    }
                }


            //Log.i("durata audio",Integer.toString(getAudioPlayer().getAudioDurationMSec()));
            //if (audioFileName.isEmpty() || (getAudioDurationMSec() == -1)) getAudioPlayer().loadSpeech(city, textFileName);
            if(!audioExists) getAudioPlayer().loadSpeech(city, textFileName, textExists);
            else try {
                getAudioPlayer().loadAudioUri(Uri.parse(Card.getAudioUrl(city, audioFileName)));
            } catch (IOException ignored) {
            }
        }
    }

    protected abstract void onCardListLoaded(Cursor data);

    @Override
    public void onAudioPlayerReady() {
        super.onAudioPlayerReady();

        if (mMustPlayAudioAfterIsLoaded) playCardAudio();
    }


    public int getCardAudioLoadedId() {
        return mLoadingAudioCardId;
    }


    public String getCardAudioLoadedName() {
        return mLoadingAudioCardName;
    }
}
