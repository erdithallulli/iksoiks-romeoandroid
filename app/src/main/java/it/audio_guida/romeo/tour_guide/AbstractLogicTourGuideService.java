package it.audio_guida.romeo.tour_guide;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.card_view.CardViewActivity;
import it.audio_guida.romeo.activities.city_summary.CitySummaryActivity;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.media.GenericAudioPlayer;
import it.audio_guida.romeo.model.City;

import org.apache.commons.lang3.text.WordUtils;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 12/08/16.
 */
public abstract class AbstractLogicTourGuideService extends AbstractLocalizatorTourGuideService {
    private String mActuallyNearCardName;
    private int mFocusedPlayerId = 0;
    private int mFocusedCardid = 0;
    private boolean inVisita = false;

    @Override
    protected void onCardNear(int cardId) {
        super.onCardNear(cardId);

        loadCardAsync(cardId, true, true);
    }

    @Override
    protected void onCardSingleLoaded(int id, String name, String city, String textFileName, String audioFileName) {
        super.onCardSingleLoaded(id, name, city, textFileName, audioFileName);

        if (getNearCardId() == id) {
            setNotificationForeground(id, name);
            mActuallyNearCardName = name;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        Double[] coordinate = getCoordinate();
        double latitude = location.getLatitude();// coordinate[0];
        double longitude = location.getLongitude(); //coordinate[1];
        Cursor cursor = getContentResolver().query(CityContract.URI,
                new String[]{CityContract.Entry.COL_NAME},
                CityContract.Entry.COL_SO_LAT + " < " + latitude + " AND "
                        + CityContract.Entry.COL_NE_LAT + " > " + latitude + " AND "
                        + CityContract.Entry.COL_SO_LON + " < " + longitude + " AND "
                        + CityContract.Entry.COL_NE_LON + " > " + longitude,
                null, null);
        boolean inVisita = false;
        if(cursor != null && (cursor.getCount()>0)){
            cursor.moveToFirst();
            String city = cursor.getString(cursor.getColumnIndex(CityContract.Entry.COL_NAME));
            city = city.toLowerCase();
            if(city.equals(getVisitingCityName().toLowerCase())){
                setNotificationForeground(getVisitingCityName(), true);
                inVisita = true;
            }
        }
        if(!inVisita)
            setNotificationForeground(getVisitingCityName(), false);
    }

    @Override
    protected void startLocationFinder(String cityName) {
        super.startLocationFinder(cityName);

        Double[] coordinate = getCoordinate();
        double latitude = coordinate[0];
        double longitude = coordinate[1];

        Cursor cursor = getContentResolver().query(CityContract.URI,
                new String[]{CityContract.Entry.COL_NAME},
                CityContract.Entry.COL_SO_LAT + " < " + latitude + " AND "
                        + CityContract.Entry.COL_NE_LAT + " > " + latitude + " AND "
                        + CityContract.Entry.COL_SO_LON + " < " + longitude + " AND "
                        + CityContract.Entry.COL_NE_LON + " > " + longitude,
                null, null);
        boolean inVisita = false;
        if(cursor != null && (cursor.getCount()>0)){
            cursor.moveToFirst();
            String city = cursor.getString(cursor.getColumnIndex(CityContract.Entry.COL_NAME));
            city = city.toLowerCase();
            if(city.equals(getVisitingCityName().toLowerCase())){
                setNotificationForeground(getVisitingCityName(), true);
                inVisita = true;
            }
        }
        if(!inVisita)
            setNotificationForeground(cityName, false);
    }

    private void setNotificationForeground(String cityName, boolean inVisita) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_explore);
        builder.setColor(getResources().getColor(R.color.colorPrimary));
        Intent intent;
        if(inVisita) {
            builder.setContentTitle(WordUtils.capitalizeFully(cityName) + " in visita, Audio Guida Attiva");
            Double[] coordinate = getCoordinate();
            intent = CitySummaryActivity.getLauncherIntent(this, cityName, true, coordinate[0], coordinate[1]);
            builder.setContentText(getResources().getString(R.string.visiting_help_not_near));
            builder.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0));
            Notification notification = builder.build();
            startForeground(1, notification);
        }
        /*
        else{
            builder.setContentTitle(WordUtils.capitalizeFully(cityName) + " in consultazione");
            intent = CitySummaryActivity.getLauncherIntent(this, getVisitingCityName(), false, 0.0, 0.0);
            builder.setContentText("");
        }

        //builder.setContentText(getResources().getString(R.string.visiting_help_not_near));

        builder.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0));
        Notification notification = builder.build();
        startForeground(1, notification);
        */
    }

    private void setNotificationForeground(int cardId, String cardName) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_explore);
        builder.setColor(getResources().getColor(R.color.colorPrimary));
        builder.setContentTitle(cardName);
        builder.setContentText(getResources().getString(R.string.visiting_help_near_card));

        Intent resultIntent = new Intent(this, CardViewActivity.class);
        resultIntent.putExtra(CardViewActivity.ARG_ID, cardId);
        //Intent resultIntent = CitySummaryActivity.getLauncherIntent(this, getVisitingCityName());

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(CitySummaryActivity.class);
        stackBuilder.addNextIntent(CitySummaryActivity.getLauncherIntent(this, getVisitingCityName(), false, 0.0, 0.0));
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        Notification notification = builder.build();
        startForeground(1, notification);
    }

    String getNearCardName() {
        return mActuallyNearCardName;
    }

    @Override
    public void onAudioPlayerStatusChange(int status, int data) {
        super.onAudioPlayerStatusChange(status, data);
        switch (status) {
            case GenericAudioPlayer.STATUS_PLAYING:
                pauseLocationFinder();
                break;
            case GenericAudioPlayer.STATUS_PAUSED: // TODO remove paused
            case GenericAudioPlayer.STATUS_STOPPED:
                unpauseLocationFinder();
                break;
        }
    }

    public void onFocusAudioPlayer(int cardId, int playerId) {
        Log.i("AUDIO", "focused " + cardId + " on " + playerId);
        if (getCardAudioLoadedId() != cardId) {
            pauseCardAudio();
        }
        mFocusedPlayerId = playerId;
        mFocusedCardid = cardId;
    }

    public void onBlurAudioPlayer(int playerId) {
        Log.i("AUDIO", "blurred on " + playerId);
        if (mFocusedPlayerId == playerId) {
            mFocusedPlayerId = 0;
            pauseCardAudio();
        }
    }
}
