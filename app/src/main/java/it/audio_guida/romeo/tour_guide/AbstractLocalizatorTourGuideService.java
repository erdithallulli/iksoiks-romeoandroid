package it.audio_guida.romeo.tour_guide;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.media.GenericAudioPlayer;
import it.audio_guida.romeo.runtime_info.RuntimeInfo;

import java.util.HashSet;
import java.util.Set;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 12/08/16.
 */
@SuppressWarnings("Duplicates")
public abstract class AbstractLocalizatorTourGuideService extends AbstractCardLoaderTourGuideService implements LocationFinder.LocationListener {

    public static final String ARG_CITY_NAME = "city_name";
    public static final String WAKELOCK_TAG = "TourGuidePalmipedoWakeLock";
    private int[] mCardIds;
    private float[] mCardLats;
    private float[] mCardLons;
    private int[] mCardRadius;
    private Set<Integer> mListenedCardIds = new HashSet<>(20);

    private boolean mVisitingStarted;
    private boolean mVisitingPaused;

    private String mVisitingCityName;
    private LocationFinder mLocationFinder;
    private int mActuallyNearCardId;
    private PowerManager.WakeLock mWakeLock;

    private double lastLat;
    private double lastLon;

    private static float distanceBetween(double latA, double lonA, double latB, double lonB) {
        float[] results = new float[1];
        Location.distanceBetween(latA, lonA, latB, lonB, results);
        return results[0];
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationFinder = new LocationFinder(this, this);
        mLocationFinder.start();
        Location location = mLocationFinder.getLocation();
        if(location != null){
            lastLat = location.getLatitude();
            lastLon = location.getLongitude();
        }
        else {
            lastLat = 500.0;
            lastLat = 500.0;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationFinder();
        mLocationFinder = null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String cityName = intent.getStringExtra(ARG_CITY_NAME);
        if (!mVisitingStarted) {
            startLocationFinder(cityName);
        } else if (!cityName.equals(mVisitingCityName)) {
            stopLocationFinder();
            startLocationFinder(cityName);
        }
        return Service.START_NOT_STICKY;
    }

    protected void startLocationFinder(String cityName) {
        mVisitingStarted = true;
        mVisitingPaused = false;
        mVisitingCityName = cityName;
        loadCardListAsync(mVisitingCityName);
        //mLocationFinder.start();
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_TAG);
        mWakeLock.acquire();

    }

    protected void pauseLocationFinder() {
        if (mVisitingStarted) mLocationFinder.stop();
        mVisitingPaused = true;
    }

    protected void unpauseLocationFinder() {
        if (mVisitingStarted && mVisitingPaused) mLocationFinder.start();
        mVisitingPaused = false;

    }

    protected void stopLocationFinder() {
        mLocationFinder.stop();
        mVisitingStarted = false;
        mVisitingPaused = false;
        mVisitingCityName = null;
        mWakeLock.release();
    }

    protected void onCardListLoaded(Cursor data) {
        int count = data.getCount();

        mCardIds = new int[count];
        mCardLats = new float[count];
        mCardLons = new float[count];
        mCardRadius = new int[count];

        if (count == 0) return;

        int idColumnIndex = data.getColumnIndex(CardContract.Entry._ID);
        int latColumnIndex = data.getColumnIndex(CardContract.Entry.COL_LAT);
        int lonColumnIndex = data.getColumnIndex(CardContract.Entry.COL_LNG);
        int radiusColumnIndex = data.getColumnIndex(CardContract.Entry.COL_RADIUS);

        int i = 0;
        data.moveToPosition(-1);
        while (data.moveToNext()) {
            mCardIds[i] = data.getInt(idColumnIndex);
            mCardLats[i] = data.getFloat(latColumnIndex);
            mCardLons[i] = data.getFloat(lonColumnIndex);
            mCardRadius[i] = data.getInt(radiusColumnIndex);
            i++;
        }
        Location location = new Location("");
        location.setLatitude(lastLat);
        location.setLongitude(lastLon);

        /*Context context = getApplicationContext();
        CharSequence text = "Controllo Schede vicinanze";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();*/
        int cardNearest = getCardNearest(location);

        /*
        text = "Trovata scheda numero: "+cardNearest;
        toast = Toast.makeText(context, text, duration);
        toast.show();*/
        if (cardNearest == -1) onCardNotNear();
        else if (cardNearest > 0) onCardNear(cardNearest);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mCardIds == null) return;

        int cardNearest = getCardNearest(location);
        if (cardNearest == -1) onCardNotNear();
        else if (cardNearest > 0) onCardNear(cardNearest);
    }

    /**
     * Get the id of card nearest
     *
     * @param location the user current position
     * @return -1 if not near, -2 if not changes, otherwise the cardId;
     */
    protected int getCardNearest(Location location) {
        double cLat = location.getLatitude();
        double cLon = location.getLongitude();
        lastLat = cLat;
        lastLon = cLon;
        RuntimeInfo runtimeInfo = RuntimeInfo.getInstance();
        int count = mCardIds.length;
        int radiusMin = Integer.MAX_VALUE;
        int nearestCardId = -1;
        for (int i = 0; i < count; i++) {
            //if (isCardListened(mCardIds[i])) return -1; //se sta ascoltando qualche scheda salta
            if (isCardListened(mCardIds[i]) ||
                    runtimeInfo.isPresent(mCardIds[i])) continue; // If is already listened


            float d = distanceBetween(cLat, cLon, mCardLats[i], mCardLons[i]);
            if (d < mCardRadius[i]) {
                if (mCardRadius[i] < radiusMin) {
                    radiusMin = mCardRadius[i];
                    nearestCardId = mCardIds[i];
                }
            }
        }
        if (mActuallyNearCardId != nearestCardId) {
            mActuallyNearCardId = nearestCardId;

            if(!runtimeInfo.isPresent(mActuallyNearCardId) || runtimeInfo.isItineraryMode()) {
                runtimeInfo.addComune(mActuallyNearCardId);
                return nearestCardId;
            }
            else
                return -2;  //scheda già visitata
        }

        return -2;
    }

    protected Double[] getCoordinate(){
        return new Double[]{lastLat, lastLon};
    }

    protected void onCardNear(int cardId) {
    }

    protected void onCardNotNear() {
    }

    @Override
    public void onLocationFinderStart() {

    }

    @Override
    public void onLocationFinderStop() {
    }

    @Override
    public void onLocationFinderFailed() {

    }

    boolean isVisitingCity() {
        return mVisitingStarted;
    }

    String getVisitingCityName() {
        return mVisitingCityName;
    }

    boolean isNearACard() {
        return mActuallyNearCardId != -1;
    }

    int getNearCardId() {
        return mActuallyNearCardId;
    }

    private boolean isCardListened(int cardId) {
        return mListenedCardIds.contains(cardId);
    }

    private boolean addCardListened(int cardId) {
        return mListenedCardIds.add(cardId);
    }

    @Override
    public void onAudioPlayerStatusChange(int status, int data) {
        super.onAudioPlayerStatusChange(status, data);
        switch (status) {
            case GenericAudioPlayer.STATUS_FINISHED:
                int cardId = getCardAudioLoadedId();
                addCardListened(cardId);
        }
    }
}
