package it.audio_guida.romeo.tour_guide;

import android.os.RemoteException;
import android.util.Log;

import it.audio_guida.romeo.ITourGuideInterface;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 10/08/16.
 */
class MyTourGuideServiceBinder extends ITourGuideInterface.Stub {

    private TourGuideService tourGuideService;

    public MyTourGuideServiceBinder(TourGuideService tourGuideService) {
        this.tourGuideService = tourGuideService;
    }

    @Override
    public void pauseTourGuide() throws RemoteException {
        tourGuideService.pauseLocationFinder();
    }

    @Override
    public void unpauseTourGuide() throws RemoteException {
        tourGuideService.unpauseLocationFinder();
    }

    @Override
    public boolean isVisitingCity() throws RemoteException {
        return tourGuideService.isVisitingCity();
    }

    @Override
    public String getVisitingCityName() throws RemoteException {
        return tourGuideService.getVisitingCityName();
    }

    @Override
    public boolean isNearACard() throws RemoteException {
        return tourGuideService.isNearACard();
    }

    @Override
    public String getNearCardName() throws RemoteException {
        return tourGuideService.getNearCardName();
    }

    @Override
    public void onFocusAudioPlayer(int cardId, int playerId) throws RemoteException {
        tourGuideService.onFocusAudioPlayer(cardId, playerId);
    }

    @Override
    public void onBlurAudioPlayer(int playerId) throws RemoteException {
        tourGuideService.onBlurAudioPlayer(playerId);
    }

    @Override
    public int getNearCardId() throws RemoteException {
        return tourGuideService.getNearCardId();
    }

    @Override
    public void loadCardAudio(int cardId, boolean autoPlay) throws RemoteException {
        tourGuideService.loadCardAsync(cardId, true, autoPlay);
    }

    @Override
    public int getCardAudioLoadedId() throws RemoteException {
        return tourGuideService.getCardAudioLoadedId();
    }

    @Override
    public String getCardAudioLoadedName() throws RemoteException {
        return tourGuideService.getCardAudioLoadedName();
    }

    @Override
    public void playCardAudio() throws RemoteException {
        tourGuideService.playCardAudio();
    }

    @Override
    public void pauseCardAudio() throws RemoteException {
        tourGuideService.pauseCardAudio();
    }

    @Override
    public void seekCardAudio(int msec) throws RemoteException {
        tourGuideService.seekCardAudio(msec);
    }

    @Override
    public int getAudioDurationMSec() throws RemoteException {
        return tourGuideService.getAudioDurationMSec();
    }

    @Override
    public int getCurrentPositionMSec() throws RemoteException {
        return tourGuideService.getCurrentPositionMSec();
    }

    @Override
    public boolean isPlaying() throws RemoteException {
        return tourGuideService.isPlaying();
    }

    @Override
    public boolean isAudioReady() throws RemoteException {
        return tourGuideService.isAudioReady();
    }
}
