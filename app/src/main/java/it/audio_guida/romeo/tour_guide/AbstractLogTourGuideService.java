package it.audio_guida.romeo.tour_guide;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Location;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.network.ServerRequests;
import it.audio_guida.romeo.runtime_info.RuntimeInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Locale;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 25/08/16.
 */
public abstract class AbstractLogTourGuideService extends AbstractLogicTourGuideService {
    private static final String TAG = "Server Logging";
    private int mUserId;

    private long timelastUpdate = 0;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int message = intent.getIntExtra(DataConstants.ARG_MESSAGE, -1);
            String operazione = null;
            switch (message) {
                case DataConstants.MESSAGE_LOG_IN:
                    sendLogAnonymousLogin(getVisitingCityName());
                    return;
                case DataConstants.MESSAGE_OPENED_CARD:
                    operazione = "SCHEDA";
                    break;
                case DataConstants.MESSAGE_AUDIO_CARD:
                    operazione = "BRANO AUDIO";
                    break;
                case DataConstants.MESSAGE_START_VIDEO:
                    operazione = "VIDEO SCHEDA AVVIO";
                    break;
                case DataConstants.MESSAGE_OPENED_VIDEO:
                    operazione = "VIDEO SCHEDA CLICK";
                    break;
                case DataConstants.MESSAGE_AUDIO_CARD_AUTO:
                    operazione = "BRANO SCHEDA AUTO";
                    break;
                case DataConstants.MESSAGE_OPENED_CARD_PREVIEW:
                    operazione = "SCHEDA PREVIEW";
                    break;
                case DataConstants.MESSAGE_AUDIO_CARD_PREVIEW:
                    operazione = "BRANO SCHEDA PREVIEW";
                    break;
                case DataConstants.MASSAGE_SEARCH:
                    operazione = "SEARCH";
                    sendLogSearch(intent.getStringExtra(DataConstants.ARG_AUDIO_NAME),
                            intent.getStringExtra(DataConstants.ARG_CITY_NAME),
                            operazione);

                    return;
            }
            if(operazione != null)
                sendLogOnFullCardOpen(operazione, intent.getIntExtra(DataConstants.ARG_ID, -1),
                    intent.getIntExtra(DataConstants.ARG_COD_CAT, 0),
                    intent.getIntExtra(DataConstants.ARG_COD_SER, 0),
                    intent.getStringExtra(DataConstants.ARG_CITY_NAME),
                    intent.getStringExtra(DataConstants.ARG_AUDIO_NAME),
                    intent.getBooleanExtra(DataConstants.ARG_IS_ITINERARY, false)
                );
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,
                new IntentFilter(DataConstants.INTENT_ACTION_EVENT));
        String comune = intent.getExtras().getString(TourGuideService.ARG_CITY_NAME, null);
        sendLogAnonymousLogin(comune);
         timelastUpdate = 0;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    //todo vedere se si può evitare il doppio controllo (qui e nel metodo della superclasse) aggiungendo una variabile o un metodo che faccia il controllo
    @Override
    public void onLocationChanged(Location location){
        double latitude = location.getLatitude();// coordinate[0];
        double longitude = location.getLongitude(); //coordinate[1];
        Cursor cursor = getContentResolver().query(CityContract.URI,
                new String[]{CityContract.Entry.COL_NAME},
                CityContract.Entry.COL_SO_LAT + " < " + latitude + " AND "
                        + CityContract.Entry.COL_NE_LAT + " > " + latitude + " AND "
                        + CityContract.Entry.COL_SO_LON + " < " + longitude + " AND "
                        + CityContract.Entry.COL_NE_LON + " > " + longitude,
                null, null);
        boolean inVisita = false;
        if(cursor != null && (cursor.getCount()>0)){
            cursor.moveToFirst();
            String city = cursor.getString(cursor.getColumnIndex(CityContract.Entry.COL_NAME));
            city = city.toLowerCase();
            if(city!=null && getVisitingCityName()!=null && city.equals(getVisitingCityName().toLowerCase())){
                inVisita = true;
            }
        }
        if(inVisita) {
            long actualTime = (new Date()).getTime();
            if ((actualTime - timelastUpdate) > 30000) {
                ServerRequests.sendPosition(new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }, mUserId, 1, location.getLatitude(), location.getLongitude());
                timelastUpdate = actualTime;
            }
        }
        super.onLocationChanged(location);
    }

    @Override
    public void onLocationFinderStart() {
        super.onLocationFinderStart();
        /*String lastcity = RuntimeInfo.getInstance().getLastCity();
        String currentCity = getVisitingCityName();
        if((lastcity != null)&&(!lastcity.equals(currentCity))) {
            RuntimeInfo.getInstance().setLastCity(currentCity);
            sendLogAnonymousLogin(getVisitingCityName());
        }
        */
    }

    public void sendLogAnonymousLogin(String comune) {
        String liveId = "NO LIVE ID";
        String deviceId = getDeviceId();
        String nomeApp = "Palmipedo Android";
        PackageInfo pInfo = null;
        String version = "2.1.6";
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String versionePrg = version;
        String terminaleTipo = "Android";
        int terminaleFirmware = Build.VERSION.SDK_INT;
        String linguaSistema = Resources.getSystem().getConfiguration().locale.getLanguage();
        String linguaSelezionata = Locale.getDefault().getLanguage();
        String lastDataOra = (new Date()).toString();
        Log.i("Server Logging", "Anonymous login...");
        ServerRequests.anonymousLogin(new Response.Listener<JSONObject>() {
                                          @Override
                                          public void onResponse(JSONObject response) {
                                              try {
                                                  mUserId = response.getInt("_id");
                                                  Log.i(TAG, "Anonymous login: mUserId = " + mUserId);
                                              } catch (JSONException e) {
                                                  e.printStackTrace();
                                              }
                                          }
                                      }, new Response.ErrorListener() {
                                          @Override
                                          public void onErrorResponse(VolleyError error) {
                                              error.printStackTrace();
                                          }
                                      }, liveId, deviceId, nomeApp, false, versionePrg, comune, terminaleTipo,
                terminaleFirmware, linguaSistema, linguaSelezionata, lastDataOra, "0", "0");
    }

    private String getDeviceId() {
        return android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
    }

    void sendLogOnFullCardOpen(String operazione, int codiceScheda, int catID, int serviceID, String cityName, String audioName, boolean isItinerary) {
        timelastUpdate = (new Date()).getTime();
        if (isItinerary) {
            Double[] coordinate = getCoordinate();

            ServerRequests.sendLog(new Response.Listener<JSONObject>() {
                                       @Override
                                       public void onResponse(JSONObject response) {
                                           Log.i(TAG, "Log send. Response: " + response);
                                       }
                                   }, new Response.ErrorListener() {
                                       @Override
                                       public void onErrorResponse(VolleyError error) {
                                           error.printStackTrace();
                                       }
                                   }, mUserId, Double.toString(coordinate[0]), Double.toString(coordinate[1]), "0", catID,
                    cityName, 0, 0, "infoFile=" + audioName, operazione);

        } else {
            int lengthCat = Integer.toString(catID).length();
            int lengthService = Integer.toString(serviceID).length();
            int service = serviceID - (catID * (int) (Math.pow(10, lengthService - lengthCat)));
            Double[] coordinate = getCoordinate();
            //String operazione = "Click Scheda Completa";
            ServerRequests.sendLog(new Response.Listener<JSONObject>() {
                                       @Override
                                       public void onResponse(JSONObject response) {
                                           Log.i(TAG, "Log send. Response: " + response);
                                       }
                                   }, new Response.ErrorListener() {
                                       @Override
                                       public void onErrorResponse(VolleyError error) {
                                           error.printStackTrace();
                                       }
                                   }, mUserId, Double.toString(coordinate[0]), Double.toString(coordinate[1]), "0", catID,
                    cityName, service, codiceScheda, "infoFile=" + audioName, operazione);
        }
    }

    private void sendLogSearch(String message, String cityName, String operazione){
        Log.i("sendLog","infoFile:"+message);
        Double[] coordinate = getCoordinate();
        ServerRequests.sendLog(new Response.Listener<JSONObject>() {
                                   @Override
                                   public void onResponse(JSONObject response) {
                                   }
                               }, new Response.ErrorListener() {
                                   @Override
                                   public void onErrorResponse(VolleyError error) {
                                       error.printStackTrace();
                                   }
                               }, mUserId, Double.toString(coordinate[0]), Double.toString(coordinate[1]), "0", 0,
                cityName, 0, 0, "infoFile="+message, operazione);
    }
}
