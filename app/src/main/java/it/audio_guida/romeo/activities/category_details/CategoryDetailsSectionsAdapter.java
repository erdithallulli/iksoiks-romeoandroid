package it.audio_guida.romeo.activities.category_details;

import android.content.Context;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 20/08/16.
 */
public class CategoryDetailsSectionsAdapter extends FragmentPagerAdapter {

    private final int[] mSectionsTypes;
    private final Context mContext;
    private final String mCityName;
    private final long mCategoryId;
    private final Location mCurrentLocation;

    public CategoryDetailsSectionsAdapter(Context context, FragmentManager fm, String cityName, long categoryId, @Nullable Location currentLocation) {
        super(fm);
        mContext = context;
        mCityName = cityName;
        mCategoryId = categoryId;
        mCurrentLocation = currentLocation;

        if (mCurrentLocation != null && (mCategoryId!=-2 && mCategoryId != -3 && mCategoryId != -4)) {
            mSectionsTypes = new int[]{
                    //CategoryDetailsFragmentFactory.TYPE_TOP,
                    CategoryDetailsFragmentFactory.TYPE_NEAR,
                    CategoryDetailsFragmentFactory.TYPE_MOST_VISITED,
                    CategoryDetailsFragmentFactory.TYPE_MOST_LISTENED,
                    CategoryDetailsFragmentFactory.TYPE_AZ,

            };
        } else if(mCurrentLocation == null && (mCategoryId !=-2 && mCategoryId!=-3 && mCategoryId != -4)) {
            mSectionsTypes = new int[]{
                    //CategoryDetailsFragmentFactory.TYPE_TOP,
                    CategoryDetailsFragmentFactory.TYPE_MOST_VISITED,
                    CategoryDetailsFragmentFactory.TYPE_MOST_LISTENED,
                    CategoryDetailsFragmentFactory.TYPE_AZ,
            };
        }
        else if(mCategoryId == -2){
            mSectionsTypes = new int[]{
                    CategoryDetailsFragmentFactory.TYPE_ITINERARY_LIST,
                    //CategoryDetailsFragmentFactory.TYPE_ITINERARY_NODE_LIST
            };
        }
        else if(mCategoryId == -3){
            mSectionsTypes = new int[]{
                    CategoryDetailsFragmentFactory.TYPE_ITINERARY_NODE_LIST
            };
        }
        else {
            mSectionsTypes = new int[]{
                    CategoryDetailsFragmentFactory.TYPE_TOP
            };
        }
    }

    @Override
    public Fragment getItem(int position) {
        return CategoryDetailsFragmentFactory.newInstance(
                mCityName, mCategoryId, mCurrentLocation, mSectionsTypes[position]);
    }

    @Override
    public int getCount() {
        return mSectionsTypes.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return CategoryDetailsFragmentFactory.getOrderNameFromType(mContext, mSectionsTypes[position]);
    }
}
