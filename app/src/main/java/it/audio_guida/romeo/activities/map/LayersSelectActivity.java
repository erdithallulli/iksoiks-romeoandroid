package it.audio_guida.romeo.activities.map;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CardContract;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Set;

import static it.audio_guida.romeo.data.contracts.CategoryUnionServiceViewContract.Entry;
import static it.audio_guida.romeo.data.contracts.CategoryUnionServiceViewContract.URI;

public class LayersSelectActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String ARG_CITY_NAME = "cityName";
    public static final String ARG_CATEGORIES = "categories";
    public static final String ARG_RESULT_SERVICES_HIDDEN = "servicesHidden";
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private LayersCursorRecyclerViewAdapter mAdapter;
    private String mCityName;
    private int[] mCategories;
    private Set<Integer> mServicesNotSelected = new HashSet<>(30);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        mCategories = intent.getIntArrayExtra(ARG_CATEGORIES);
        mCityName = intent.getStringExtra(ARG_CITY_NAME);

        setContentView(R.layout.activity_layers_select);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new LayersCursorRecyclerViewAdapter(this, null, mServicesNotSelected);
        mRecyclerView.setAdapter(mAdapter);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyPreferences myPreferences = new MyPreferences(this);
        int[] intArray = myPreferences.getIntArray(DataConstants.PREF_KEY_SERVICES_HIDDEN, null);
        if (intArray != null) {
            mServicesNotSelected.clear();
            for (int i : intArray) mServicesNotSelected.add(i);
        }
    }

    @Override
    public void onBackPressed() {
        // Convert mServicesNotSelected to int[] and set it as Activity result.
        int[] ints = ArrayUtils.toPrimitive(mServicesNotSelected.toArray(new Integer[mServicesNotSelected.size()]));
        Intent data = new Intent();
        data.putExtra(ARG_RESULT_SERVICES_HIDDEN, ints);
        setResult(RESULT_OK, data);

        super.onBackPressed();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String subQueryS = SQLiteQueryBuilder.buildQueryString(true,
                CardContract.Entry.TABLE_NAME,
                new String[]{CardContract.Entry.COL_SERVICE_ID},
                CardContract.Entry.COL_CITY + " = ?",
                null, null, null, null);
        String subQueryC = SQLiteQueryBuilder.buildQueryString(true,
                CardContract.Entry.TABLE_NAME,
                new String[]{CardContract.Entry.COL_CAT_ID},
                CardContract.Entry.COL_CITY + " = ?",
                null, null, null, null);
        String categoriesToMap = StringUtils.join(ArrayUtils.toObject(mCategories), ", ");
        return new CursorLoader(this,
                URI,
                new String[]{
                        Entry._ID,
                        Entry.COL_CAT_ID,
                        Entry.COL_NAME,
                        Entry.COL_ICON,
                        Entry.COL_TYPE,
                }, "(" +
                "(" + Entry._ID + " IN (" + subQueryS + ") AND " + Entry.COL_TYPE + " = 2 ) OR " +
                "(" + Entry._ID + " IN (" + subQueryC + ") AND " + Entry.COL_TYPE + " = 1 )" +
                ") AND " +
                "(" + Entry.COL_CAT_ID + " IN (" + categoriesToMap + "))",
                new String[]{mCityName, mCityName},
                Entry.COL_CAT_ID + ", " + Entry._ID);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
