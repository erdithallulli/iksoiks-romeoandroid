package it.audio_guida.romeo.activities.category_details;

import org.apache.commons.lang3.ArrayUtils;

import it.audio_guida.romeo.data.contracts.CardContract;

public class CategoryDetailsFragmentTop extends AbstractCategoryDetailsFragment {

    @Override
    protected String[] getProjection() {
        if(mLocation != null) {
            String fieldCalculatedDistance = CardContract.Entry.getFieldCalculatedDistance(
                    mLocation.getLatitude(), mLocation.getLongitude());
            return ArrayUtils.add(super.getProjection(), fieldCalculatedDistance);
        }
        else return super.getProjection();
    }

    @Override
    protected String getSelection() {
        return CardContract.Entry.COL_CITY + " = ? AND " + CardContract.Entry.COL_PRIORITY + " = 1";
    }

    @Override
    protected String getSortOrder() {
        if(mLocation != null)
            return CardContract.Entry.CALCULATED_DISTANCE + " ASC";
        return CardContract.Entry.COL_NAME + " ASC";
    }
}
