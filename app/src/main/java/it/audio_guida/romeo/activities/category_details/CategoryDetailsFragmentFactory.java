package it.audio_guida.romeo.activities.category_details;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import it.audio_guida.romeo.R;

/**
 * A Factory that creates {@link AbstractCategoryDetailsFragment} based by ordering type
 * Created by christian on 20/08/16.
 */
public class CategoryDetailsFragmentFactory {

    public static final int TYPE_TOP = 0;
    public static final int TYPE_MOST_VISITED = 1;
    public static final int TYPE_MOST_LISTENED = 2;
    public static final int TYPE_AZ = 3;
    public static final int TYPE_NEAR = 4;
    public static final int TYPE_ITINERARY_LIST = 5;
    public static final int TYPE_ITINERARY_NODE_LIST = 6;

    /**
     * Create a new {@link AbstractCategoryDetailsFragment} based by ordering type
     *
     * @param cityName        the name of the city where we have to find the cards
     * @param categoryId      the id of category where we have to find the cards
     * @param currentLocation the current User Location
     * @param type            fragment type/order
     * @return a new {@link AbstractCategoryDetailsFragment}
     */
    public static AbstractCategoryDetailsFragment newInstance(
            String cityName,
            long categoryId,
            @Nullable Location currentLocation,
            int type) {
        Bundle args = new Bundle();
        args.putString(AbstractCategoryDetailsFragment.ARG_CITY_NAME, cityName);
        args.putLong(AbstractCategoryDetailsFragment.ARG_CATEGORY_ID, categoryId);
        args.putParcelable(AbstractCategoryDetailsFragment.ARG_CURRENT_LOCATION, currentLocation);

        AbstractCategoryDetailsFragment fragment;
        switch (type) {
            case TYPE_TOP:
                fragment = new CategoryDetailsFragmentTop();
                break;
            case TYPE_MOST_VISITED:
                fragment = new CategoryDetailsFragmentMostVisited();
                break;
            case TYPE_MOST_LISTENED:
                fragment = new CategoryDetailsFragmentMostListened();
                break;
            case TYPE_NEAR:
                fragment = new CategoryDetailsFragmentNear();
                break;
            case TYPE_ITINERARY_LIST:
                fragment = new CategoryDetailsFragmentItinerary();
                break;
            case TYPE_ITINERARY_NODE_LIST:
                fragment = new CategoryDetailsFragmentItineraryNode();
                break;
            case TYPE_AZ:
            default:
                fragment = new CategoryDetailsFragmentAz();
        }

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Get the title of an order type
     *
     * @param context the current Context
     * @param type    fragment type/order
     * @return the name of a type
     */
    public static String getOrderNameFromType(Context context, int type) {
        switch (type) {
            case TYPE_TOP:
                return context.getString(R.string.cards_order_top);
            case TYPE_MOST_VISITED:
                return context.getString(R.string.cards_order_most_visited);
            case TYPE_MOST_LISTENED:
                return context.getString(R.string.cards_order_most_listened);
            case TYPE_AZ:
                return context.getString(R.string.cards_order_az);
            case TYPE_NEAR:
                return context.getString(R.string.around_you);
            case TYPE_ITINERARY_LIST:
                return new String("Itinerari");
            case TYPE_ITINERARY_NODE_LIST:
                return new String("Tappe Itinerari");
        }
        return "";
    }
}
