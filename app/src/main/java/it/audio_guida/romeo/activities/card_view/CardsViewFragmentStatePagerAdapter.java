package it.audio_guida.romeo.activities.card_view;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.ItineraryNodeContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.view_libs.CursorFragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 02/08/16.
 */
class CardsViewFragmentStatePagerAdapter extends CursorFragmentStatePagerAdapter {
    public static final String PHOTOS_URLS = "_photo_urls";

    private CardViewActivity mCardViewActivity;

    public CardsViewFragmentStatePagerAdapter(Context context, FragmentManager fm, Cursor cursor) {
        super(context, fm, cursor);
        this.mCardViewActivity = (CardViewActivity) context;
    }

    @Override
    public Fragment getItem(Context context, Cursor cursor) {
        if(cursor.getColumnName(1).equals(ItineraryNodeContract.Entry.COL_NAME)){
            int cardCode = cursor.getInt(cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_SCHEDA_CODE));
            Cursor query = mContext.getContentResolver().query(CardContract.URI, new String[]{
                    CardContract.Entry._ID,
                    CardContract.Entry.COL_PHOTO_1,
                    CardContract.Entry.COL_PHOTO_2,
                    CardContract.Entry.COL_PHOTO_3,
                    CardContract.Entry.COL_CITY,
                    CardContract.Entry.COL_NAME,
                    CardContract.Entry.COL_CITY,
                    CardContract.Entry.COL_TEXT,
                    CardContract.Entry.COL_LAT,
                    CardContract.Entry.COL_LNG,
                    CardContract.Entry.COL_VIDEO
                    },  CardContract.Entry._ID +" = ?", new String[]{Integer.toString(cardCode)}, null);
            if(query != null) {
                query.moveToFirst();
                String city = query.getString(query.getColumnIndex(CardContract.Entry.COL_CITY));
                String video = query.getString(query.getColumnIndex(CardContract.Entry.COL_VIDEO));
                String photo1 = Card.getImageUrl(city, query.getString(query.getColumnIndex(CardContract.Entry.COL_PHOTO_1)));
                String photo2 = Card.getImageUrl(city, query.getString(query.getColumnIndex(CardContract.Entry.COL_PHOTO_2)));
                String photo3 = Card.getImageUrl(city, query.getString(query.getColumnIndex(CardContract.Entry.COL_PHOTO_3)));

                ArrayList<String> photoUrls = new ArrayList<>(4);
                if (video != null && !video.isEmpty()) photoUrls.add(photo1);
                if (photo1 != null) photoUrls.add(photo1);
                if (photo2 != null) photoUrls.add(photo2);
                if (photo3 != null) photoUrls.add(photo3);

                Bundle bundle = new Bundle();
                bundle.putInt(CardContract.Entry._ID, query.getInt(query.getColumnIndex(CardContract.Entry._ID)));
                bundle.putString(CardContract.Entry.COL_NAME, query.getString(query.getColumnIndex(CardContract.Entry.COL_NAME)));
                bundle.putString(CardContract.Entry.COL_CITY, city);
                bundle.putString(CardContract.Entry.COL_TEXT, query.getString(query.getColumnIndex(CardContract.Entry.COL_TEXT)));
                bundle.putDouble(CardContract.Entry.COL_LAT, query.getDouble(query.getColumnIndex(CardContract.Entry.COL_LAT)));
                bundle.putDouble(CardContract.Entry.COL_LNG, query.getDouble(query.getColumnIndex(CardContract.Entry.COL_LNG)));
                bundle.putString(CardContract.Entry.COL_VIDEO, video);
                bundle.putStringArrayList(PHOTOS_URLS, photoUrls);
                bundle.putInt(CardViewActivity.ARG_ITINERARY, 1);

                return CardViewFragment.newInstance(bundle, mCardViewActivity);
            }
        }

        String city = cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_CITY));
        String video = cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_VIDEO));
        String photo1 = Card.getImageUrl(city, cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_PHOTO_1)));
        String photo2 = Card.getImageUrl(city, cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_PHOTO_2)));
        String photo3 = Card.getImageUrl(city, cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_PHOTO_3)));

        ArrayList<String> photoUrls = new ArrayList<>(4);
        if (video != null && !video.isEmpty()) photoUrls.add(photo1);
        if (photo1 != null) photoUrls.add(photo1);
        if (photo2 != null) photoUrls.add(photo2);
        if (photo3 != null) photoUrls.add(photo3);

        String serviceName = cursor.getString(cursor.getColumnIndex(ServiceContract.Entry.COL_NAME));

        Bundle bundle = new Bundle();
        bundle.putInt(CardContract.Entry._ID, cursor.getInt(cursor.getColumnIndex(CardContract.Entry._ID)));
        bundle.putString(CardContract.Entry.COL_NAME, cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_NAME)));
        bundle.putString(CardContract.Entry.COL_CITY, city);
        bundle.putString(CardContract.Entry.COL_TEXT, cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_TEXT)));
        bundle.putDouble(CardContract.Entry.COL_LAT, cursor.getDouble(cursor.getColumnIndex(CardContract.Entry.COL_LAT)));
        bundle.putDouble(CardContract.Entry.COL_LNG, cursor.getDouble(cursor.getColumnIndex(CardContract.Entry.COL_LNG)));
        bundle.putString(CardContract.Entry.COL_VIDEO, video);
        bundle.putString(ServiceContract.Entry.COL_NAME, serviceName);
        bundle.putStringArrayList(PHOTOS_URLS, photoUrls);
        bundle.putInt(CardViewActivity.ARG_ITINERARY, 0);
        return CardViewFragment.newInstance(bundle, mCardViewActivity);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mDataValid && mCursor.moveToPosition(position)) {
            if(mCursor.getColumnName(1).equals(ItineraryNodeContract.Entry.COL_NAME)){
                int cardCode = mCursor.getInt(mCursor.getColumnIndex(ItineraryNodeContract.Entry.COL_SCHEDA_CODE));
                Cursor query = mContext.getContentResolver().query(CardContract.URI, new String[]{
                        CardContract.Entry._ID,
                        CardContract.Entry.COL_NAME},  CardContract.Entry._ID +" = ?", new String[]{Integer.toString(cardCode)}, null);
                if(query != null) {
                    query.moveToFirst();
                    return query.getString(query.getColumnIndex(CardContract.Entry.COL_NAME));
                }
            }
            return mCursor.getString(getCursor().getColumnIndex(CardContract.Entry.COL_NAME));
        } else {
            return null;
        }
    }

    public int getPageId(int position) {
        if (mDataValid && mCursor.moveToPosition(position)) {
            if(mCursor.getColumnName(1).equals(ItineraryNodeContract.Entry.COL_NAME)){
                return mCursor.getInt(mCursor.getColumnIndex(ItineraryNodeContract.Entry.COL_SCHEDA_CODE));
            }
            return mCursor.getInt(mCursor.getColumnIndex(CardContract.Entry._ID));
        } else {
            return -1;
        }
    }

}
