package it.audio_guida.romeo.activities.city_select;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import it.audio_guida.romeo.data.contracts.CityTypeContract;
import it.audio_guida.romeo.view_libs.CursorFragmentStatePagerAdapter;

/**
 * An adapter that generates {@link CitySelectFragment}s.
 * Created by christian on 02/08/16.
 */
public class CityTypesFragmentStatePagerAdapter extends CursorFragmentStatePagerAdapter {



    public CityTypesFragmentStatePagerAdapter(Context context, FragmentManager fm, Cursor cursor) {
        super(context, fm, cursor);
    }

    @Override
    public Fragment getItem(Context context, Cursor cursor) {
        int cityTypeId = cursor.getInt(cursor.getColumnIndex(CityTypeContract.Entry._ID));
        if (cityTypeId == 100) return SmartCitySelectFragment.newInstance();
        //else if (cityTypeId == 101) return SmartCitySelectFragment.newInstance();
        return CitySelectFragment.newInstance(cityTypeId);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mDataValid) {
            mCursor.moveToPosition(position);
            return mCursor.getString(getCursor().getColumnIndex(CityTypeContract.Entry.COL_NAME));
        } else {
            return null;
        }
    }
}
