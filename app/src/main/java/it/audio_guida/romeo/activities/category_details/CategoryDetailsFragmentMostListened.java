package it.audio_guida.romeo.activities.category_details;

import it.audio_guida.romeo.data.contracts.CardContract;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 21/08/16.
 */
public class CategoryDetailsFragmentMostListened extends AbstractCategoryDetailsFragment {
    @Override
    protected String getSortOrder() {
        return "("+CardContract.Entry.COL_N_LISTENS + " + "+ CardContract.Entry.COL_N_CLICKS +")" +" DESC";
    }
}
