package it.audio_guida.romeo.activities.category_details;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 20/08/16.
 */
public abstract class AbstractCategoryDetailsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, CategoryDetailsCursorRecyclerViewAdapter.OnAdapterInteractionListener {

    public static final String ARG_CITY_NAME = "city_name";
    public static final String ARG_CATEGORY_ID = "category_id";
    public static final String ARG_CURRENT_LOCATION = "c_location";
    protected Location mLocation;
    private String mCityName;
    private long mCategoryId;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private CategoryDetailsCursorRecyclerViewAdapter mAdapter;
    private OnCategoryDetailsFragmentInteractionListener mListener;

    public AbstractCategoryDetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle args = getArguments();
        mCityName = args.getString(ARG_CITY_NAME);
        mCategoryId = args.getLong(ARG_CATEGORY_ID);
        mLocation = args.getParcelable(ARG_CURRENT_LOCATION);

        View rootView = inflater.inflate(R.layout.fragment_category_details, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = onCreateCursorRecyclerViewAdapter();
        mRecyclerView.setAdapter(mAdapter);

        getLoaderManager().initLoader(0, null, this);

        return rootView;
    }

    protected CategoryDetailsCursorRecyclerViewAdapter onCreateCursorRecyclerViewAdapter() {
        return new CategoryDetailsCursorRecyclerViewAdapter(getContext(), null, mLocation, this);
    }

    protected String[] getProjection() {
        return new String[]{
                CardContract.Entry._ID,
                CardContract.Entry.COL_NAME,
                CardContract.Entry.COL_CITY,
                CardContract.Entry.COL_PHOTO_1,
                CardContract.Entry.COL_N_LISTENS,
                CardContract.Entry.COL_N_VISITS,
                CardContract.Entry.COL_N_CLICKS,
                CardContract.Entry.COL_LAT,
                CardContract.Entry.COL_LNG,
                ServiceContract.Entry.COL_NAME,
        };
    }

    //fare overloading dell'altro metodo nella versione definitiva
    protected String[] getProjectionItinerary() {
        return new String[]{
                ItineraryContract.Entry._ID,
                ItineraryContract.Entry.COL_NAME,
                ItineraryContract.Entry.COL_COMUNE,
                ItineraryContract.Entry.COL_IMAGE,
                ItineraryContract.Entry.COL_LUNGHEZZA,
                ItineraryContract.Entry.COL_N_SITI,
                ItineraryContract.Entry.COL_H_DURATA,
                ItineraryContract.Entry.COL_TIPO
        };
    }

    protected String getSelection() {
        return CardContract.Entry.COL_CITY + " = ? AND " + CardContract.Entry.COL_CAT_ID + " = " + mCategoryId;
    }

    protected String[] getSelectionArgs() {
        return new String[]{mCityName};
    }

    protected abstract String getSortOrder();

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        //invece di inserire l'if fare l'overloading nella versione definitiva
       // if(mCategoryId == -2)
         //   return new CursorLoader(getContext(), ItineraryContract.URI, getProjectionItinerary(), null, null, null);
        return new CursorLoader(getContext(), CardContract.URI, getProjection(), getSelection(), getSelectionArgs(), getSortOrder());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void cardSelected(int cardCursorPosition, int id, int cardId, boolean isItinerary) {
        if(!isItinerary)
            mListener.cardSelected(cardCursorPosition, this);
        else
            mListener.cardSelected(cardCursorPosition, id, cardId, this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCategoryDetailsFragmentInteractionListener) {
            mListener = (OnCategoryDetailsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}

