package it.audio_guida.romeo.activities.city_select;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.location.Location;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.model.City;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A subclass of {@link CitySelectFragment} that show a dynamic list of city based by last selection and current
 * position.
 * Created by christian on 15/09/16.
 */
public class SmartCitySelectFragment extends CitySelectFragment implements LastLocationFinder.OnLastLocationFindListener {

    private static final int LOADER_CITY_RECENT = 0;
    private static final int LOADER_CITY_INSIDE = 1;
    private static final int LOADER_CITIES_NEAR = 2;

    private Cursor mCityRecentCursor = null;
    private Cursor mCityInsideCursor = null;
    private Cursor mCitiesNearCursor = null;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private CitiesCursorAdapter mAdapter;
    private LastLocationFinder mLastLocationFinder;

    protected static final String[] PROJECTION = new String[]{
            CityContract.Entry._ID,
            CityContract.Entry.COL_NAME,
            CityContract.Entry.COL_IMAGE,
            CityContract.Entry.COL_N_CARDS,
            CityContract.Entry.COL_N_PHOTOS,
            CityContract.Entry.COL_N_AUDIO,
            CityContract.Entry.COL_LENGTH_AUDIO,
            CityContract.Entry.COL_LANGUAGES,
            CityContract.Entry.COL_C_LAT,
            CityContract.Entry.COL_C_LON,
            CityContract.Entry.COL_RAGGIO,
            CityContract.Entry.COL_NE_LAT,
            CityContract.Entry.COL_NE_LON,
            CityContract.Entry.COL_SO_LAT,
            CityContract.Entry.COL_SO_LON
    };

    public static CitySelectFragment newInstance() {
        return new SmartCitySelectFragment();
    }

    public SmartCitySelectFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCitiesNearCursor = null;
        View rootView = inflater.inflate(R.layout.fragment_city_select, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new CitiesCursorAdapter(getContext(), null, this);
        mRecyclerView.setAdapter(mAdapter);

        // Get recent city
        loadRecentCity();
        loadInsideCity();

        return rootView;
    }

    private void loadRecentCity() {
        MyPreferences preferences = new MyPreferences(getContext());
        String cityName = preferences.getString(DataConstants.PREF_KEY_CITY, null);
        if (cityName != null) {
            Bundle args = new Bundle();
            args.putString(CityContract.Entry.COL_NAME, cityName);
            getLoaderManager().restartLoader(LOADER_CITY_RECENT, args, this);
        }
    }

    private void loadInsideCity() {
        mLastLocationFinder = new LastLocationFinder(getContext(), this);
        mLastLocationFinder.loadLastLocationAsync();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_CITY_RECENT:
                return new CursorLoader(getContext(),
                        CityContract.URI,
                        PROJECTION,
                        CityContract.Entry.COL_NAME + " = ?",
                        new String[]{args.getString(CityContract.Entry.COL_NAME)},
                        null);
            case LOADER_CITY_INSIDE:
                latitude = args.getDouble("latitude");
                longitude = args.getDouble("longitude");
                return new CursorLoader(getContext(),
                        CityContract.URI,
                        PROJECTION,
                        null,
                        null, null);

                /*return new CursorLoader(getContext(),
                        CityContract.URI,
                        PROJECTION,
                        CityContract.Entry.COL_SO_LAT + " < " + latitude + " AND "
                                + CityContract.Entry.COL_NE_LAT + " > " + latitude + " AND "
                                + CityContract.Entry.COL_SO_LON + " < " + longitude + " AND "
                                + CityContract.Entry.COL_NE_LON + " > " + longitude,
                        null, null);
                        */
        }
        return null;
    }

    public static Double Distance(Double lat1, Double lon1, Double lat2, Double lon2)
    {
        double theta = lon1 - lon2;
        double distance = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.cos(Math.toRadians(theta));

        distance = Math.acos(distance);
        distance = Math.toDegrees(distance);
        distance = distance * 60 * 1.1515;

        distance = distance * 1.609344; //fattore di conversione da miglia a chilometri
        return (distance);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_CITY_RECENT:
                mCityRecentCursor = data;
                break;
            case LOADER_CITY_INSIDE:
                if(data != null){
                    LinkedList<CityNearClass> cityNearClasses = new LinkedList<>();
                    mCityInsideCursor = null;
                    MatrixCursor matrixCursor = new MatrixCursor(PROJECTION);
                    int id, ncards, nphotos, naudio;
                    String name, image, languages, lengthaudio;
                    double lat1, lat_ne, lat_so ;
                    double lon1, lon_ne, lon_so;
                    int raggio;
                    double distanza;
                    data.moveToPosition(-1);
                    int index;
                    while(data.moveToNext()) {
                        id = data.getInt(data.getColumnIndex(CityContract.Entry._ID));
                        name = data.getString(data.getColumnIndex(CityContract.Entry.COL_NAME));
                        image = data.getString(data.getColumnIndex(CityContract.Entry.COL_IMAGE));
                        ncards = data.getInt(data.getColumnIndex(CityContract.Entry.COL_N_CARDS));
                        nphotos = data.getInt(data.getColumnIndex(CityContract.Entry.COL_N_PHOTOS));
                        naudio = data.getInt(data.getColumnIndex(CityContract.Entry.COL_N_AUDIO));
                        lengthaudio = data.getString(data.getColumnIndex(CityContract.Entry.COL_LENGTH_AUDIO));
                        languages = data.getString(data.getColumnIndex(CityContract.Entry.COL_LANGUAGES));
                        lat1 = data.getDouble(data.getColumnIndex(CityContract.Entry.COL_C_LAT));
                        lon1 = data.getDouble(data.getColumnIndex(CityContract.Entry.COL_C_LON));
                        raggio = data.getInt(data.getColumnIndex(CityContract.Entry.COL_RAGGIO));
                        lat_ne = data.getDouble(data.getColumnIndex(CityContract.Entry.COL_NE_LAT));
                        lon_ne = data.getDouble(data.getColumnIndex(CityContract.Entry.COL_NE_LON));
                        lat_so = data.getDouble(data.getColumnIndex(CityContract.Entry.COL_SO_LAT));
                        lon_so = data.getDouble(data.getColumnIndex(CityContract.Entry.COL_SO_LON));
                        distanza = Distance(lat1, lon1, latitude, longitude);


                        if((lat_so < latitude) && (lat_ne  > latitude) &&
                                (lon_so < longitude) && (lon_ne > longitude)) {


                            distanza = 0;
                            MatrixCursor in_aus = new MatrixCursor(PROJECTION);
                            in_aus.addRow(new Object[]{
                                    id,
                                    name,
                                    image,
                                    ncards,
                                    nphotos,
                                    naudio,
                                    lengthaudio,
                                    languages,
                                    lat1,
                                    lon1,
                                    distanza,
                                    lat_ne,
                                    lon_ne,
                                    lat_so,
                                    lon_so
                            });
                            mCityInsideCursor = in_aus;

                        }

                        if((distanza<raggio)&&(distanza!=0)){
                            index = (int)distanza/5;
                            /*matrixCursor[index>=matrixCursor.length?matrixCursor.length-1: index]
                                    .addRow(new Object[]{
                                    id,
                                    name,
                                    image,
                                    ncards,
                                    nphotos,
                                    naudio,
                                    lengthaudio,
                                    languages,
                                    lat1,
                                    lon1,
                                    distanza,
                                    lat_ne,
                                    lon_ne,
                                    lat_so,
                                    lon_so
                            });
                            */

                            cityNearClasses.add(new CityNearClass(id,
                                    name,
                                    image,
                                    ncards,
                                    nphotos,
                                    naudio,
                                    lengthaudio,
                                    languages,
                                    lat1,
                                    lon1,
                                    distanza,
                                    lat_ne,
                                    lon_ne,
                                    lat_so,
                                    lon_so
                            ));
                        }
                    }
                    if(cityNearClasses.isEmpty())
                        return;
                    CityNearClass[] cityNearClassArray = new CityNearClass[cityNearClasses.size()];
                    cityNearClasses.toArray(cityNearClassArray);
                    java.util.Arrays.sort(cityNearClassArray, new Comparator<CityNearClass>() {
                        @Override
                        public int compare(CityNearClass o1, CityNearClass o2) {
                            return o1.getDistanza()<o2.getDistanza()? -2: 2;
                        }
                    });
                    for(int i=0; i<cityNearClassArray.length; ++i){
                        matrixCursor.addRow(cityNearClassArray[i].cityNear());
                    }

                    mCitiesNearCursor = matrixCursor;

                    break;
                }

                //mCityInsideCursor = data;
                break;
            case LOADER_CITIES_NEAR:
                //mCitiesNearCursor = data;
                break;
        }
        swapAdapterToMergeCursor();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    private void swapAdapterToMergeCursor() {
        List<Cursor> cursorList = new LinkedList<>();

        if (mCityInsideCursor != null && mCityInsideCursor.getCount() != 0) {
            MatrixCursor matrixCursor = new MatrixCursor(new String[]{BaseColumns._ID, "name"});
            //matrixCursor.addRow(new Object[]{-1, "Audio guida turistica attiva"});
            matrixCursor.addRow(new Object[]{-1, "La tua posizione"});
            cursorList.add(matrixCursor);
            cursorList.add(mCityInsideCursor);
        }

        if (mCitiesNearCursor != null && mCitiesNearCursor.getCount() != 0) {
            MatrixCursor matrixCursor = new MatrixCursor(new String[]{BaseColumns._ID, "name"});
            matrixCursor.addRow(new Object[]{-1, "Nelle vicinanze"});
            cursorList.add(matrixCursor);
            cursorList.add(mCitiesNearCursor);
        }

        /*
        if (mCityRecentCursor != null && mCityRecentCursor.getCount() != 0) {
            MatrixCursor matrixCursor = new MatrixCursor(new String[]{BaseColumns._ID, "name"});
            matrixCursor.addRow(new Object[]{-3, getContext().getString(R.string.recent_city)});
            cursorList.add(matrixCursor);
            cursorList.add(mCityRecentCursor);
        }
        */

        if (cursorList.size() != 0) {
            MergeCursor mergeCursor = new MergeCursor(cursorList.toArray(new Cursor[cursorList.size()]));
            mAdapter.swapCursor(mergeCursor);
        } else {
            mAdapter.swapCursor(null);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        loadRecentCity();
        loadInsideCity();
    }

    @Override
    public void onLastLocationFind(Location location) {
        Bundle args = new Bundle();
        args.putDouble("longitude", location.getLongitude());
        args.putDouble("latitude", location.getLatitude());
        getLoaderManager().restartLoader(LOADER_CITY_INSIDE, args, this);
    }

    @Override
    public void onLastLocationFindError(String message) {
        List<Cursor> cursorList = new LinkedList<>();
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{BaseColumns._ID, "name"});
        matrixCursor.addRow(new Object[]{-1, message});
        cursorList.add(matrixCursor);
        if (cursorList.size() != 0) {
            MergeCursor mergeCursor = new MergeCursor(cursorList.toArray(new Cursor[cursorList.size()]));
            mAdapter.swapCursor(mergeCursor);
        }

    }

    private class CityNearClass{

        private int id, ncards, nphotos, naudio;
        private String name, image, languages, lengthaudio;
        double lat1, lat_ne, lat_so ;
        double lon1, lon_ne, lon_so;
        double distanza;

        public CityNearClass(int id, String name, String image, int ncards, int nphotos, int naudio, String lengthaudio,
                             String languages, double lat1, double lon1, double distanza, double lat_ne, double lon_ne,
                              double lat_so, double lon_so){

            this.id = id;
            this.name = name;
            this.image = image;
            this.ncards = ncards;
            this.nphotos = nphotos;
            this.naudio = naudio;
            this.lengthaudio = lengthaudio;
            this.languages = languages;
            this.lat1 = lat1;
            this.lon1 = lon1;
            this.distanza = distanza;
            this.lat_ne = lat_ne;
            this.lon_ne = lon_ne;
            this.lat_so = lat_so;
            this.lon_so = lon_so;

        }

        public double getDistanza() {
            return distanza;
        }

        public Object[] cityNear(){
            return new Object[]{
                    id,
                    name,
                    image,
                    ncards,
                    nphotos,
                    naudio,
                    lengthaudio,
                    languages,
                    lat1,
                    lon1,
                    distanza,
                    lat_ne,
                    lon_ne,
                    lat_so,
                    lon_so
            };
        }

    }

}
