package it.audio_guida.romeo.activities.city_summary;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.card_view.CardViewActivity;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.CategoryContract;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.model.City;
import org.apache.commons.lang3.text.WordUtils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class CitySummaryFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        CategoriesCardsCursorAdapter.OnAdapterInteractionListener, View.OnClickListener {
    @SuppressWarnings("unused")
    public static final int LOADER_ID_CARDS = 0;
    public static final int LOADER_ID_CITY = -1;

    public static final int LOADER_ID_ITINERARY = -2;
    public static final int LOADER_ID_DA_NON_PERDERE = -3;

    public static final int ITINERARI = 0;
    public static final int DA_NON_PERDERE = 1;
    public static final int CATEGORIE = 2;
    public static final int VICINANZE = 3;

    public static final String KEY_CATEGORY_ID = "categoryId";
    private static final String ARG_CITY_NAME = "cityName";
    private static final String ARG_IN_VISITA = "inVisita";
    private static final String ARG_LATITUDINE = "latitudine";
    private static final String ARG_LONGITUDINE = "longitudine";

    private String mCityName;
    private OnFragmentInteractionListener mListener;

    private RecyclerView[] mRecyclerViews;
    private CategoriesCardsCursorAdapter[] mAdapters;
    private LinearLayout mCardsGroupContainer;
    private LayoutInflater mLayoutInflater;
    private View[] mInflatedViews;
    private int mLoadersLoaded = 0;
    private Map<Integer, Integer> mCategories = new HashMap<>();
    private ProgressBar mProgressBar;
    private boolean daNonPerderePresenti = false;

    public CitySummaryFragment() {
    }

    public static CitySummaryFragment newInstance(String cityName) {
        CitySummaryFragment fragment = new CitySummaryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CITY_NAME, cityName.toUpperCase());;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCityName = getArguments().getString(ARG_CITY_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_summary, container, false);

        mCardsGroupContainer = (LinearLayout) view.findViewById(R.id.ll);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        mLayoutInflater = getLayoutInflater(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID_CITY, null, this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_ID_CITY:
                return new CursorLoader(getContext(), CityContract.URI, new String[]{
                        CityContract.Entry.COL_CATEGORIES,
                        CityContract.Entry.COL_IMAGE,
                        CityContract.Entry.COL_N_ITINERARIES
                }, CityContract.Entry.COL_NAME + " LIKE ?", new String[]{mCityName}, null);
            case LOADER_ID_ITINERARY:
                return new CursorLoader(getContext(), ItineraryContract.URI, new String[]{
                        ItineraryContract.Entry._ID,
                        ItineraryContract.Entry.COL_NAME,
                        ItineraryContract.Entry.COL_COMUNE,
                        ItineraryContract.Entry.COL_IMAGE}, null, null, null);
            case LOADER_ID_DA_NON_PERDERE:
                return new CursorLoader(getContext(), CardContract.URI, new String[]{CardContract.Entry._ID,
                        CardContract.Entry.COL_NAME,
                        CardContract.Entry.COL_PHOTO_1,
                        CardContract.Entry.COL_CITY,
                        CardContract.Entry.COL_CAT_ID,
                        CardContract.Entry.COL_N_CLICKS,
                        CategoryContract.Entry.COL_NAME,
                }, CardContract.Entry.COL_CITY + " = ? AND " + CardContract.Entry.COL_PRIORITY + " = 1",
                        new String[]{mCityName}, CardContract.Entry.COL_N_CLICKS + " DESC");
            default:
                int catId = args.getInt(KEY_CATEGORY_ID);
                String[] projection = {
                        CardContract.Entry._ID,
                        CardContract.Entry.COL_NAME,
                        CardContract.Entry.COL_PHOTO_1,
                        CardContract.Entry.COL_CITY,
                        CardContract.Entry.COL_CAT_ID,
                        CategoryContract.Entry.COL_NAME,
                };
                String selection = CardContract.Entry.COL_CITY + " = ? AND " + CardContract.Entry.COL_CAT_ID + " = " + catId;
                String[] selectionArgs = {mCityName};
                String sortOrder = CardContract.Entry.COL_PRIORITY + ", " + CardContract.Entry.COL_NAME;
                return new CursorLoader(getContext(), CardContract.URI, projection, selection, selectionArgs, sortOrder);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int id = loader.getId();
        switch (id) {
            case LOADER_ID_CITY:
                if (data.moveToFirst()) {
                    String categoriesString = data.getString(data.getColumnIndex(CityContract.Entry.COL_CATEGORIES));
                    int[] categories = City.getCategoriesFromEncodedString(categoriesString);
                    int nItinerary = data.getInt(data.getColumnIndex(CityContract.Entry.COL_N_ITINERARIES));
                    showCardsCategories(categories, nItinerary);
                    mListener.onCityCursorLoadFinished(data);
                }
                break;

            case LOADER_ID_ITINERARY:
                if(data.moveToFirst()) {
                    int index = mInflatedViews.length-1;
                    mInflatedViews[index].setVisibility(View.VISIBLE);
                    setCategoryTitle(index, data, ITINERARI);
                    mAdapters[index].swapCursor(data);
                }
                break;
            case LOADER_ID_DA_NON_PERDERE:
                if (data.getCount() > 0) {
                    mProgressBar.setVisibility(View.GONE);
                    mInflatedViews[0].setVisibility(View.VISIBLE);
                    setCategoryTitle(id, data, DA_NON_PERDERE);
                }
                mAdapters[0].swapCursor(data);
                break;
            default:

                if (data.getCount() > 0) {
                    if(daNonPerderePresenti) {
                        //mProgressBar.setVisibility(View.GONE);
                        mInflatedViews[id + 1].setVisibility(View.VISIBLE);
                        setCategoryTitle(id, data, CATEGORIE);
                        mAdapters[id + 1 ].swapCursor(data);
                    }
                    else {
                        mProgressBar.setVisibility(View.GONE);
                        mInflatedViews[id].setVisibility(View.VISIBLE);
                        setCategoryTitle(id , data, CATEGORIE);
                        mAdapters[id].swapCursor(data);
                    }
                }
                mAdapters[daNonPerderePresenti?id+1:id].swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        int id = loader.getId();
        if (id >= 0) {
            mAdapters[id].swapCursor(null);
        }
    }

    private void setCategoryTitle(int id, Cursor data, int tipo) {
        TextView categoryNameTextView;
        String categoryName;
        int categoryId;
        Button moreCardsButton;
        switch (tipo) {
            case ITINERARI:
                categoryNameTextView = (TextView) mInflatedViews[id].findViewById(R.id.category_name);
                categoryName = getString(R.string.itinerary_title);
                categoryNameTextView.setText(WordUtils.capitalizeFully(categoryName));

                categoryId = -2;
                moreCardsButton = (Button) mInflatedViews[id].findViewById(R.id.more_cards_button);
                moreCardsButton.setTag(R.id.tag_category_id, categoryId);
                moreCardsButton.setTag(R.id.tag_category_name, categoryName);
                moreCardsButton.setOnClickListener(this);
                break;
            case DA_NON_PERDERE:
                categoryNameTextView = (TextView) mInflatedViews[0].findViewById(R.id.category_name);
                categoryName = getString(R.string.cards_order_top);
                categoryNameTextView.setText(WordUtils.capitalizeFully(categoryName));

                categoryId = -4;  //TODO usare le costanti, -2 itinerari, -3 tappe, -4 da non perdere
                moreCardsButton = (Button) mInflatedViews[0].findViewById(R.id.more_cards_button);
                moreCardsButton.setTag(R.id.tag_category_id, categoryId);
                moreCardsButton.setTag(R.id.tag_category_name, categoryName);
                moreCardsButton.setOnClickListener(this);
                break;
            case VICINANZE:
                categoryNameTextView = (TextView) mInflatedViews[0].findViewById(R.id.category_name);
                categoryName = "Nelle Vicinanze";//getString(R.string.cards_order_top);
                categoryNameTextView.setText(WordUtils.capitalizeFully(categoryName));

                break;
            default:
                if (data.moveToFirst()) {
                    int categorieSupplementari = 0;
                    if(daNonPerderePresenti) {
                        categorieSupplementari = 1;
                    }

                    categoryNameTextView = (TextView) mInflatedViews[id+categorieSupplementari].findViewById(R.id.category_name);
                    categoryName = data.getString(data.getColumnIndex(CategoryContract.Entry.COL_NAME));
                    categoryNameTextView.setText(WordUtils.capitalizeFully(categoryName));

                    categoryId = data.getInt(data.getColumnIndex(CardContract.Entry.COL_CAT_ID));
                    moreCardsButton = (Button) mInflatedViews[id+categorieSupplementari].findViewById(R.id.more_cards_button);
                    moreCardsButton.setTag(R.id.tag_category_id, categoryId);
                    moreCardsButton.setTag(R.id.tag_category_name, categoryName);
                    moreCardsButton.setOnClickListener(this);
                }
                break;
        }
    }

    /**
     * Build the categories views and put in the linear layout
     *
     * @param categories array of categories ids
     */
    //TODO da riorganizzare il codice
    private void showCardsCategories(int[] categories, int nItinerary) {

        if(nItinerary>0) {

            mCardsGroupContainer.removeAllViews();
            mCategories.clear();
            for (int i = 0; i < mLoadersLoaded; i++) getLoaderManager().destroyLoader(i);

            //TODO soluzione provvisoria da modificare
            Cursor query = getContext().getContentResolver().query(CardContract.URI,
                    new String[]{CardContract.Entry._ID},
                    /*CardContract.Entry.COL_CITY + " = ? AND " +*/ CardContract.Entry.COL_PRIORITY + " = 1",
                    null /*new String[]{mCityName}*/, null);

            int categorieSupplementari = 0;
            query.moveToFirst();
            if(query.getCount()>0) {
                categorieSupplementari = 1;
                daNonPerderePresenti = true;
            }

            int length = categories.length + 1 + categorieSupplementari;
            mAdapters = new CategoriesCardsCursorAdapter[length];
            mRecyclerViews = new RecyclerView[length];
            mInflatedViews = new View[length];
            mLoadersLoaded = length;

            LinearLayoutManager layoutManager;

            if(categorieSupplementari > 0) {

                mInflatedViews[0] = mLayoutInflater.inflate(R.layout.partial_city_summary, mCardsGroupContainer, false);
                mRecyclerViews[0] = (RecyclerView) mInflatedViews[0].findViewById(R.id.category_cards_recycler);

                layoutManager = new LinearLayoutManager(getContext());
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                mRecyclerViews[0].setLayoutManager(layoutManager);
                mRecyclerViews[0].setHasFixedSize(true);
                mRecyclerViews[0].setNestedScrollingEnabled(false);
                mAdapters[0] = new CategoriesCardsCursorAdapter(getContext(), null, this);
                mRecyclerViews[0].setAdapter(mAdapters[0]);

                getLoaderManager().initLoader(LOADER_ID_DA_NON_PERDERE, null, this);
            }

            for (int i = categorieSupplementari; i < length - 1; i++) {
                int categoryId = categories[i-categorieSupplementari];

                mInflatedViews[i] = mLayoutInflater.inflate(R.layout.partial_city_summary, mCardsGroupContainer, false);
                mRecyclerViews[i] = (RecyclerView) mInflatedViews[i].findViewById(R.id.category_cards_recycler);

                layoutManager = new LinearLayoutManager(getContext());
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                mRecyclerViews[i].setLayoutManager(layoutManager);
                mRecyclerViews[i].setHasFixedSize(true);
                mRecyclerViews[i].setNestedScrollingEnabled(false);
                mAdapters[i] = new CategoriesCardsCursorAdapter(getContext(), null, this);
                mRecyclerViews[i].setAdapter(mAdapters[i]);

                Bundle args = new Bundle();
                args.putInt(KEY_CATEGORY_ID, categoryId);
                mCategories.put(categoryId, i-categorieSupplementari);
                getLoaderManager().initLoader(i-categorieSupplementari, args, this);
            }


            //int categoryId = categories[length-1];

            mInflatedViews[length - 1] = mLayoutInflater.inflate(R.layout.partial_city_summary, mCardsGroupContainer, false);
            mRecyclerViews[length - 1] = (RecyclerView) mInflatedViews[length - 1].findViewById(R.id.category_cards_recycler);

            layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            mRecyclerViews[length - 1].setLayoutManager(layoutManager);
            mRecyclerViews[length - 1].setHasFixedSize(true);
            mRecyclerViews[length - 1].setNestedScrollingEnabled(false);
            mAdapters[length - 1] = new CategoriesCardsCursorAdapter(getContext(), null, this);
            mRecyclerViews[length - 1].setAdapter(mAdapters[length - 1]);

            //Bundle args = new Bundle();
            //args.putInt(KEY_CATEGORY_ID, categoryId);
            //mCategories.put(categoryId, 2);
            getLoaderManager().initLoader(LOADER_ID_ITINERARY, null, this);
        }
        else{
            mCardsGroupContainer.removeAllViews();
            mCategories.clear();
            for (int i = 0; i < mLoadersLoaded; i++) getLoaderManager().destroyLoader(i);

            Cursor query = getContext().getContentResolver().query(CardContract.URI,
                    new String[]{CardContract.Entry._ID},
                    /*CardContract.Entry.COL_CITY + " = ? AND " +*/ CardContract.Entry.COL_PRIORITY + " = 1",
                    null /*new String[]{mCityName}*/, null);

            int categorieSupplementari = 0;
            query.moveToFirst();
            if(query.getCount()>0) {
                categorieSupplementari = 1;
                daNonPerderePresenti = true;
            }

            int length = categories.length + categorieSupplementari;
            mAdapters = new CategoriesCardsCursorAdapter[length];
            mRecyclerViews = new RecyclerView[length];
            mInflatedViews = new View[length];
            mLoadersLoaded = length;

            LinearLayoutManager layoutManager;

            if(categorieSupplementari > 0) {

                mInflatedViews[0] = mLayoutInflater.inflate(R.layout.partial_city_summary, mCardsGroupContainer, false);
                mRecyclerViews[0] = (RecyclerView) mInflatedViews[0].findViewById(R.id.category_cards_recycler);

                layoutManager = new LinearLayoutManager(getContext());
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                mRecyclerViews[0].setLayoutManager(layoutManager);
                mRecyclerViews[0].setHasFixedSize(true);
                mRecyclerViews[0].setNestedScrollingEnabled(false);
                mAdapters[0] = new CategoriesCardsCursorAdapter(getContext(), null, this);
                mRecyclerViews[0].setAdapter(mAdapters[0]);

                getLoaderManager().initLoader(LOADER_ID_DA_NON_PERDERE, null, this);
            }

            for (int i = categorieSupplementari; i < length; i++) {
                int categoryId = categories[i-categorieSupplementari];

                mInflatedViews[i] = mLayoutInflater.inflate(R.layout.partial_city_summary, mCardsGroupContainer, false);
                mRecyclerViews[i] = (RecyclerView) mInflatedViews[i].findViewById(R.id.category_cards_recycler);

                layoutManager = new LinearLayoutManager(getContext());
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                mRecyclerViews[i].setLayoutManager(layoutManager);
                mRecyclerViews[i].setHasFixedSize(true);
                mRecyclerViews[i].setNestedScrollingEnabled(false);
                mAdapters[i] = new CategoriesCardsCursorAdapter(getContext(), null, this);
                mRecyclerViews[i].setAdapter(mAdapters[i]);

                Bundle args = new Bundle();
                args.putInt(KEY_CATEGORY_ID, categoryId);
                mCategories.put(categoryId, i-categorieSupplementari);
                getLoaderManager().initLoader(i -categorieSupplementari, args, this);
            }
        }

        for (View v : mInflatedViews) {
            v.setVisibility(View.GONE);
            mCardsGroupContainer.addView(v);
        }
    }

    @Override
    public void cardSelected(int position, int cardId, int catId) {
        if(catId != -2) {
            Intent intent = new Intent(getContext(), CardViewActivity.class);
            String selection = null;
            String[] selectionArgs = {mCityName};;
            String sortOrder = null;
            if(catId == -4){
                selection = CardContract.Entry.COL_CITY + " = ? AND " + CardContract.Entry.COL_PRIORITY + " = 1";
                sortOrder = CardContract.Entry.COL_N_CLICKS + " DESC";

            }
            else {
                selection = CardContract.Entry.COL_CITY + " = ? AND " + CardContract.Entry.COL_CAT_ID + " = " + catId;
                sortOrder = CardContract.Entry.COL_PRIORITY + ", " + CardContract.Entry.COL_NAME;
            }
            intent.putExtra(CardViewActivity.ARG_ID, cardId);
            intent.putExtra(CardViewActivity.ARG_POSITION, position);
            intent.putExtra(CardViewActivity.ARG_SELECTION, selection);
            intent.putExtra(CardViewActivity.ARG_SELECTION_ARGS, selectionArgs);
            intent.putExtra(CardViewActivity.ARG_SORT_ORDER, sortOrder);
            startActivity(intent);
        }
        else{
            int categoryId = -3;
            Cursor c = getActivity().getContentResolver().query(ItineraryContract.URI, new String[]{
                            ItineraryContract.Entry._ID,
                            ItineraryContract.Entry.COL_NAME,},
                    ItineraryContract.Entry._ID +" = ?", new String[]{Integer.toString(cardId)}, null);

            c.moveToFirst();
            String name = c.getString(c.getColumnIndex(ItineraryContract.Entry.COL_NAME));
            mListener.startItineraryDetailsActivity(name, categoryId, cardId);
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.more_cards_button:
                int categoryId = (int) v.getTag(R.id.tag_category_id);
                String categoryName = (String) v.getTag(R.id.tag_category_name);
                mListener.startCategoryDetailsActivity(mCityName, categoryId, categoryName);
        }
    }

    public interface OnFragmentInteractionListener {
        void startCitySelectActivity();

        void startCategoryDetailsActivity(String cityName, int categoryId, String categoryName);

        void startItineraryDetailsActivity(String cityName, int categoryId, int cardId);

        void onCityCursorLoadFinished(Cursor data);
    }
}
