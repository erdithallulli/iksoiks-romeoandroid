package it.audio_guida.romeo.activities.card_view;

import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.model.Card;

public class VideoActivity extends AppCompatActivity {

    private String videoUrl;
    public static final String VIDEO_URL = "video_url";
    public static final String CARD_ID = "card_id";
    private ProgressBar progressBar;
    private LocalBroadcastManager mBroadcaster;
    private int mCardId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoUrl = getIntent().getStringExtra(VIDEO_URL);
        mCardId = getIntent().getIntExtra(CARD_ID, -1);
        mBroadcaster = LocalBroadcastManager.getInstance(this);
        sendLog(false);
        setContentView(R.layout.activity_video);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        //progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        videoView.setVideoURI(Uri.parse(videoUrl));
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.INVISIBLE);
                sendLog(true);
            }
        });

        videoView.start();
    }

    //startVideo è true se il video è avviato,
    // se invece si è cliccato sul video, ma non è partito è false
    private void sendLog(boolean startVideo){

        if(mCardId == -1) return;

        Cursor query = getContentResolver().query(CardContract.URI, new String[]{CardContract.Entry._ID,
                        CardContract.Entry.COL_SERVICE_ID,CardContract.Entry.COL_CAT_ID, CardContract.Entry.COL_CITY,
                        CardContract.Entry.COL_AUDIO, CardContract.Entry.COL_TEXT, CardContract.Entry.COL_DATA_EXISTS},
                CardContract.Entry._ID +" = ?", new String[]{Integer.toString(mCardId)}, null);
        int service_id = 0;
        int cat_id = 0;
        String name = "";
        if(query != null) {
            query.moveToFirst();
            int index = query.getColumnIndex(CardContract.Entry.COL_SERVICE_ID);
            service_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CAT_ID);
            cat_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CITY);
            name = query.getString(index);
        }

        Intent i = new Intent(DataConstants.INTENT_ACTION_EVENT);
        i.putExtra(DataConstants.ARG_ID, mCardId);
        i.putExtra(DataConstants.ARG_COD_CAT, cat_id);
        i.putExtra(DataConstants.ARG_COD_SER, service_id);
        i.putExtra(DataConstants.ARG_CITY_NAME, name);
        i.putExtra(DataConstants.ARG_IS_ITINERARY, false);  //serve per invio log dal dettaglio degli itinerari

        if(startVideo) {
            i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_START_VIDEO);
            i.putExtra(DataConstants.ARG_AUDIO_NAME, videoUrl);
        }else {
            i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_OPENED_VIDEO);
            i.putExtra(DataConstants.ARG_AUDIO_NAME, videoUrl);
        }
        mBroadcaster.sendBroadcast(i);
    }
}
