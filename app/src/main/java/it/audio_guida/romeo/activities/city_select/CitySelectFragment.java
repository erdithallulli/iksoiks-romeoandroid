package it.audio_guida.romeo.activities.city_select;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.model.City;

/**
 * A Fragment that shows a list of cities.
 */
public class CitySelectFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, CitiesCursorAdapter.OnAdapterInteractionListener {
    protected static final String[] PROJECTION = new String[]{
            CityContract.Entry._ID,
            CityContract.Entry.COL_NAME,
            CityContract.Entry.COL_IMAGE,
            CityContract.Entry.COL_N_CARDS,
            CityContract.Entry.COL_N_PHOTOS,
            CityContract.Entry.COL_N_AUDIO,
            CityContract.Entry.COL_LENGTH_AUDIO,
            CityContract.Entry.COL_LANGUAGES,
            CityContract.Entry.COL_C_LAT,
            CityContract.Entry.COL_C_LON,
            CityContract.Entry.COL_NE_LAT,
            CityContract.Entry.COL_NE_LON,
            CityContract.Entry.COL_SO_LAT,
            CityContract.Entry.COL_SO_LON
    };

    protected double latitude;
    protected double longitude;

    protected static final String ARG_CITY_TYPE_ID = "city_type_id";

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private CitiesCursorAdapter mAdapter;
    private OnCitySelectionListener mListener;

    public CitySelectFragment() {
    }

    public static CitySelectFragment newInstance(int cityTypeId) {
        CitySelectFragment fragment = new CitySelectFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CITY_TYPE_ID, cityTypeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_city_select, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new CitiesCursorAdapter(getContext(), null, this);
        mRecyclerView.setAdapter(mAdapter);

        getLoaderManager().initLoader(0, null, this);

        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int sectionCode = getArguments().getInt(ARG_CITY_TYPE_ID);
        String selection = CityContract.Entry.COL_CITY_TYPE + " = " + sectionCode;
        return new CursorLoader(getContext(), CityContract.URI, PROJECTION, selection, null, null);
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCitySelectionListener) {
            mListener = (OnCitySelectionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCitySelectionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void citySelected(String cityName, boolean inVisita) {
        if(inVisita)
            mListener.citySelected(cityName, inVisita, latitude, longitude);
        else
            mListener.citySelected(cityName, inVisita, 0.0, 0.0);
    }
}