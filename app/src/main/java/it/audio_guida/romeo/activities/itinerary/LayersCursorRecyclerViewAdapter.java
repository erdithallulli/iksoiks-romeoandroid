package it.audio_guida.romeo.activities.itinerary;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.contracts.CategoryUnionServiceViewContract;
import it.audio_guida.romeo.model.CategoryService;
import it.audio_guida.romeo.view_libs.CursorRecyclerViewAdapter;
import org.apache.commons.lang3.text.WordUtils;

import java.util.Set;

public class LayersCursorRecyclerViewAdapter extends CursorRecyclerViewAdapter<LayersCursorRecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

    public static final int VIEW_TYPE_CATEGORY = CategoryUnionServiceViewContract.Entry.TYPE_CATEGORY;
    public static final int VIEW_TYPE_SERVICE = CategoryUnionServiceViewContract.Entry.TYPE_SERVICE;
    private final Context mContext;
    private final Set<Integer> mServicesNotSelected;

    public LayersCursorRecyclerViewAdapter(Context context, Cursor cursor, Set<Integer> servicesNotSelected) {
        super(context, cursor);
        mContext = context;
        mServicesNotSelected = servicesNotSelected;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndex(CategoryUnionServiceViewContract.Entry._ID));
        int type = cursor.getInt(cursor.getColumnIndex(CategoryUnionServiceViewContract.Entry.COL_TYPE));
        String nameStr = cursor.getString(cursor.getColumnIndex(CategoryUnionServiceViewContract.Entry.COL_NAME));
        String iconStr = cursor.getString(cursor.getColumnIndex(CategoryUnionServiceViewContract.Entry.COL_ICON));
        viewHolder.name.setTag(R.id.tag_id, id);
        viewHolder.name.setTag(R.id.tag_is_category, type == CategoryUnionServiceViewContract.Entry.TYPE_CATEGORY);
        viewHolder.name.setText(WordUtils.capitalizeFully(nameStr));
        viewHolder.name.setChecked(!mServicesNotSelected.contains(id));
        viewHolder.name.setOnClickListener(this);
        viewHolder.icon.setImageDrawable(mContext.getResources().getDrawable(CategoryService.getIconResourceId(iconStr)));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case VIEW_TYPE_CATEGORY:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_layers_category, parent, false);
                return new ViewHolder(v, VIEW_TYPE_CATEGORY);
            case VIEW_TYPE_SERVICE:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_layers_service, parent, false);
                return new ViewHolder(v, VIEW_TYPE_SERVICE);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        Cursor c = getCursor();
        if (c != null && c.moveToPosition(position)) {
            return c.getInt(c.getColumnIndex(CategoryUnionServiceViewContract.Entry.COL_TYPE));
        }
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checked_text_name:
                CheckedTextView ctv = (CheckedTextView) v;
                boolean checked = !ctv.isChecked();
                ctv.setChecked(checked);

                int id = (int) v.getTag(R.id.tag_id);
                if ((boolean) v.getTag(R.id.tag_is_category)) setAllServicesInCategorySelected(id, checked);
                setServiceSelected(id, checked);
                break;
        }
    }

    private void setServiceSelected(int serviceId, boolean selected) {
        if (selected) mServicesNotSelected.remove(serviceId);
        else mServicesNotSelected.add(serviceId);
    }

    private void setAllServicesInCategorySelected(int catId, boolean selected) {
        Cursor cursor = getCursor();
        if (cursor == null) return;

        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            int cCatId = cursor.getInt(cursor.getColumnIndex(CategoryUnionServiceViewContract.Entry.COL_CAT_ID));
            if (catId != cCatId) continue;
            int id = cursor.getInt(cursor.getColumnIndex(CategoryUnionServiceViewContract.Entry._ID));
            setServiceSelected(id, selected);
            notifyItemChanged(cursor.getPosition());
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final CheckedTextView name;
        public final int viewType;
        public final ImageView icon;

        public ViewHolder(View v, int viewType) {
            super(v);
            view = v;
            name = (CheckedTextView) v.findViewById(R.id.checked_text_name);
            icon = (ImageView) v.findViewById(R.id.image_icon);
            this.viewType = viewType;
        }
    }
}
