package it.audio_guida.romeo.activities.category_details;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.data.contracts.ItineraryNodeContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.network.VolleySingleton;
import it.audio_guida.romeo.view_libs.AutoResizeTextView;
import it.audio_guida.romeo.view_libs.CursorRecyclerViewAdapter;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 20/08/16.
 */
public class CategoryDetailsCursorRecyclerViewAdapter extends CursorRecyclerViewAdapter<CategoryDetailsCursorRecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

    private final OnAdapterInteractionListener mListener;
    private final Context mContext;
    private final Location mCurrentLocation;
    private final ImageLoader mImageLoader;

    public CategoryDetailsCursorRecyclerViewAdapter(Context context, Cursor cursor, Location currentLocation, OnAdapterInteractionListener listener) {
        super(context, cursor);
        mContext = context;
        mCurrentLocation = currentLocation;
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        mListener = listener;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        if(!cursor.getColumnName(1).equals(ItineraryContract.Entry.COL_NAME) &&
                !cursor.getColumnName(1).equals(ItineraryNodeContract.Entry.COL_NAME)) {

            int idIndex = cursor.getColumnIndex(CardContract.Entry._ID);
            int cardNameIndex = cursor.getColumnIndex(CardContract.Entry.COL_NAME);
            int photoIndex = cursor.getColumnIndex(CardContract.Entry.COL_PHOTO_1);
            int cityIndex = cursor.getColumnIndex(CardContract.Entry.COL_CITY);
            int nListensIndex = cursor.getColumnIndex(CardContract.Entry.COL_N_LISTENS);
            int nVisitsIndex = cursor.getColumnIndex(CardContract.Entry.COL_N_VISITS);
            int nClicksIndex = cursor.getColumnIndex(CardContract.Entry.COL_N_CLICKS);
            int latIndex = cursor.getColumnIndex(CardContract.Entry.COL_LAT);
            int lngIndex = cursor.getColumnIndex(CardContract.Entry.COL_LNG);
            int serviceNameIndex = cursor.getColumnIndex(ServiceContract.Entry.COL_NAME);

            long id = cursor.getLong(idIndex);
            String cardName = cursor.getString(cardNameIndex);
            String photo = cursor.getString(photoIndex);
            String city = cursor.getString(cityIndex);
            int nListens = cursor.getInt(nListensIndex);
            int nVisits = cursor.getInt(nVisitsIndex);
            int nClicks = cursor.getInt(nClicksIndex);
            double lat = cursor.getDouble(latIndex);
            double lng = cursor.getDouble(lngIndex);
            String serviceName = cursor.getString(serviceNameIndex);

            int totaleListensClicks = nClicks + nListens;

            StringBuilder textDescriptionString = new StringBuilder(mContext.getString(R.string.card_data_description, nVisits, totaleListensClicks));
            if (mCurrentLocation != null) {
                float[] distances = new float[1];
                Location.distanceBetween(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), lat, lng, distances);
                textDescriptionString.append('\n').append(mContext.getString(R.string.card_data_description_distance, distances[0] / 1000));
            }

            viewHolder.textCardName.setText(cardName);
            viewHolder.textDescription.setText(textDescriptionString);
            viewHolder.networkImageView.setImageUrl(Card.getPreviewImageUrl(city, photo), mImageLoader);
            viewHolder.textServiceName.setText(serviceName);
            viewHolder.parent.setTag(R.id.tag_position, cursor.getPosition());
            viewHolder.parent.setOnClickListener(this);
        }
        else if(cursor.getColumnName(1).equals(ItineraryContract.Entry.COL_NAME)){

            int cardNameIndex = cursor.getColumnIndex(ItineraryContract.Entry.COL_NAME);
            int cityIndex = cursor.getColumnIndex(ItineraryContract.Entry.COL_COMUNE);
            int photoIndex = cursor.getColumnIndex(ItineraryContract.Entry.COL_IMAGE);
            int tipoIndex = cursor.getColumnIndex(ItineraryContract.Entry.COL_TIPO);
            int hdurataIndex = cursor.getColumnIndex(ItineraryContract.Entry.COL_H_DURATA);
            int nsitiIndex = cursor.getColumnIndex(ItineraryContract.Entry.COL_N_SITI);
            int lunghezzaIndex = cursor.getColumnIndex(ItineraryContract.Entry.COL_LUNGHEZZA);

            String cardName = cursor.getString(cardNameIndex);
            String city = cursor.getString(cityIndex);
            String photo = cursor.getString(photoIndex);
            String tipo = cursor.getString(tipoIndex);
            String hdurata = cursor.getString(hdurataIndex);
            int nsiti = cursor.getInt(nsitiIndex);
            int lunghezza = cursor.getInt(lunghezzaIndex);
            String textDescriptionString = new String(""+tipo+ "\nLunghezza (mt): "+lunghezza+"\n" +
                    "Tempo di percorrenza: "+hdurata+"\nNumero siti:"+ nsiti);

            viewHolder.textCardName.setText(cardName);
            viewHolder.textDescription.setText(textDescriptionString);
            viewHolder.networkImageView.setImageUrl("http://195.120.231.180/Palmipedo/ArchivioCitta/ImgItinerari/"+ Uri.encode(photo), mImageLoader);
            //viewHolder.textServiceName.setText(serviceName);
            //viewHolder.parent.setTag(R.id.tag_position, cursor.getPosition());
            viewHolder.parent.setTag(R.id.tag_position, cursor.getInt(cursor.getColumnIndex(ItineraryContract.Entry._ID)));
            viewHolder.parent.setOnClickListener(this);
        }
        else{

            int cardNameIndex = cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_NAME);
            int cityIndex;
            int photoIndex;
            int hdurataIndex = cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_H_DURATA);
            int distanzaIndex = cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_N_DISTANZA);
            int codeIndex = cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_SCHEDA_CODE);
            int cardCode = cursor.getInt(codeIndex);

            String photo = null;
            String city = null;

            Cursor query = mContext.getContentResolver().query(CardContract.URI, new String[]{
                    CardContract.Entry._ID,
                    CardContract.Entry.COL_PHOTO_1,
                    CardContract.Entry.COL_CITY},
                    CardContract.Entry._ID +" = ?", new String[]{Integer.toString(cardCode)}, null);
            if(query != null) {
                query.moveToFirst();
                photoIndex = query.getColumnIndex(CardContract.Entry.COL_PHOTO_1);
                cityIndex = query.getColumnIndex(CardContract.Entry.COL_CITY);
                photo = query.getString(photoIndex);
                city = query.getString(cityIndex);
            }

            String cardName = cursor.getString(cardNameIndex);
            //String city = cursor.getString(cityIndex);
            //String tipo = cursor.getString(tipoIndex);
            String hdurata = cursor.getString(hdurataIndex);
            //int nsiti = cursor.getInt(nsitiIndex);
            int distanza = cursor.getInt(distanzaIndex);
            String textDescriptionString = new String("Distanza (mt): "+distanza+"\n" +
                    "Tempo di percorrenza: "+hdurata);

            viewHolder.textCardName.setText(cardName);
            viewHolder.textDescription.setText(textDescriptionString);
            viewHolder.networkImageView.setImageUrl(null, mImageLoader);
            if(city!=null && photo != null)
                viewHolder.networkImageView.setImageUrl(Card.getPreviewImageUrl(city, photo), mImageLoader);
            //viewHolder.textServiceName.setText(serviceName);
            viewHolder.parent.setTag(R.id.tag_position, cursor.getPosition());
            viewHolder.parent.setOnClickListener(this);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_in_category_details, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onClick(View v) {

        //Cursor c = getCursor();
        /*if (c.moveToPosition(position)) {
            if (c.getColumnName(1).equals(ItineraryContract.Entry.COL_NAME)) {

            } else if (c.getColumnName(1).equals(ItineraryNodeContract.Entry.COL_NAME)) {
                int id = -1;// c.getInt(c.getColumnIndex(CardContract.Entry._ID));
                int cardId = c.getInt(c.getColumnIndex(ItineraryNodeContract.Entry.COL_SCHEDA_CODE));
                mListener.cardSelected((int) v.getTag(R.id.tag_position), id, cardId, true);
            }
            else{
                */       mListener.cardSelected((int) v.getTag(R.id.tag_position), -1, -1, false);
                // }
                //}
    }

    public interface OnAdapterInteractionListener {
        void cardSelected(int cardCursorPosition, int id, int cardId, boolean isItinerary);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final View parent;
        private final AutoResizeTextView textCardName;
        private final TextView textServiceName;
        private final TextView textDescription;
        private final NetworkImageView networkImageView;

        public ViewHolder(View v) {
            super(v);

            parent = v;
            textCardName = (AutoResizeTextView) v.findViewById(R.id.text_card_name);
            textServiceName = (TextView) v.findViewById(R.id.text_service_name);
            textDescription = (TextView) v.findViewById(R.id.text_description);
            networkImageView = (NetworkImageView) v.findViewById(R.id.image);
        }
    }
}
