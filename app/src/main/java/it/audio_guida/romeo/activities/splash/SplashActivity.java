package it.audio_guida.romeo.activities.splash;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Locale;

import it.audio_guida.romeo.activities.city_select.CitySelectActivity;
import it.audio_guida.romeo.activities.welcome.WelcomeActivity;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.runtime_info.RuntimeInfo;
import it.audio_guida.romeo.sync.SyncUtilities;

import static java.lang.Thread.sleep;

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // TODO Show user explanation if needed
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);

            } else onLaunchNextActivity();
        } else onLaunchNextActivity();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        /*if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }*/
        onLaunchNextActivity();
    }

    //TODO modificare il controllo per l'aggiornamento dinamico
    private void onLaunchNextActivity() {
        final MyPreferences preferences = new MyPreferences(SplashActivity.this);
        long lastSyncTime = preferences.getLong(DataConstants.PREF_KEY_LAST_SYNC_TIME, -1);

        String lan = preferences.getString(DataConstants.PREF_KEY_LAN, Locale.getDefault().getISO3Language().toUpperCase());

        //TODO sopostare in un'altra classe apposita per la gestione delle lingue
        String lan2Char;
        if(lan.equals(Locale.ITALIAN.getISO3Language().toUpperCase()))
            lan2Char = "it";
        else if(lan.equals(Locale.GERMAN.getISO3Language().toUpperCase()))
            lan2Char = "de";
        else if(lan.equals(Locale.FRENCH.getISO3Language().toUpperCase()))
            lan2Char = "fr";
        else if(lan.equals((new Locale("es")).getISO3Language().toUpperCase()))
            lan2Char = "es";
        else if(lan.equals((new Locale("ru")).getISO3Language().toUpperCase()))
            lan2Char = "ru";
        else
            lan2Char = "en";


        setLanguage(getApplicationContext(), new Locale(lan2Char).getLanguage());

        /*
        preferences.put(DataConstants.PREF_KEY_SYNC_CITIES, 1);

        SyncUtilities.forceSync(this);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                boolean sincronizzato = false;
                int key_sync;
                while (!sincronizzato) {
                    try {
                        sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    key_sync = preferences.getInt(DataConstants.PREF_KEY_SYNC_CITIES, 0);
                    sincronizzato = key_sync==0? true:false;
                }

                //launchWelcomeActivityAndFinish();
            }
        });
        */

        /*if (lastSyncTime != -1) {
            //RuntimeInfo.getInstance().setCanceled(false);
            SyncUtilities.forceSync(this);
            final Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                        try {
                            sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    launchWelcomeActivityAndFinish();
                }
            });
        }
        */
        //SyncUtilities.forceSync(this);
        launchWelcomeActivityAndFinish();
        //}
       // else launchMainActivityAndFinish();
    }

    private void launchWelcomeActivityAndFinish() {
        startActivity(new Intent(this, WelcomeActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    private void launchMainActivityAndFinish() {
        Intent intent = new Intent(this, CitySelectActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    @SuppressWarnings("deprecation")
    public void setSystemLocaleLegacy(Configuration config, Locale locale){
        config.locale = locale;
        Log.i("Locale depre", config.locale.toString());
    }

    @TargetApi(Build.VERSION_CODES.N)
    public void setSystemLocale(Configuration config, Locale locale){
        config.setLocale(locale);
        Log.i("Locale android N", config.getLocales().get(0).toString());
    }

    public void setLanguage(Context context, String languageCode){
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration config = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            setSystemLocale(config, locale);
        }else{
            setSystemLocaleLegacy(config, locale);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1)
            context.getApplicationContext().getResources().updateConfiguration(config,
                    context.getResources().getDisplayMetrics());
    }
}
