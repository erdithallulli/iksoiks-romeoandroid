package it.audio_guida.romeo.activities.city_select;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Locale;

import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.city_summary.CitySummaryActivity;
import it.audio_guida.romeo.activities.searchCity.SearchCityActivity;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CityTypeContract;
import it.audio_guida.romeo.sync.SyncUtilities;
import it.audio_guida.romeo.tour_guide.LocationFinder;

/**
 * An Activity that permits the user to select a city
 */
public class CitySelectActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, OnCitySelectionListener,
        LocationFinder.LocationListener{

    private static final String[] PROJECTION = new String[]{
            CityTypeContract.Entry._ID,
            CityTypeContract.Entry.COL_NAME,
            CityTypeContract.Entry.COL_ORDER,
    };
    private CityTypesFragmentStatePagerAdapter mCityTypesFragmentStatePagerAdapter;
    private ViewPager mViewPager;
    private Menu menu;

    private LocationFinder mLocationFinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //SyncUtilities.forceSync(this);

        mLocationFinder = new LocationFinder(this, this);
        mLocationFinder.start();
       // mLocationFinder.start();


        configure();
        /*
        setContentView(R.layout.activity_city_select);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mCityTypesFragmentStatePagerAdapter = new CityTypesFragmentStatePagerAdapter(this, getSupportFragmentManager(), null);
        getSupportLoaderManager().restartLoader(0, null, this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mCityTypesFragmentStatePagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        //TODO listener
        TabLayout lingue = (TabLayout) findViewById(R.id.lan_select);
        lingue.addOnTabSelectedListener(new OnLanguageSelectionListener(this));
        */


    }

    public void configure(){
        setContentView(R.layout.activity_city_select);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.setTitle(R.string.title_activity_city_select);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mCityTypesFragmentStatePagerAdapter = new CityTypesFragmentStatePagerAdapter(this, getSupportFragmentManager(), null);
        getSupportLoaderManager().restartLoader(0, null, this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mCityTypesFragmentStatePagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        //TODO listener
        TabLayout lingue = (TabLayout) findViewById(R.id.lan_select);
        lingue.addOnTabSelectedListener(new OnLanguageSelectionListener(this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_city_select, menu);
        menu.getItem(0).setIcon(R.drawable.flag_de);

        //TODO Ricontrollare
        MyPreferences preferences = new MyPreferences(this);
        String lan = preferences.getString(DataConstants.PREF_KEY_LAN, Locale.getDefault().getISO3Language().toUpperCase());

        String lan2Char;
        if(lan.equals(Locale.ITALIAN.getISO3Language().toUpperCase()))
            menu.getItem(0).setIcon(R.drawable.flag_it);
        else if(lan.equals(Locale.GERMAN.getISO3Language().toUpperCase()))
            menu.getItem(0).setIcon(R.drawable.flag_de);
        else if(lan.equals(Locale.FRENCH.getISO3Language().toUpperCase()))
            menu.getItem(0).setIcon(R.drawable.flag_fr);
        else if(lan.equals((new Locale("es")).getISO3Language().toUpperCase()))
            menu.getItem(0).setIcon(R.drawable.flag_es);
        else if(lan.equals((new Locale("ru")).getISO3Language().toUpperCase()))
            menu.getItem(0).setIcon(R.drawable.flag_ru);
        else
            menu.getItem(0).setIcon(R.drawable.flag_en);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.lan_change:
                TabLayout languages = (TabLayout) findViewById(R.id.lan_select);
                if(languages.getVisibility()== TabLayout.GONE)
                    languages.setVisibility(TabLayout.VISIBLE);
                else
                    languages.setVisibility(TabLayout.GONE);
                return true;

            case R.id.action_search:
                Intent intent = new Intent(this, SearchCityActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, CityTypeContract.URI, PROJECTION, "", null, CityTypeContract.Entry.COL_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//        MatrixCursor matrixCursor = new MatrixCursor(PROJECTION);
//        matrixCursor.addRow(new Object[]{-1, "HOME", -1});
//
//        MergeCursor mergeCursor = new MergeCursor(new Cursor[]{matrixCursor, data});
//        mCityTypesFragmentStatePagerAdapter.swapCursor(mergeCursor);
        mCityTypesFragmentStatePagerAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCityTypesFragmentStatePagerAdapter.swapCursor(null);
    }

    @Override
    public void citySelected(String cityName, boolean inVisita, double latitudine, double longitudine) {
        CitySummaryActivity.launch(this, cityName, inVisita, latitudine, longitudine);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setTitle(R.string.closing_title)
                .setMessage(R.string.closing_message)
                .setPositiveButton(R.string.closing_positive_answer, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })
                .setNegativeButton(R.string.closing_negative_answer, null)
                .show();

    }

    @Override
    public void onStop(){
        super.onStop();

    }



    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onLocationFinderStart() {

    }

    @Override
    public void onLocationFinderStop() {

    }

    @Override
    public void onLocationFinderFailed() {

    }
}
