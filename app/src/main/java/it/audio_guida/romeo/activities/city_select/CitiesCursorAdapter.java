package it.audio_guida.romeo.activities.city_select;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.network.VolleySingleton;
import it.audio_guida.romeo.view_libs.CursorRecyclerViewAdapter;
import org.apache.commons.lang3.text.WordUtils;

/**
 * An Adapter that generates a list of cities from a {@link Cursor}.
 */
public class CitiesCursorAdapter extends CursorRecyclerViewAdapter<CitiesCursorAdapter.ViewHolder> implements View.OnClickListener {

    public static final int VIEW_TYPE_SUBHEADER = 1;
    public static final int VIEW_TYPE_CITY = 0;

    private final ImageLoader mImageLoader;
    private final Context mContext;
    private final OnAdapterInteractionListener mListener;
    private final int mFlagMarginRight;
    private final Resources mResources;

    public CitiesCursorAdapter(Context context, Cursor cursor, OnAdapterInteractionListener listener) {
        super(context, cursor);
        this.mContext = context;
        this.mListener = listener;
        mImageLoader = VolleySingleton.getInstance().getImageLoader();

        mResources = mContext.getResources();
        mFlagMarginRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, mResources.getDisplayMetrics());
    }

    @Override
    public int getItemViewType(int position) {
        Cursor c = getCursor();
        if (c != null && c.moveToPosition(position)) {
            return c.getInt(c.getColumnIndex(BaseColumns._ID)) > 0 ? VIEW_TYPE_CITY : VIEW_TYPE_SUBHEADER;
        }
        return 0;
    }

    @Override
    public CitiesCursorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case VIEW_TYPE_CITY:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_city, parent, false);
                return new ViewHolder(v, viewType);
            case VIEW_TYPE_SUBHEADER:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_subheader, parent, false);
                return new ViewHolder(v, viewType);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        Resources res = mContext.getResources();

        switch (viewHolder.viewType) {
            case VIEW_TYPE_CITY:
                onBindViewHolderCity(viewHolder, cursor, res);
                break;

            case VIEW_TYPE_SUBHEADER:
                String name = cursor.getString(cursor.getColumnIndex("name"));
                viewHolder.text.setText(name);
                break;
        }

    }

    private void onBindViewHolderCity(ViewHolder viewHolder, Cursor cursor, Resources res) {
        String name = cursor.getString(cursor.getColumnIndex(CityContract.Entry.COL_NAME));
        String image = cursor.getString(cursor.getColumnIndex(CityContract.Entry.COL_IMAGE));
        int nCards = cursor.getInt(cursor.getColumnIndex(CityContract.Entry.COL_N_CARDS));
        int nPhotos = cursor.getInt(cursor.getColumnIndex(CityContract.Entry.COL_N_PHOTOS));
        int nAudio = cursor.getInt(cursor.getColumnIndex(CityContract.Entry.COL_N_AUDIO));
        String lAudio = cursor.getString(cursor.getColumnIndex(CityContract.Entry.COL_LENGTH_AUDIO));
        String languages = cursor.getString(cursor.getColumnIndex(CityContract.Entry.COL_LANGUAGES));

        String cityDescriptionFormat = res.getString(R.string.city_description);
        String cardsString = res.getQuantityString(R.plurals.cards, nCards, nCards);
        String photosString = res.getQuantityString(R.plurals.photos, nPhotos, nPhotos);
        String itinerariesString = res.getQuantityString(R.plurals.itineraries, 0, 0);
        String audioTracksString = res.getQuantityString(R.plurals.audio_tracks, nAudio, nAudio);
        String descriptionString = String.format(cityDescriptionFormat, cardsString, itinerariesString, photosString, audioTracksString, lAudio);


        viewHolder.view.setTag(name);
        if(cursor.getColumnName(10).equals(CityContract.Entry.COL_RAGGIO)) {
            double raggio = cursor.getDouble(cursor.getColumnIndex(CityContract.Entry.COL_RAGGIO));
            if(raggio > 0) {
                viewHolder.distanzaText.setVisibility(View.VISIBLE);
                viewHolder.distanzaText.setEms(4);
                viewHolder.distanzaText.setText(Integer.toString((int) raggio) + " km");
            }
            else{
                viewHolder.distanzaText.setVisibility(View.VISIBLE);
                viewHolder.distanzaText.setEms(12);
                viewHolder.distanzaText.setText("Audio guida turistica attiva");
                viewHolder.view.setTag("_"+name);
            }
        }
        else {
            viewHolder.distanzaText.setVisibility(View.GONE);
        }

        viewHolder.networkImageView.setImageUrl(City.getImageUrl(image), mImageLoader);
        viewHolder.textName.setText(WordUtils.capitalizeFully(name));
        viewHolder.textDescription.setText(descriptionString);

        //viewHolder.view.setTag(name);
        viewHolder.view.setOnClickListener(this);

        viewHolder.languages.removeAllViews();
        String[] supportedLanguages = City.getSupportedLanguagesFromEncodedString(languages);
        for (String lang : supportedLanguages) {
            ImageView imageView = new ImageView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, mFlagMarginRight, 0);
            imageView.setImageDrawable(mResources.getDrawable(City.getFlagResourceId(lang)));
            viewHolder.languages.addView(imageView, layoutParams);
        }
    }

    @Override
    public void onClick(View v) {
        CharSequence cityName = (String) v.getTag();
        if(cityName.charAt(0) == '_')
            mListener.citySelected(cityName.subSequence(1, cityName.length()).toString(), true);
        else
           mListener.citySelected(cityName.toString(), false);
    }

    public interface OnAdapterInteractionListener {
        void citySelected(String cityName, boolean inVisita);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {

        private final int viewType;
        private final View view;
        private final TextView text;
        private final TextView textName;
        private final TextView textDescription;
        private final NetworkImageView networkImageView;
        private final ViewGroup languages;
        private final TextView distanzaText;

        private ViewHolder(View v, int viewType) {
            super(v);
            view = v;
            text = (TextView) view.findViewById(R.id.text);
            textName = (TextView) view.findViewById(R.id.text_name);
            textDescription = (TextView) view.findViewById(R.id.text_description);
            networkImageView = (NetworkImageView) view.findViewById(R.id.image);
            languages = (ViewGroup) view.findViewById(R.id.ll_languages);
            distanzaText = (TextView) view.findViewById(R.id.distanzaText);
            this.viewType = viewType;

        }
    }
}


