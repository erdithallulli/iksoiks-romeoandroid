package it.audio_guida.romeo.activities.city_select;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.design.widget.TabLayout;

import java.util.Locale;

import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.sync.SyncUtilities;

public class OnLanguageSelectionListener implements TabLayout.OnTabSelectedListener {

    private Context context;
    private CitySelectActivity citySelectActivity;

    public OnLanguageSelectionListener(CitySelectActivity citySelectActivity){
        this.citySelectActivity = citySelectActivity;
        this.context = citySelectActivity.getApplicationContext();
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onTabSelected(TabLayout.Tab tab){

        int lan = tab.getPosition();
        String new_lan;
        MyPreferences preferences = new MyPreferences(context);
        switch (lan){
            case 0:
                new_lan = Locale.ENGLISH.getISO3Language();
                break;

            case 1:
                new_lan = Locale.ITALIAN.getISO3Language();
                break;

            case 2:
                new_lan = Locale.GERMAN.getISO3Language();
                break;

            case 3:
                new_lan = (new Locale("es")).getISO3Language();
                break;

            case 4:
                new_lan = Locale.FRENCH.getISO3Language();
                break;

            case 5:
                new_lan = (new Locale("ru")).getISO3Language();
                break;

            default:
                new_lan = Locale.ENGLISH.getISO3Language();
                break;
        }

        //TODO sopostare in un'altra classe apposita per la gestione delle lingue
        String lan2Char;
        if(new_lan.equals(Locale.ITALIAN.getISO3Language()))
            lan2Char = "it";
        else if(new_lan.equals(Locale.GERMAN.getISO3Language()))
            lan2Char = "de";
        else if(new_lan.equals(Locale.FRENCH.getISO3Language()))
            lan2Char = "fr";
        else if(new_lan.equals((new Locale("es")).getISO3Language()))
            lan2Char = "es";
        else if(new_lan.equals((new Locale("ru")).getISO3Language()))
            lan2Char = "ru";
        else
            lan2Char = "en";

        preferences.put(DataConstants.PREF_KEY_LAN, new_lan.toUpperCase());
        setLanguage(context, new Locale(lan2Char).getLanguage());

        preferences.put(DataConstants.PREF_KEY_SYNC_CITIES, 1);

        SyncUtilities.forceSync(context);
        Intent intent = new Intent(citySelectActivity, CitySelectActivity.class);

        citySelectActivity.startActivity(intent);
        citySelectActivity.finish();

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        int lan = tab.getPosition();
        String new_lan;
        MyPreferences preferences = new MyPreferences(context);
        switch (lan){
            case 0:
                new_lan = Locale.ENGLISH.getISO3Language();
                break;

            case 1:
                new_lan = Locale.ITALIAN.getISO3Language();
                break;

            case 2:
                new_lan = Locale.GERMAN.getISO3Language();
                break;

            case 3:
                new_lan = (new Locale("es")).getISO3Language();
                break;

            case 4:
                new_lan = Locale.FRENCH.getISO3Language();
                break;
            case 5:
                new_lan = (new Locale("ru")).getISO3Language();
                break;

            default:
                new_lan = Locale.ENGLISH.getISO3Language();
                break;
        }

        //TODO spostare in un'altra classe apposita per la gestione delle lingue
        String lan2Char;
        if(new_lan.equals(Locale.ITALIAN.getISO3Language()))
            lan2Char = "it";
        else if(new_lan.equals(Locale.GERMAN.getISO3Language()))
            lan2Char = "de";
        else if(new_lan.equals(Locale.FRENCH.getISO3Language()))
            lan2Char = "fr";
        else if(new_lan.equals((new Locale("es")).getISO3Language()))
            lan2Char = "es";
        else if(new_lan.equals((new Locale("ru")).getISO3Language()))
            lan2Char = "ru";
        else
            lan2Char = "en";

        preferences.put(DataConstants.PREF_KEY_LAN, new_lan.toUpperCase());
        setLanguage(context, new Locale(lan2Char).getLanguage());

        preferences.put(DataConstants.PREF_KEY_SYNC_CITIES, 1);

        SyncUtilities.forceSync(context);
        Intent intent = new Intent(citySelectActivity, CitySelectActivity.class);

        citySelectActivity.startActivity(intent);
        citySelectActivity.finish();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @SuppressWarnings("deprecation")
    public void setSystemLocaleLegacy(Configuration config, Locale locale){
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    @TargetApi(Build.VERSION_CODES.N)
    public void setSystemLocale(Configuration config, Locale locale){
        config.setLocale(locale);
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    public void setLanguage(Context context, String languageCode){
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration config = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            setSystemLocale(config, locale);
        }else{
            setSystemLocaleLegacy(config, locale);
        }
        //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1)
            context.getResources().updateConfiguration(config,
                    context.getResources().getDisplayMetrics());
    }
}
