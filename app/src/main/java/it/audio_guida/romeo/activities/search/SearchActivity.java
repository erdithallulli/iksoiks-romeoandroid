package it.audio_guida.romeo.activities.search;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.card_view.CardViewActivity;
import it.audio_guida.romeo.activities.category_details.CategoryDetailsCursorRecyclerViewAdapter;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.Card;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements TextWatcher, CategoryDetailsCursorRecyclerViewAdapter.OnAdapterInteractionListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final String[] PROJECTION = {
            CardContract.Entry._ID,
            CardContract.Entry.COL_NAME,
            CardContract.Entry.COL_CITY,
            CardContract.Entry.COL_PHOTO_1,
            CardContract.Entry.COL_N_LISTENS,
            CardContract.Entry.COL_N_CLICKS,
            CardContract.Entry.COL_N_VISITS,
            CardContract.Entry.COL_LAT,
            CardContract.Entry.COL_LNG,
            ServiceContract.Entry.COL_NAME,
    };

    private static final String SORT_ORDER = CardContract.Entry.COL_PRIORITY + " ASC, " + CardContract.Entry.COL_NAME;
    private static final String ARG_CITY_NAME = "cityName";
    private static final String ARG_CATEGORY_ID = "categoryId";

    private EditText mQueryEditText;
    private CategoryDetailsCursorRecyclerViewAdapter mAdapter;
    private String mQuery;
    private String mCityName;
    private int mCategoryId;
    private LocalBroadcastManager mBroadcaster;

    public static void start(FragmentActivity activity, String cityName) {
        start(activity, cityName, -1);
    }

    public static void start(FragmentActivity activity, String cityName, int categoryId) {
        Intent intent = new Intent(activity, SearchActivity.class);
        intent.putExtra(ARG_CITY_NAME, cityName);
        intent.putExtra(ARG_CATEGORY_ID, categoryId);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        mCityName = intent.getStringExtra(ARG_CITY_NAME);
        mCategoryId = intent.getIntExtra(ARG_CATEGORY_ID, -1);
        mBroadcaster = LocalBroadcastManager.getInstance(this);

        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mQueryEditText = (EditText) findViewById(R.id.edit_text_query);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // Settings RecyclerView
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new CategoryDetailsCursorRecyclerViewAdapter(this, null, null, this);
        mRecyclerView.setAdapter(mAdapter);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mQueryEditText.addTextChangedListener(this);

        if (mQueryEditText.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        search();
    }

    private void search() {
        mQuery = mQueryEditText.getText().toString();
        if (mQuery.length() >= 3) {
            getLoaderManager().restartLoader(0, null, this);
        } else {
            getLoaderManager().destroyLoader(0);
        }
    }

    //TODO adattare al nuovo listener
    /*
    @Override
    public void cardSelected(int cardCursorPosition) {
        Intent intent = new Intent(this, CardViewActivity.class);
        intent.putExtra(CardViewActivity.ARG_POSITION, cardCursorPosition);
        intent.putExtra(CardViewActivity.ARG_PROJECTION, PROJECTION);
        intent.putExtra(CardViewActivity.ARG_SELECTION, getSelection());
        intent.putExtra(CardViewActivity.ARG_SELECTION_ARGS, getSelectionArgs());
        intent.putExtra(CardViewActivity.ARG_SORT_ORDER, SORT_ORDER);
        startActivity(intent);
    }
    */


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, CardContract.URI, PROJECTION, getSelection(),
                getSelectionArgs(), SORT_ORDER);
    }

    protected String getSelection() {
        String selection = CardContract.Entry.COL_NAME + " LIKE ?";
        if (mCityName != null) selection += " AND " + CardContract.Entry.COL_CITY + " = ?";
        if (mCategoryId > -1) selection += " AND " + CardContract.Entry.COL_CAT_ID + " = " + mCategoryId;
        else if(mCategoryId == -4) selection += " AND " + CardContract.Entry.COL_PRIORITY + " = 1";
        return selection;
    }

    protected String[] getSelectionArgs() {
        ArrayList<String> args = new ArrayList<>(3);
        args.add(String.format("%%%s%%", mQuery));
        if (mCityName != null) args.add(mCityName);
        return args.toArray(new String[args.size()]);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(DataConstants.INTENT_ACTION_EVENT);
        i.putExtra(DataConstants.ARG_CITY_NAME, mCityName);
        i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MASSAGE_SEARCH);
        i.putExtra(DataConstants.ARG_AUDIO_NAME, mQuery);
        mBroadcaster.sendBroadcast(i);
        super.onBackPressed();
    }

    @Override
    public void cardSelected(int cardCursorPosition, int id, int cardId, boolean isItinerary) {

        Cursor query = getContentResolver().query(CardContract.URI,
                PROJECTION,
                getSelection(),
                getSelectionArgs(),
                SORT_ORDER);
        if(query != null && query.moveToPosition(cardCursorPosition)){
            int index = query.getColumnIndex(CardContract.Entry.COL_NAME);
            String name = query.getString(index);
            Intent i = new Intent(DataConstants.INTENT_ACTION_EVENT);
            i.putExtra(DataConstants.ARG_CITY_NAME, mCityName);
            i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MASSAGE_SEARCH);
            i.putExtra(DataConstants.ARG_AUDIO_NAME, name);
            mBroadcaster.sendBroadcast(i);
        }

        Intent intent = new Intent(this, CardViewActivity.class);
        intent.putExtra(CardViewActivity.ARG_POSITION, cardCursorPosition);
        intent.putExtra(CardViewActivity.ARG_PROJECTION, PROJECTION);
        intent.putExtra(CardViewActivity.ARG_SELECTION, getSelection());
        intent.putExtra(CardViewActivity.ARG_SELECTION_ARGS, getSelectionArgs());
        intent.putExtra(CardViewActivity.ARG_SORT_ORDER, SORT_ORDER);
        startActivity(intent);
    }
}
