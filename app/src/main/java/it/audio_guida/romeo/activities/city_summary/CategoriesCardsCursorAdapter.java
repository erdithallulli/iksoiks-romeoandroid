package it.audio_guida.romeo.activities.city_summary;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.network.VolleySingleton;
import it.audio_guida.romeo.view_libs.CursorRecyclerViewAdapter;

public class CategoriesCardsCursorAdapter extends CursorRecyclerViewAdapter<CategoriesCardsCursorAdapter.ViewHolder> implements View.OnClickListener {

    private final ImageLoader mImageLoader;
    private final OnAdapterInteractionListener mListener;

    public CategoriesCardsCursorAdapter(Context context, Cursor cursor, OnAdapterInteractionListener listener) {
        super(context, cursor);
        this.mListener = listener;
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
    }

    @Override
    public CategoriesCardsCursorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        String cardName;
        String city;
        String photo1;
        if(cursor.getColumnName(1).equals(ItineraryContract.Entry.COL_NAME)){
            cardName = cursor.getString(cursor.getColumnIndex(ItineraryContract.Entry.COL_NAME));
            cardName = cardName.substring(0, 1).toUpperCase() + cardName.substring(1).toLowerCase();
            city = cursor.getString(cursor.getColumnIndex(ItineraryContract.Entry.COL_COMUNE));
            photo1 = cursor.getString(cursor.getColumnIndex(ItineraryContract.Entry.COL_IMAGE));
            viewHolder.photo.setImageUrl("http://195.120.231.180/Palmipedo/ArchivioCitta/ImgItinerari/"+ Uri.encode(photo1), mImageLoader);
        }
        else {
            cardName = cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_NAME));
            city = cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_CITY));
            photo1 = cursor.getString(cursor.getColumnIndex(CardContract.Entry.COL_PHOTO_1));
            viewHolder.photo.setImageUrl(Card.getPreviewImageUrl(city, photo1), mImageLoader);
        }
        viewHolder.textName.setText(cardName);

        viewHolder.mCardView.setTag(cursor.getPosition());
        viewHolder.mCardView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Integer position = (Integer) v.getTag();
        Cursor c = getCursor();
        if (c.moveToPosition(position)) {
            if(c.getColumnName(1).equals(ItineraryContract.Entry.COL_NAME)){
                int id = c.getInt(c.getColumnIndex(ItineraryContract.Entry._ID));
                mListener.cardSelected(position, id, -2);

            }else {

                int id = c.getInt(c.getColumnIndex(CardContract.Entry._ID));
                int catId = c.getInt(c.getColumnIndex(CardContract.Entry.COL_CAT_ID));
                if(c.getColumnIndex(CardContract.Entry.COL_N_CLICKS) != -1)
                    catId = -4;
                mListener.cardSelected(position, id, catId);
            }
        }
    }

    public interface OnAdapterInteractionListener {
        void cardSelected(int position, int cardId, int catId);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView textName;
        public NetworkImageView photo;

        public ViewHolder(CardView v) {
            super(v);
            mCardView = v;
            textName = (TextView) mCardView.findViewById(R.id.text_name);
            photo = (NetworkImageView) mCardView.findViewById(R.id.image);
        }
    }

    public static class ViewHolder1 extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView textName;
        public NetworkImageView photo;

        public ViewHolder1(CardView v) {
            super(v);
            mCardView = v;
            textName = (TextView) mCardView.findViewById(R.id.text_name);
            photo = (NetworkImageView) mCardView.findViewById(R.id.image);
        }
    }

}