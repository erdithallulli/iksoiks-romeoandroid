package it.audio_guida.romeo.activities.searchCity;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.card_view.CardViewActivity;
import it.audio_guida.romeo.activities.category_details.CategoryDetailsCursorRecyclerViewAdapter;
import it.audio_guida.romeo.activities.city_select.CitiesCursorAdapter;
import it.audio_guida.romeo.activities.city_select.CityTypesFragmentStatePagerAdapter;
import it.audio_guida.romeo.activities.city_select.OnCitySelectionListener;
import it.audio_guida.romeo.activities.city_summary.CitySummaryActivity;
import it.audio_guida.romeo.activities.search.SearchActivity;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.network.ServerRequests;

public class SearchCityActivity extends AppCompatActivity implements TextWatcher, CitiesCursorAdapter.OnAdapterInteractionListener, LoaderManager.LoaderCallbacks<Cursor> {

    protected static final String[] PROJECTION = new String[]{
            CityContract.Entry._ID,
            CityContract.Entry.COL_NAME,
            CityContract.Entry.COL_IMAGE,
            CityContract.Entry.COL_N_CARDS,
            CityContract.Entry.COL_N_PHOTOS,
            CityContract.Entry.COL_N_AUDIO,
            CityContract.Entry.COL_LENGTH_AUDIO,
            CityContract.Entry.COL_LANGUAGES,
            CityContract.Entry.COL_C_LAT,
            CityContract.Entry.COL_C_LON,
            CityContract.Entry.COL_NE_LAT,
            CityContract.Entry.COL_NE_LON,
            CityContract.Entry.COL_SO_LAT,
            CityContract.Entry.COL_SO_LON
    };

    private static final String SORT_ORDER = CityContract.Entry.COL_NAME + " ASC";


    private EditText mQueryEditText;
    private CitiesCursorAdapter mAdapter;
    private String mQuery;

    private int mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        mUserId = -1;

        setContentView(R.layout.activity_search_city);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mQueryEditText = (EditText) findViewById(R.id.edit_text_query);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // Settings RecyclerView
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new CitiesCursorAdapter(this, null, this);
        mRecyclerView.setAdapter(mAdapter);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mQueryEditText.addTextChangedListener(this);

        if (mQueryEditText.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        search();
    }

    private void search() {
        mQuery = mQueryEditText.getText().toString();
        if (mQuery.length() >= 3) {
            getLoaderManager().restartLoader(0, null, this);
        } else {
            getLoaderManager().destroyLoader(0);
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, CityContract.URI, PROJECTION, getSelection(),
                getSelectionArgs(), SORT_ORDER);
    }

    protected String getSelection() {
        String selection = CityContract.Entry.COL_NAME + " LIKE ?";
        return selection;
    }

    protected String[] getSelectionArgs() {
        ArrayList<String> args = new ArrayList<>(3);
        args.add(String.format("%%%s%%", mQuery));
        return args.toArray(new String[args.size()]);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void citySelected(String cityName, boolean inVisita) {
        anonymousLogin(cityName);
        CitySummaryActivity.launch(this, cityName, false, 0.0, 0.0);
    }

    @Override
    public void onBackPressed(){
        if(mQuery.length()>=3)
            anonymousLogin(mQuery);
        super.onBackPressed();
    }

    private void anonymousLogin(final String message){
        String liveId = "NO LIVE ID";
        String deviceId = getDeviceId();
        String nomeApp = "Palmipedo Android";
        PackageInfo pInfo = null;
        String version = "2.1.4";
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String versionePrg = version;
        String terminaleTipo = "Android";
        int terminaleFirmware = Build.VERSION.SDK_INT;
        String linguaSistema = Resources.getSystem().getConfiguration().locale.getLanguage();
        String linguaSelezionata = Locale.getDefault().getLanguage();
        String lastDataOra = (new Date()).toString();
        Log.i("Server Logging", "Anonymous login...");
        ServerRequests.anonymousLogin(new Response.Listener<JSONObject>() {
                                          @Override
                                          public void onResponse(JSONObject response) {
                                              try {
                                                  mUserId = response.getInt("_id");
                                                  sendLog(message);
                                              } catch (JSONException e) {
                                                  e.printStackTrace();
                                              }
                                          }
                                      }, new Response.ErrorListener() {
                                          @Override
                                          public void onErrorResponse(VolleyError error) {
                                              error.printStackTrace();
                                          }
                                      }, liveId, deviceId, nomeApp, false, versionePrg, "", terminaleTipo,
                terminaleFirmware, linguaSistema, linguaSelezionata, lastDataOra, "0", "0");
    }

    private String getDeviceId() {
        return android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
    }


    private void sendLog(String message){
        Log.i("sendLog","infoFile:"+message);
        ServerRequests.sendLog(new Response.Listener<JSONObject>() {
                                   @Override
                                   public void onResponse(JSONObject response) {
                                   }
                               }, new Response.ErrorListener() {
                                   @Override
                                   public void onErrorResponse(VolleyError error) {
                                       error.printStackTrace();
                                   }
                               }, mUserId, "0", "0", "0", 0,
                "", 0, 0, "infoFile="+message, "SEARCH COMUNE");
    }

}