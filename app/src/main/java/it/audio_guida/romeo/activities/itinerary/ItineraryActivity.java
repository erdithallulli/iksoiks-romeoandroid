package it.audio_guida.romeo.activities.itinerary;

import android.content.*;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.*;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import it.audio_guida.romeo.ITourGuideInterface;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.card_view.CardViewActivity;
import it.audio_guida.romeo.activities.map.MapsActivity;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.data.contracts.ItineraryNodeContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.model.CategoryService;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.network.VolleySingleton;
import it.audio_guida.romeo.runtime_info.RuntimeInfo;
import it.audio_guida.romeo.tour_guide.TourGuideService;
import it.audio_guida.romeo.view_libs.AutoResizeTextView;
import it.audio_guida.romeo.view_libs.MediaPlayerUIControlManager;

import static it.audio_guida.romeo.tour_guide.TourGuideService.MESSAGE_AUDIO_READY;
import static it.audio_guida.romeo.tour_guide.TourGuideService.MESSAGE_NEAR;

public class ItineraryActivity extends AppCompatActivity implements OnMapReadyCallback,
        View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor>,
        GoogleMap.OnMarkerClickListener,
        ServiceConnection{

    public static final String ARG_CARD_ID = "card_id";
    public static final String ARG_ITINERARY_NAME = "itinerary_name";
    private static final int LOADER_TAPPE = 0;
    private static final int LOADER_CITY = 1;
    private String mCityName;
    private boolean mMapDrawed = false;
    protected Cursor mCityCursor = null;
    protected Cursor mCardsCursor = null;
    protected GoogleMap mMap;
    private LinkedList<Item> mItems;
    private int mCardSelectedId;
    private CardView mCardView;
    private AutoResizeTextView mNameTextView;
    private NetworkImageView mImageView;
    private ImageLoader mImageLoader;
    private MediaPlayerUIControlManager mMediaPlayerUIControlManager;
    private Button prev;
    private Button next;
    private boolean mMoveMapCameraExecuted = false;
    private ITourGuideInterface mITourGuideService;
    private Marker lastMarker;
    private ArrayList<Marker> mMarkers;
    private LocalBroadcastManager mBroadcaster;
    private boolean autoplay;


    private boolean isSatelliteView = false;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String s = intent.getStringExtra(TourGuideService.ARG_MESSAGE);
            int cardId;
            switch (s) {
                case MESSAGE_NEAR:
                    cardId = intent.getIntExtra(TourGuideService.ARG_CARD_ID, -1);
                    if(cardId != -1) sendLog(cardId, true, true);
                    findMarker(cardId);
                    break;
                case MESSAGE_AUDIO_READY:
                    cardId = intent.getIntExtra(TourGuideService.ARG_CARD_ID, -1);
                    if(cardId != -1) sendLog(cardId, false, true);
                    findMarker(cardId);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCityName = getIntent().getStringExtra(MapsActivity.ARG_CITY_NAME);
        mCardSelectedId = getIntent().getIntExtra(ItineraryActivity.ARG_CARD_ID, -1);
        String title = getIntent().getStringExtra(ItineraryActivity.ARG_ITINERARY_NAME);
        if(title!=null)
            setTitle(title.substring(0, 1).toUpperCase() +  title.substring(1).toLowerCase());
        if (mCityName == null) throw new IllegalArgumentException("Too few arguments");
        setContentView(R.layout.activity_itinerary);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mImageLoader = VolleySingleton.getInstance().getImageLoader();

        autoplay = false;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBroadcaster = LocalBroadcastManager.getInstance(this);

        mCardView = (CardView) findViewById(R.id.card);
        mNameTextView = (AutoResizeTextView) findViewById(R.id.text_name);
        mImageView = (NetworkImageView) findViewById(R.id.image);
        prev = (Button) findViewById(R.id.btn_prev);
        next = (Button) findViewById(R.id.btn_next);
        mCardView.setOnClickListener(this);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);
        mMediaPlayerUIControlManager = new MediaPlayerUIControlManager(this, R.id.media_player_controls, mCardView, true);

        getSupportLoaderManager().initLoader(LOADER_CITY, null, this);
        getSupportLoaderManager().initLoader(LOADER_TAPPE, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        menu.findItem(R.id.action_layers).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_map_type:
                isSatelliteView = !isSatelliteView;
                mMap.setMapType(isSatelliteView ? GoogleMap.MAP_TYPE_SATELLITE : GoogleMap.MAP_TYPE_NORMAL);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        int index;
        Marker marker;
        switch (v.getId()) {
            case R.id.card:
                // Start Card View Activity
                Intent intent = new Intent(this, CardViewActivity.class);
                intent.putExtra(CardViewActivity.ARG_ID, mCardSelectedId);
                startActivity(intent);
                break;
            case R.id.btn_prev:
                // Start Card View Activity
                index = mMarkers.indexOf(lastMarker);
                if(index == 0) return;
                marker = mMarkers.get(index-1);
                centerToMarker((Item)marker.getTag(), marker);
                break;
            case R.id.btn_next:
                // Start Card View Activity
                index = mMarkers.indexOf(lastMarker);
                if(index == (mMarkers.size()-1)) return;
                marker = mMarkers.get(index+1);
                centerToMarker((Item)marker.getTag(), marker);
                break;
        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException ignored) {
        }
        googleMap.setBuildingsEnabled(false);
        googleMap.setTrafficEnabled(false);

        Cursor c = getContentResolver().query(CityContract.URI, new String[]{
                CityContract.Entry._ID,
                CityContract.Entry.COL_NAME,
                CityContract.Entry.COL_USER_MAP
        }, CityContract.Entry.COL_NAME+ " = ?", new String[]{mCityName}, null);

        c.moveToFirst();
        String userMap = c.getString(c.getColumnIndex(CityContract.Entry.COL_USER_MAP));

        if(userMap!=null && userMap.toLowerCase().equals("si"))
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        maybeIsReady();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_CITY:
                return new CursorLoader(this, CityContract.URI, new String[]{
                        CityContract.Entry._ID,
                        CityContract.Entry.COL_CATEGORIES,
                        CityContract.Entry.COL_SO_LAT,
                        CityContract.Entry.COL_SO_LON,
                        CityContract.Entry.COL_NE_LAT,
                        CityContract.Entry.COL_NE_LON,
                }, CityContract.Entry.COL_NAME + " = ?", new String[]{mCityName}, "");
            case LOADER_TAPPE:
                return new CursorLoader(this, ItineraryNodeContract.URI, new String[]{
                        ItineraryNodeContract.Entry._ID,
                        ItineraryNodeContract.Entry.COL_NAME,
                        ItineraryNodeContract.Entry.COL_SCHEDA_CODE,
                        ItineraryNodeContract.Entry.COL_LAT,
                        ItineraryNodeContract.Entry.COL_LON
                }, /*ItineraryNodeContract.Entry.COL_SCHEDA_CODE +" != 0"*/ null, null,
                        ItineraryNodeContract.Entry._ID + " ASC");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_CITY:
                mCityCursor = data;
                maybeIsReady();
                break;
            case LOADER_TAPPE:
                mCardsCursor = data;
                maybeIsReady();
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        onLoadFinished(loader, null);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && !mMapDrawed) {
            mMapDrawed = true;
            maybeIsReady();
        }
    }

    private void maybeIsReady() {
        if (isReady()) onReady();
    }

    protected boolean isReady() {
        return mMap != null && mMapDrawed && mCardsCursor != null && mCityCursor != null;
    }

    protected void onReady() {
        moveMapCamera();
        putCardsCursorToMap();

        boolean first = false;

        if(mCardSelectedId == -2)
            first = true;

        Iterator<Item> it = mItems.iterator();
        Item item;
        mMarkers = new ArrayList<>();
        PolylineOptions polylineOptions = new PolylineOptions().geodesic(true);

        int i = 0;
        int length = mItems.size();
        int cardId;
        Marker markerApp;
        while (it.hasNext()){
            item = it.next();
            polylineOptions.add(item.getLatLng());
            cardId = item.getCardId();
            if(item.isScheda) {
                if(i==0){
                    markerApp = mMap.addMarker(new MarkerOptions()
                            .icon(item.getIconStart(this))
                            .anchor(0.5f, 1.0f)
                            .position(item.getLatLng()));
                    markerApp.setTag(null);
                }else if(i== length-1){
                    markerApp = mMap.addMarker(new MarkerOptions()
                            .icon(item.getIconEnd(this))
                            .anchor(0.5f, 1.0f)
                            .position(item.getLatLng()));
                    markerApp.setTag(null);
                }

                if((i==0 && first) ||(cardId == mCardSelectedId)) {
                    MarkerOptions marker = new MarkerOptions()
                            .icon(item.getIconBitmapDescriptorEmph(this))
                            .anchor(0.5f, 0.5f)
                            .position(item.getLatLng());
                    lastMarker = mMap.addMarker(marker);
                    lastMarker.setTag(item);
                    mMarkers.add(lastMarker);
                    centerToMarker(item, null);
                }
                else {
                    markerApp = mMap.addMarker(new MarkerOptions()
                            .icon(item.getIconBitmapDescriptor(this))
                            .anchor(0.5f, 0.5f)
                            .position(item.getLatLng()));
                    markerApp.setTag(item);
                    mMarkers.add(markerApp);
                }
            }
            else{
                if(item.isFirstWithoutCard()){
                    MarkerOptions marker = new MarkerOptions()
                            .icon(item.getIconStart(this))
                            .anchor(0.5f, 0.5f)
                            .position(item.getLatLng());
                    lastMarker = mMap.addMarker(marker);
                    lastMarker.setTag(item);
                    mMarkers.add(lastMarker);
                    centerToMarker(item, null);
                }
                else if(item.isLastWithoutCard()){
                    markerApp = mMap.addMarker(new MarkerOptions()
                            .icon(item.getIconEnd(this))
                            .anchor(0.5f, 0.5f)
                            .position(item.getLatLng()));
                    markerApp.setTag(item);
                    mMarkers.add(markerApp);
                }
            }
            ++i;
        }

        mMap.addPolyline(polylineOptions.color(Color.argb(255, 2, 119, 189)).width(18));
        mMap.setOnMarkerClickListener(this);

        //double lat = 44.4939554592719;
        //double lng = 11.3431651806546;
        //mMap.addPolyline(new PolylineOptions().geodesic(true)
        //        .add(new LatLng(lat, lng))  // Sydney
         //       .add(new LatLng(lat+0.01, lng+0.01))  // Fiji
        //);
    }

    protected void moveMapCamera() {
        if(mMoveMapCameraExecuted) return;

        mMoveMapCameraExecuted = true;
        if (mCityCursor.moveToFirst()) {
            double neLat = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_NE_LAT));
            double neLng = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_NE_LON));
            double soLat = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_SO_LAT));
            double soLng = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_SO_LON));
            LatLng neLatLng = new LatLng(neLat, neLng);
            LatLng soLatLng = new LatLng(soLat, soLng);
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(new LatLngBounds(soLatLng, neLatLng), 10));
        }
    }


    protected void putCardsCursorToMap() {
        mMap.clear();

        Cursor cursor = mCardsCursor;

        mItems = new LinkedList<>();

        int colId;
        int colLat;
        int colLng;
        int colName;
        int colCatId;
        int colPhoto;
        int colIcon;

        int codeIndex = cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_SCHEDA_CODE);
        int cardCode;
        Item item;
        cursor.moveToPosition(-1);
        int i = 0;
        int length = cursor.getCount();
        while (cursor.moveToNext()) {
            cardCode = cursor.getInt(codeIndex);

            if (cardCode == 0) {
                double lat = cursor.getDouble(cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_LAT));
                double lon = cursor.getDouble(cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_LON));
                if(i==0)
                    item = new Item(lat, lon, null, false, cardCode, null, null, true, false);
                else if(i == length-1)
                    item = new Item(lat, lon, null, false, cardCode, null, null, false, true);
                else
                    item = new Item(lat, lon, null, false, cardCode, null, null, false, false);
            }
            else {
                double lat = cursor.getDouble(cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_LAT));
                double lon = cursor.getDouble(cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_LON));

                Cursor c = getContentResolver().query(CardContract.URI, new String[]{
                                CardContract.Entry._ID,
                                CardContract.Entry.COL_NAME,
                                CardContract.Entry.COL_LAT,
                                CardContract.Entry.COL_LNG,
                                CardContract.Entry.COL_PHOTO_1,
                                CardContract.Entry.COL_SERVICE_ID,
                                CardContract.Entry.COL_CAT_ID,
                                ServiceContract.Entry.COL_NAME,
                                ServiceContract.Entry.COL_ICON},
                        CardContract.Entry._ID + " = ?", new String[]{Integer.toString(cardCode)}, null);

                c.moveToFirst();

                colId = c.getColumnIndex(CardContract.Entry._ID);
                //colLat = c.getColumnIndex(CardContract.Entry.COL_LAT);
                //colLng = c.getColumnIndex(CardContract.Entry.COL_LNG);
                colName = c.getColumnIndex(CardContract.Entry.COL_NAME);
                colCatId = c.getColumnIndex(CardContract.Entry.COL_CAT_ID);
                colPhoto = c.getColumnIndex(CardContract.Entry.COL_PHOTO_1);
                colIcon = c.getColumnIndex(ServiceContract.Entry.COL_ICON);

                int id = c.getInt(colId);
                int catId = c.getInt(colCatId);
                //int id = c.getInt(colId);
                //double lat = c.getDouble(colLat);
                //double lng = c.getDouble(colLng);
                String icon = c.getString(colIcon);
                String name = c.getString(colName);
                String photo = c.getString(colPhoto);


                item = new Item(lat, lon, icon, true, id, name, photo, false, false);
            }
            ++i;
            mItems.add(item);
        }
    }

    public void findMarker(int schedaCode){
        Item item;
        for(Marker marker: mMarkers){
            item = ((Item)marker.getTag());
            if(item.getCardId() == schedaCode){
                int index = mMarkers.indexOf(marker);
                if( index == 0){
                    //lastMarker = null;
                    if(lastMarker != null) {
                        marker.setIcon(item.getIconBitmapDescriptorEmph(this));

                        if(!((Item)lastMarker.getTag()).isFirstWithoutCard() && !((Item)lastMarker.getTag()).isLastWithoutCard())
                            lastMarker.setIcon(((Item)lastMarker.getTag()).getIconBitmapDescriptor(this));
                        lastMarker = marker;
                    }
                }
                centerToMarker(item, null);
                return;
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if(marker.getTag() == null)
            return false;
        Item item = (Item)marker.getTag();

        centerToMarker(item, marker);
        return true;
    }

    public void centerToMarker(Item item, Marker marker){

        LatLng latLng = item.getLatLng();
        String name = item.getName();
        String image = item.getImage();
        int id = item.getCardId();

        if(marker != null)
            sendLog(id, false, false);

        if(marker != null) {
            if(!item.isFirstWithoutCard() && !item.isLastWithoutCard())
                marker.setIcon(item.getIconBitmapDescriptorEmph(this));

            if(!((Item)lastMarker.getTag()).isFirstWithoutCard() && !((Item)lastMarker.getTag()).isLastWithoutCard())
                lastMarker.setIcon(((Item)lastMarker.getTag()).getIconBitmapDescriptor(this));
            lastMarker = marker;
        }

        if(item.isFirstWithoutCard() || item.isLastWithoutCard()){
            mCardView.setVisibility(View.GONE);
            mMap.setPadding(0, 0, 0, 0);
        }
        else {

            // Show card preview
            Resources r = getResources();
            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, r.getDisplayMetrics());
            mMap.setPadding(0, 0, 0, px);
            mCardView.setVisibility(View.VISIBLE);
            mCardSelectedId = id;

            mMediaPlayerUIControlManager.changeCardIdControlled(id);
            mNameTextView.setText(name);
            mImageView.setImageUrl(Card.getPreviewImageUrl(mCityName, image), mImageLoader);
        }

        // Animate camera to icon
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom((latLng), 18);
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom((latLng), 18));
        mMap.animateCamera(location);
    }

    @Override
    protected void onStart() {
        super.onStart();
        RuntimeInfo.getInstance().setItineraryMode(true);
        bindService(new Intent(this, TourGuideService.class), this, BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,
                new IntentFilter(TourGuideService.INTENT_ACTION));
        mMediaPlayerUIControlManager.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        RuntimeInfo.getInstance().setItineraryMode(false);
        mMediaPlayerUIControlManager.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mITourGuideService = ITourGuideInterface.Stub.asInterface(service);
        try {
            if (mITourGuideService.isPlaying()) {

                int cardAudioLoadedId = mITourGuideService.getCardAudioLoadedId();
                //empathizeCardAsync(cardAudioLoadedId);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mITourGuideService = null;
    }

    private void sendLog(int mCardId, boolean auto, boolean branoAudio){
        if(auto)
            autoplay = true;

        Cursor query = getContentResolver().query(CardContract.URI, new String[]{CardContract.Entry._ID,
                        CardContract.Entry.COL_SERVICE_ID,CardContract.Entry.COL_CAT_ID, CardContract.Entry.COL_CITY,
                        CardContract.Entry.COL_AUDIO, CardContract.Entry.COL_TEXT, CardContract.Entry.COL_DATA_EXISTS},
                CardContract.Entry._ID +" = ?", new String[]{Integer.toString(mCardId)}, null);
        int service_id = 0;
        int cat_id = 0;
        String name = "";
        String audio = "";
        String text = "";
        String dataExists = "";
        if(query != null) {
            query.moveToFirst();
            int index = query.getColumnIndex(CardContract.Entry.COL_SERVICE_ID);
            service_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CAT_ID);
            cat_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CITY);
            name = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_AUDIO);
            audio = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_TEXT);
            text = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_DATA_EXISTS);
            dataExists = query.getString(index);
        }

        int flagPosition = 0;
        boolean audioExists = false;

        //serve per il path
        boolean textExists = false;
        if(dataExists!=null || !dataExists.isEmpty())
            for(int i=0; i<dataExists.length(); ++i){
                if(dataExists.charAt(i)=='0'||dataExists.charAt(i)=='1'){
                    flagPosition++;
                    if(flagPosition==1 && dataExists.charAt(i)=='1')
                        audioExists = true;
                    else if(flagPosition==2 && dataExists.charAt(i)=='1')
                        textExists = true;
                }
            }

        Intent i = new Intent(DataConstants.INTENT_ACTION_EVENT);
        i.putExtra(DataConstants.ARG_ID, mCardId);
        i.putExtra(DataConstants.ARG_COD_CAT, cat_id);
        i.putExtra(DataConstants.ARG_COD_SER, service_id);
        i.putExtra(DataConstants.ARG_CITY_NAME, name);
        i.putExtra(DataConstants.ARG_IS_ITINERARY, false);  //serve per invio log dal dettaglio degli itinerari
        if(branoAudio){
            if(auto) {
                i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_AUDIO_CARD_AUTO);
                if (audioExists)
                    i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getAudioUrl(name, audio));
                else
                    i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getTextUrl(name, text));
            }
            else{
                if(autoplay) {
                    autoplay = false;
                    return;
                }

                i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_AUDIO_CARD_PREVIEW);
                if (audioExists)
                    i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getAudioUrl(name, audio));
                else
                    i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getTextUrl(name, text));

            }
        }
        else {
            i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_OPENED_CARD_PREVIEW);
            i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getTextUrl(name, text));
            autoplay = false;
        }
        mBroadcaster.sendBroadcast(i);
    }


    private class Item{
        private LatLng latLng;
        private String icon;
        private boolean isScheda;
        private int cardId;
        private String name;
        private String image;
        private boolean firstWithoutCard;
        private boolean lastWithoutCard;

        public Item(double lat, double lng, String icon, boolean isScheda, int cardId, String name, String image,
                        boolean firstWithoutCard, boolean lastWithoutCard){
            this.latLng = new LatLng(lat, lng);
            this.icon = icon;
            this.isScheda = isScheda;
            this.cardId = cardId;
            this.name = name;
            this.image = image;
            this.firstWithoutCard = firstWithoutCard;
            this.lastWithoutCard = lastWithoutCard;
        }

        public boolean isFirstWithoutCard() {
            return firstWithoutCard;
        }

        public boolean isLastWithoutCard() {
            return lastWithoutCard;
        }

        public LatLng getLatLng(){ return latLng;}

        public String getIcon(){ return icon;}

        public boolean isScheda(){  return isScheda;}

        public int getCardId(){  return cardId;}

        public String getName(){ return name;}

        public String getImage(){ return image; }

        public BitmapDescriptor getIconStart(Context context) {
            Bitmap baseBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.map_start);

            return BitmapDescriptorFactory.fromBitmap(baseBitmap);
        }

        public BitmapDescriptor getIconEnd(Context context) {
            Bitmap baseBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.map_end);

            return BitmapDescriptorFactory.fromBitmap(baseBitmap);
        }


        public int getIconResourceId() {
            int iconResourceId = CategoryService.getIconResourceId(icon);
            if (iconResourceId == -1) iconResourceId = R.drawable.map_100manca;
            return iconResourceId;
        }

        public BitmapDescriptor getIconBitmapDescriptor(Context context) {
            int iconResourceId = getIconResourceId();

            Bitmap baseBitmap = BitmapFactory.decodeResource(context.getResources(), iconResourceId);
            return BitmapDescriptorFactory.fromBitmap(baseBitmap);
        }

        public BitmapDescriptor getIconBitmapDescriptorEmph(Context context) {
            int iconResourceId = getIconResourceId();

            Bitmap baseBitmap = BitmapFactory.decodeResource(context.getResources(), iconResourceId);
            Bitmap emphBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.map_evidenziato_inmappa);
            Bitmap drawnBitmap = baseBitmap.copy(baseBitmap.getConfig(), true);
            Canvas c = new Canvas(drawnBitmap);
            Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
            c.drawBitmap(emphBitmap, 0, 0, paint);
            return BitmapDescriptorFactory.fromBitmap(drawnBitmap);
        }
    }
}