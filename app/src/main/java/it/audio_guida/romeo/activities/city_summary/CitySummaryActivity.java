package it.audio_guida.romeo.activities.city_summary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.category_details.CategoryDetailsActivity;
import it.audio_guida.romeo.activities.city_select.CitySelectActivity;
import it.audio_guida.romeo.activities.itinerary_details.ItineraryDetailsActivity;
import it.audio_guida.romeo.activities.map.MapsActivity;
import it.audio_guida.romeo.activities.search.SearchActivity;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.network.VolleySingleton;
import it.audio_guida.romeo.runtime_info.RuntimeInfo;
import it.audio_guida.romeo.sync.SyncUtilities;
import it.audio_guida.romeo.tour_guide.TourGuideService;
import org.apache.commons.lang3.text.WordUtils;

import static it.audio_guida.romeo.tour_guide.TourGuideService.MESSAGE_NEAR;

/**
 * An Activity that shows a summary of the cards inside a city.
 */
public class CitySummaryActivity extends AppCompatActivity implements
        CitySummaryFragment.OnFragmentInteractionListener,
        View.OnClickListener {

    private static final String ARG_CITY_NAME = "cityName";
    private static final String ARG_IN_VISITA = "inVisita";
    private static final String ARG_LATITUDINE = "latitudine";
    private static final String ARG_LONGITUDINE = "longitudine";

    private NetworkImageView mNetworkImageView;
    private ImageLoader mImageLoader;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private String mCityName;
    private boolean inVisita = false;
    private double lat, lng;
    LocalBroadcastManager mbroadcastManager;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String s = intent.getStringExtra(TourGuideService.ARG_MESSAGE);
            switch (s) {
                case MESSAGE_NEAR:
                    Intent i = new Intent(CitySummaryActivity.this, MapsActivity.class);
                    int cardId = intent.getIntExtra(TourGuideService.ARG_CARD_ID, -1);
                    i.putExtra(MapsActivity.ARG_CITY_NAME, mCityName);
                    i.putExtra(MapsActivity.ARG_CARD_ID, cardId);
                    i.putExtra(MapsActivity.ARG_AUTO_START_SCHEDA, true);
                    startActivity(i);
                    break;
            }
        }
    };

    /**
     * Launch this activity
     *
     * @param packageContext the current {@link Context}
     * @param cityName       the activity shows infos and cards about this city
     */
    public static void launch(Context packageContext, String cityName, boolean inVisita, double latitudine, double longitudine) {
        Intent i = getLauncherIntent(packageContext, cityName, inVisita, latitudine, longitudine);

        Cursor query = packageContext.getContentResolver().query(CityContract.URI, new String[]{CityContract.Entry._ID,CityContract.Entry.COL_NAME,CityContract.Entry.COL_LANGUAGES},  CityContract.Entry.COL_NAME +" = ?", new String[]{cityName}, null);
        if(query != null){
            query.moveToFirst();
            int index = query.getColumnIndex(CityContract.Entry.COL_LANGUAGES);
            String languages = query.getString(index);
            MyPreferences preferences = new MyPreferences(packageContext);
            preferences.put(DataConstants.PREF_KEY_CITY_LAN, languages);
        }
        packageContext.startActivity(i);
    }

    /**
     * Makes an {@link Intent} for launching this Activity
     * @param packageContext the current {@link Context}
     * @param cityName the activity shows infos and cards about this city
     * @return the {@link Intent} for launching this Activity
     */
    public static Intent getLauncherIntent(Context packageContext, String cityName, boolean inVisita,
                                           double latitudine, double longitudine) {
        Intent i = new Intent(packageContext, CitySummaryActivity.class);
        i.putExtra(ARG_CITY_NAME, cityName);
        i.putExtra(ARG_IN_VISITA, inVisita);
        i.putExtra(ARG_LATITUDINE, latitudine);
        i.putExtra(ARG_LONGITUDINE, longitudine);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageLoader = VolleySingleton.getInstance().getImageLoader();

        mbroadcastManager = LocalBroadcastManager.getInstance(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mNetworkImageView = (NetworkImageView) findViewById(R.id.header_image);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        setSupportActionBar(toolbar);
        //mbroadcastManager.registerReceiver(mReceiver,
        //        new IntentFilter(TourGuideService.INTENT_ACTION));

        if (savedInstanceState == null) {
            String cityNameExtra = getIntent().getStringExtra(ARG_CITY_NAME);
            inVisita = getIntent().getBooleanExtra(ARG_IN_VISITA, false);
            lat = getIntent().getDoubleExtra(ARG_LATITUDINE, 0.0);
            lng = getIntent().getDoubleExtra(ARG_LONGITUDINE, 0.0);
            if (cityNameExtra == null) throw new RuntimeException("Not enough extras to start " + getLocalClassName());
            loadAndSaveCity(cityNameExtra);
        } else {
            String cn = savedInstanceState.getString(ARG_CITY_NAME);
            inVisita = savedInstanceState.getBoolean(ARG_IN_VISITA);
            lat = getIntent().getDoubleExtra(ARG_LATITUDINE, 0.0);
            lng = getIntent().getDoubleExtra(ARG_LONGITUDINE, 0.0);
            if (cn != null) loadAndSaveCity(cn);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(ARG_CITY_NAME, mCityName);
        outState.putBoolean(ARG_IN_VISITA, inVisita);
        outState.putDouble(ARG_LATITUDINE, lat);
        outState.putDouble(ARG_LONGITUDINE, lng);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        RuntimeInfo.getInstance().setItineraryMode(false);  //serve nel caso di crash dalla mappa itinerario
        //LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,
               // new IntentFilter(TourGuideService.INTENT_ACTION));
    }

    @Override
    protected void onResume(){
        super.onResume();
        mbroadcastManager.registerReceiver(mReceiver,
                new IntentFilter(TourGuideService.INTENT_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    public void onBackPressed() {
        RuntimeInfo.getInstance().setLastCity(null);
        stopService(new Intent(CitySummaryActivity.this, TourGuideService.class));
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                SearchActivity.start(this, mCityName);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Loads this city and save that is the last selected city.
     * @param cityName
     */
    private void loadAndSaveCity(@NonNull String cityName) {
        mCityName = cityName;
        Intent service = new Intent(this, TourGuideService.class);
        service.putExtra(TourGuideService.ARG_CITY_NAME, cityName);
        startService(service);

        /*Intent intent = new Intent(DataConstants.INTENT_ACTION_EVENT);
        intent.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_LOG_IN);
        mbroadcastManager.sendBroadcast(intent);
        Log.i("City Summary", "invio broadcast");
        */

        // TODO dovrebbe essere responsabilità di CitySelectFragment salvare l'ultima città selezionata.
        MyPreferences preferences = new MyPreferences(CitySummaryActivity.this);
        preferences.put(DataConstants.PREF_KEY_CITY, cityName);

        preferences.put(DataConstants.PREF_KEY_SYNC_CITIES, 1);
        SyncUtilities.forceSync(this);
        String title = WordUtils.capitalizeFully(cityName);
        setTitle(title);
        mCollapsingToolbarLayout.setTitle(title);
        getSupportFragmentManager().beginTransaction().add(R.id.ll, CitySummaryFragment.newInstance(cityName), "sCity").commit();
    }

    @Override
    public void startCitySelectActivity() {
        Intent intent = new Intent(this, CitySelectActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    public void startCategoryDetailsActivity(String cityName, int categoryId, String categoryName) {
        if(categoryId != -3) {
            Intent intent = new Intent(this, CategoryDetailsActivity.class);
            intent.putExtra(CategoryDetailsActivity.ARG_CITY_NAME, mCityName);
            intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_ID, categoryId);
            intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_NAME, categoryName);
            if(inVisita) {
                Location location = new Location("");
                location.setLatitude(lat);
                location.setLongitude(lng);
                intent.putExtra(CategoryDetailsActivity.ARG_CURRENT_LOCATION, location);
            }
            // TODO pass current location (or do CategoryDetailsActivity finds the location itself with a Service)
//        if (mLocation != null) intent.putExtra(CategoryDetailsActivity.ARG_CURRENT_LOCATION, mLocation);
            startActivity(intent);
        }
        else {
            Intent intent = new Intent(this, ItineraryDetailsActivity.class);
            intent.putExtra(CategoryDetailsActivity.ARG_CITY_NAME, mCityName);
            intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_ID, categoryId);
            intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_NAME, categoryName);
            if(inVisita) {
                Location location = new Location("");
                location.setLatitude(lat);
                location.setLongitude(lng);
                intent.putExtra(CategoryDetailsActivity.ARG_CURRENT_LOCATION, location);
            }
            // TODO pass current location (or do CategoryDetailsActivity finds the location itself with a Service)
//        if (mLocation != null) intent.putExtra(CategoryDetailsActivity.ARG_CURRENT_LOCATION, mLocation);
            startActivity(intent);
        }
    }

    public void startItineraryDetailsActivity(String cityName, int categoryId, int cardId) {
        Intent intent = new Intent(this, ItineraryDetailsActivity.class);
        intent.putExtra(CategoryDetailsActivity.ARG_CITY_NAME, cityName);
        intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_ID, categoryId);
        intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_NAME, cityName);
        intent.putExtra(ItineraryDetailsActivity.ARG_ITINERARY_ID, cardId);

        // TODO pass current location (or do CategoryDetailsActivity finds the location itself with a Service)
//      if (mLocation != null) intent.putExtra(CategoryDetailsActivity.ARG_CURRENT_LOCATION, mLocation);
        startActivity(intent);
    }

    @Override
    public void onCityCursorLoadFinished(Cursor data) {
        if (data.moveToFirst()) {
            String imageFileName = data.getString(data.getColumnIndex(CityContract.Entry.COL_IMAGE));
            mNetworkImageView.setImageUrl(City.getImageUrl(imageFileName), mImageLoader);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(this, MapsActivity.class);
                intent.putExtra(MapsActivity.ARG_CITY_NAME, mCityName);

                if(inVisita){
                    Location location = new Location("Actual position");
                    location.setLatitude(lat);
                    location.setLongitude(lng);
                    intent.putExtra(MapsActivity.ARG_IN_VISITA, location);
                }
                startActivity(intent);
        }
    }

}

