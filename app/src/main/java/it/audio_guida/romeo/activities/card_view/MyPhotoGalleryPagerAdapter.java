package it.audio_guida.romeo.activities.card_view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.util.CircularArray;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.network.VolleySingleton;

import java.util.ArrayList;

/**
 * PagerAdapter that shows a simple photo gallery
 * Created by christian on 17/08/16.
 */
class MyPhotoGalleryPagerAdapter extends PagerAdapter {

    private final Context mContext;
    private final ArrayList<String> mPhotoUrls;
    private final ImageLoader mImageLoader;
    private final String videoUrl;
    private final boolean thereIsVideo;
    private final int cardCode;

    public MyPhotoGalleryPagerAdapter(Context mContext, ArrayList<String> mPhotoUrls, String video, int cardCode) {
        this.mContext = mContext;
        this.mPhotoUrls = mPhotoUrls;
        this.videoUrl = video;
        this.thereIsVideo = (video!=null && !video.isEmpty())? true: false;
        this.cardCode = cardCode;
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
    }

    @Override
    public int getCount() {
        return mPhotoUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.item_photo, container, false);
        NetworkImageView networkImageView = (NetworkImageView) v.findViewById(R.id.image);
        networkImageView.setImageUrl(mPhotoUrls.get(position), mImageLoader);
        if(thereIsVideo && position == 0) {
            Resources res = mContext.getResources();
            Drawable drawable;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                drawable = res.getDrawable(R.drawable.play, null);
                networkImageView.setForeground(drawable);
            } else {
                drawable = res.getDrawable(R.drawable.play);
                networkImageView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_play_arrow));
                
            }
            //networkImageView.setForeground(drawable);
        }
        //networkImageView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.map_evidenziato_inmappa));
        container.addView(v);
        networkImageView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(thereIsVideo && position == 0)
                            startActivityVideo();

                    }
                }
        );
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        container.removeView((View) view);
    }

    private void startActivityVideo(){
        Intent intent = new Intent(mContext, VideoActivity.class);
        intent.putExtra(VideoActivity.VIDEO_URL, videoUrl);
        intent.putExtra(VideoActivity.CARD_ID, cardCode);
        mContext.startActivity(intent);
    }
}
