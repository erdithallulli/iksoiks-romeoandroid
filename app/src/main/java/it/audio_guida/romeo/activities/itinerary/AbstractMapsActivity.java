package it.audio_guida.romeo.activities.itinerary;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.GridBasedAlgorithm;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.data.contracts.ItineraryNodeContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.runtime_info.ItineraryInfo;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 19/08/16.
 */
public abstract class AbstractMapsActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        /*ClusterManager.OnClusterItemClickListener<MyItem>,*/
        GoogleMap.OnMapClickListener,
        OnMapReadyCallback {

    public static final String ARG_CITY_NAME = "city_name";
    private static final int LOADER_TAPPE = 0;
    private static final int LOADER_CITY = 1;
    protected String mCityName;
    protected GoogleMap mMap;
    protected Cursor mCityCursor = null;
    protected Cursor mCardsCursor = null;
    protected HashMap<Integer, MyItem> mCardIdToMyItemHashMap = new HashMap<>(100);
    protected ArrayList<MyItem> myItemArrayList= new ArrayList<>(100);
    protected ClusterManager<MyItem> mClusterManager;
    private int[] mServicesHidden;
    private int[] mCategories;
    private boolean mMoveMapCameraExecuted = false;
    private boolean mMapDrawed = false;
    protected int currentPosition = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCityName = getIntent().getStringExtra(ARG_CITY_NAME);
        if (mCityName == null) throw new IllegalArgumentException("Too few arguments");

        MyPreferences preferences = new MyPreferences(this);
        mServicesHidden = preferences.getIntArray(DataConstants.PREF_KEY_SERVICES_HIDDEN, new int[0]);

        setContentView(R.layout.activity_itinerary);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getSupportLoaderManager().initLoader(LOADER_CITY, null, this);
        getSupportLoaderManager().initLoader(LOADER_TAPPE, null, this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && !mMapDrawed) {
            mMapDrawed = true;
            maybeIsReady();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException ignored) {
        }
        googleMap.setBuildingsEnabled(false);
        googleMap.setTrafficEnabled(false);

        double lat = 44.4939554592719;
        double lng = 11.3431651806546;
        googleMap.addPolyline(new PolylineOptions().geodesic(true)
                .add(new LatLng(lat, lng))  // Sydney
                .add(new LatLng(lat+0.01, lng+0.01))  // Fiji
        );

        /*mClusterManager = new ClusterManager<>(this, mMap);
        MyClusterRenderer view = new MyClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(view);
        mClusterManager.setAlgorithm(new GridBasedAlgorithm<MyItem>());

        //Point the map's listeners at the listeners implemented by the cluster manager.
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setOnClusterItemClickListener(this);
        */
        mMap.setOnMapClickListener(this);

        maybeIsReady();
    }

    protected void putCardsCursorToMap() {
        mMap.clear();
        /*mClusterManager.clearItems();
        mCardIdToMyItemHashMap.clear();
        */

        Cursor cursor = mCardsCursor;

        int colId;
        int colLat;
        int colLng;
        int colName;
        int colCatId;
        int colServiceId;
        int colIcon;

        int codeIndex = cursor.getColumnIndex(ItineraryNodeContract.Entry.COL_SCHEDA_CODE);
        int cardCode;
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            cardCode = cursor.getInt(codeIndex);

            Cursor c = getContentResolver().query(CardContract.URI, new String[]{
                    CardContract.Entry._ID,
                    CardContract.Entry.COL_NAME,
                    CardContract.Entry.COL_LAT,
                    CardContract.Entry.COL_LNG,
                    CardContract.Entry.COL_PHOTO_1,
                    CardContract.Entry.COL_SERVICE_ID,
                    CardContract.Entry.COL_CAT_ID,
                    ServiceContract.Entry.COL_NAME,
                    ServiceContract.Entry.COL_ICON},
                    CardContract.Entry._ID +" = ?", new String[]{Integer.toString(cardCode)}, null);

            c.moveToFirst();

            colId = c.getColumnIndex(CardContract.Entry._ID);
            colLat = c.getColumnIndex(CardContract.Entry.COL_LAT);
            colLng = c.getColumnIndex(CardContract.Entry.COL_LNG);
            colName = c.getColumnIndex(CardContract.Entry.COL_NAME);
            colCatId = c.getColumnIndex(CardContract.Entry.COL_CAT_ID);
            colServiceId = c.getColumnIndex(CardContract.Entry.COL_SERVICE_ID);
            colIcon = c.getColumnIndex(ServiceContract.Entry.COL_ICON);

            int catId = c.getInt(colCatId);
            int serviceId = c.getInt(colServiceId);
            if (!ArrayUtils.contains(mCategories, catId) || ArrayUtils.contains(mServicesHidden, serviceId)) {
                continue;
            }
            int id = c.getInt(colId);
            double lat = c.getDouble(colLat);
            double lng = c.getDouble(colLng);
            String icon = c.getString(colIcon);
            MyItem myItem = new MyItem(id, lat, lng, icon);
            /*mCardIdToMyItemHashMap.put(id, myItem);
            myItemArrayList.add(myItem);
            mClusterManager.addItem(myItem);
            */
        }

        //mClusterManager.cluster();

    }

    protected void moveMapCamera() {
        if(mMoveMapCameraExecuted) return;

        mMoveMapCameraExecuted = true;
        if (mCityCursor.moveToFirst()) {
            double neLat = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_NE_LAT));
            double neLng = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_NE_LON));
            double soLat = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_SO_LAT));
            double soLng = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_SO_LON));
            LatLng neLatLng = new LatLng(neLat, neLng);
            LatLng soLatLng = new LatLng(soLat, soLng);
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(new LatLngBounds(soLatLng, neLatLng), 10));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mServicesHidden = data.getIntArrayExtra(LayersSelectActivity.ARG_RESULT_SERVICES_HIDDEN);
        MyPreferences preferences = new MyPreferences(this);
        preferences.put(DataConstants.PREF_KEY_SERVICES_HIDDEN, mServicesHidden);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_CITY:
                return new CursorLoader(this, CityContract.URI, new String[]{
                        CityContract.Entry._ID,
                        CityContract.Entry.COL_CATEGORIES,
                        CityContract.Entry.COL_SO_LAT,
                        CityContract.Entry.COL_SO_LON,
                        CityContract.Entry.COL_NE_LAT,
                        CityContract.Entry.COL_NE_LON,
                }, CityContract.Entry.COL_NAME + " = ?", new String[]{mCityName}, "");
            case LOADER_TAPPE:
                return new CursorLoader(this, ItineraryNodeContract.URI, new String[]{
                        ItineraryNodeContract.Entry._ID,
                        ItineraryNodeContract.Entry.COL_NAME,
                        ItineraryNodeContract.Entry.COL_SCHEDA_CODE,
                }, ItineraryNodeContract.Entry.COL_SCHEDA_CODE +" != 0", null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_CITY:
                onCityLoadFinished(data);
                break;
            case LOADER_TAPPE:
                onCardsLoadFinished(data);
                break;
        }
    }

    protected void onCityLoadFinished(Cursor data) {
        mCityCursor = data;
        if (data != null && data.moveToFirst()) {
            mCategories = City.getCategoriesFromEncodedString(
                    data.getString(data.getColumnIndex(CityContract.Entry.COL_CATEGORIES))
            );
        }
        maybeIsReady();
    }

    protected void onCardsLoadFinished(Cursor data) {
        mCardsCursor = data;
        maybeIsReady();
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        onLoadFinished(loader, null);
    }

    //TODO soluzione provvisoria con hiddenservices, da modificare
    @Override
    protected void onStart() {
        super.onStart();
        MyPreferences preferences = new MyPreferences(this);
        mServicesHidden = preferences.getIntArray(DataConstants.PREF_KEY_SERVICES_HIDDEN, new int[0]);
        maybeIsReady();
    }


    protected void startLayersSelectActivity() {
        Intent intent = new Intent(this, LayersSelectActivity.class);
        intent.putExtra(LayersSelectActivity.ARG_CITY_NAME, mCityName);
        intent.putExtra(LayersSelectActivity.ARG_CATEGORIES, mCategories);
        startActivityForResult(intent, 0);
    }

    /*
    @Override
    public abstract boolean onClusterItemClick(MyItem myItem);*/

    @Override
    public abstract void onMapClick(LatLng latLng);

    private void maybeIsReady() {
        if (isReady()) onReady();
    }

    protected boolean isReady() {
        return mMap != null && mMapDrawed && mCardsCursor != null && mCityCursor != null;
    }

    protected void onReady() {
        moveMapCamera();
        putCardsCursorToMap();
    }
}