package it.audio_guida.romeo.activities.itinerary_details;

import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;


import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.category_details.CategoryDetailsActivity;
import it.audio_guida.romeo.activities.category_details.CategoryDetailsSectionsAdapter;
import it.audio_guida.romeo.activities.itinerary.ItineraryActivity;
import it.audio_guida.romeo.activities.map.MapsActivity;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.data.contracts.PreferencesContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.network.VolleySingleton;
import it.audio_guida.romeo.sync.SyncUtilities;

public class ItineraryDetailsActivity extends CategoryDetailsActivity{

    public static String ARG_ITINERARY_ID = "id_itinerary";

    private ImageLoader mImageLoader;
    private NetworkImageView mNetworkImageView;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;

    private String mCityName;
    private int mCategoryId;
    private String mCategoryName;
    private Location mCurrentLocation;
    private CategoryDetailsSectionsAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private String city;
    private LocalBroadcastManager mBroadcaster;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        city = "";
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        Intent intent = getIntent();

        setContentView(R.layout.activity_itinerary_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mNetworkImageView = (NetworkImageView) findViewById(R.id.header_image);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        setSupportActionBar(toolbar);

        TextView mfirstTextView = (TextView) findViewById(R.id.text_title);
        TextView msecondTextView = (TextView) findViewById(R.id.text_description);

        int id = intent.getIntExtra(ItineraryDetailsActivity.ARG_ITINERARY_ID, -1);


        MyPreferences preferences = new MyPreferences(getContentResolver());
        preferences.put(DataConstants.LAST_OPENED_ITINERARY, id);
        SyncUtilities.forceSync(this);

        Cursor c = getContentResolver().query(ItineraryContract.URI, new String[]{
                        ItineraryContract.Entry._ID,
                        ItineraryContract.Entry.COL_NAME,
                        ItineraryContract.Entry.COL_LUNGHEZZA,
                        ItineraryContract.Entry.COL_H_DURATA,
                        ItineraryContract.Entry.COL_TIPO,
                        ItineraryContract.Entry.COL_N_SITI,
                        ItineraryContract.Entry.COL_IMAGE,
                        ItineraryContract.Entry.COL_COMUNE},
                ItineraryContract.Entry._ID +" = ?", new String[]{Integer.toString(id)}, null);

        c.moveToFirst();

        mfirstTextView.setText(c.getString(c.getColumnIndex(ItineraryContract.Entry.COL_TIPO)));
        //mfirstTextView.setText("Itinerario a piedi");

        int lunghezza = c.getInt(c.getColumnIndex(ItineraryContract.Entry.COL_LUNGHEZZA));
        int nSiti = c.getInt(c.getColumnIndex(ItineraryContract.Entry.COL_N_SITI));
        String tempo = c.getString(c.getColumnIndex(ItineraryContract.Entry.COL_H_DURATA));
        String image = c.getString(c.getColumnIndex(ItineraryContract.Entry.COL_IMAGE));
        String name =c.getString(c.getColumnIndex(ItineraryContract.Entry.COL_NAME));
        city =c.getString(c.getColumnIndex(ItineraryContract.Entry.COL_COMUNE));

        msecondTextView.setText(
                        "Lunghezza(mt): "+lunghezza+", " +
                        "Tempo di percorrenza: "+tempo+", " +
                        "Numero siti: "+ nSiti);

        setTitle(name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase());

        mNetworkImageView.setImageUrl("http://195.120.231.180/Palmipedo/ArchivioCitta/ImgItinerari/H-"+image, mImageLoader);

        mCityName = intent.getStringExtra(CategoryDetailsActivity.ARG_CITY_NAME);
        mCategoryId = intent.getIntExtra(CategoryDetailsActivity.ARG_CATEGORY_ID, -1);
        mCategoryName = intent.getStringExtra(CategoryDetailsActivity.ARG_CATEGORY_NAME);
        if (mCityName == null || mCategoryId == -1 || mCategoryName == null || id == -1)
            throw new RuntimeException("Activity extras insufficient");
        mCurrentLocation = intent.getParcelableExtra(CategoryDetailsActivity.ARG_CURRENT_LOCATION);

        //setContentView(R.layout.activity_category_details);

        mSectionsPagerAdapter = new CategoryDetailsSectionsAdapter(
                this, getSupportFragmentManager(), mCityName, mCategoryId, mCurrentLocation);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mBroadcaster = LocalBroadcastManager.getInstance(this);
        Intent i = new Intent(DataConstants.INTENT_ACTION_EVENT);
        i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_OPENED_CARD);
        i.putExtra(DataConstants.ARG_COD_CAT, id);
        i.putExtra(DataConstants.ARG_CITY_NAME, city);
        i.putExtra(DataConstants.ARG_AUDIO_NAME, name);
        i.putExtra(DataConstants.ARG_IS_ITINERARY, true);  //serve per invio log dal dettaglio degli itinerari
        mBroadcaster.sendBroadcast(i);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_search).setVisible(false);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(this, ItineraryActivity.class);
                intent.putExtra(ItineraryActivity.ARG_CARD_ID, -2);
                intent.putExtra(MapsActivity.ARG_CITY_NAME, city);
                intent.putExtra(ItineraryActivity.ARG_ITINERARY_NAME, mCityName);
                startActivity(intent);
        }
    }

}
