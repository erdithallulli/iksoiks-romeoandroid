package it.audio_guida.romeo.activities.category_details;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;

public class CategoryDetailsFragmentItinerary extends AbstractCategoryDetailsFragment {
    @Override
    protected String getSortOrder() {
        return null;
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        //invece di inserire l'if fare l'overloading nella versione definitiva
        //if(mCategoryId == -2)
            return new CursorLoader(getContext(), ItineraryContract.URI, getProjectionItinerary(), null, null, null);
        //return new CursorLoader(getContext(), CardContract.URI, getProjection(), getSelection(), getSelectionArgs(), getSortOrder());
    }
}
