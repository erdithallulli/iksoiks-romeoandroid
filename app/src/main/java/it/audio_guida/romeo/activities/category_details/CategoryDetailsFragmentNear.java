package it.audio_guida.romeo.activities.category_details;

import it.audio_guida.romeo.data.contracts.CardContract;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 21/08/16.
 */
public class CategoryDetailsFragmentNear extends AbstractCategoryDetailsFragment {

    @Override
    protected String[] getProjection() {
        String fieldCalculatedDistance = CardContract.Entry.getFieldCalculatedDistance(
                mLocation.getLatitude(), mLocation.getLongitude());
        return ArrayUtils.add(super.getProjection(), fieldCalculatedDistance);
    }

    @Override
    protected String getSortOrder() {
        return CardContract.Entry.CALCULATED_DISTANCE + " ASC";
    }
}
