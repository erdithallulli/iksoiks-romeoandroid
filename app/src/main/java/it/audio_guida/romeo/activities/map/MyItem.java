package it.audio_guida.romeo.activities.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.model.CategoryService;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 16/08/16.
 */
class MyItem implements ClusterItem, Cloneable {
    private final int mId;
    private final String mIcon;
    private LatLng mPosition;
    private boolean mEm = false;

    MyItem(int id, double lat, double lng, String icon) {
        mId = id;
        mIcon = icon;
        mPosition = new LatLng(lat, lng);
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public int getId() {
        return mId;
    }

    public boolean isEm() {
        return mEm;
    }

    public void setEm(boolean em) {
        mEm = em;
    }

    int getIconResourceId() {
        int iconResourceId = CategoryService.getIconResourceId(mIcon);
        if (iconResourceId == -1) iconResourceId = R.drawable.map_100manca;
        return iconResourceId;
    }

    BitmapDescriptor getIconBitmapDescriptor(Context context) {
        int iconResourceId = getIconResourceId();
        if (!mEm) return BitmapDescriptorFactory.fromResource(iconResourceId);

        Bitmap baseBitmap = BitmapFactory.decodeResource(context.getResources(), iconResourceId);
        Bitmap emphBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.map_evidenziato_inmappa);
        Bitmap drawnBitmap = baseBitmap.copy(baseBitmap.getConfig(), true);
        Canvas c = new Canvas(drawnBitmap);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        c.drawBitmap(emphBitmap, 0, 0, paint);
        return BitmapDescriptorFactory.fromBitmap(drawnBitmap);
    }

    public MyItem clone() {
        try {
            MyItem clone = (MyItem) super.clone();
            clone.mPosition = new LatLng(mPosition.latitude, mPosition.longitude);
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
