package it.audio_guida.romeo.activities.category_details;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import it.audio_guida.romeo.data.contracts.*;

public class CategoryDetailsFragmentItineraryNode extends AbstractCategoryDetailsFragment {
    @Override
    protected String getSortOrder() {
        return null;
    }

    protected String[] getProjectionItinerary() {
        return null;
        /*return new String[]{
                ItineraryNodeContract.Entry._ID,
                ItineraryNodeContract.Entry.COL_NAME,
                ItineraryNodeContract.Entry.COL_H_DURATA,
                ItineraryNodeContract.Entry.COL_N_DISTANZA,
        };
        */
    }

    protected String getSelection() {
        return ItineraryNodeContract.Entry.COL_SCHEDA_CODE +" != 0";
    }

    protected String[] getProjection() {
        return new String[]{
                ItineraryNodeContract.Entry._ID,
                ItineraryNodeContract.Entry.COL_NAME,
                ItineraryNodeContract.Entry.COL_H_DURATA,
                ItineraryNodeContract.Entry.COL_N_DISTANZA,
                ItineraryNodeContract.Entry.COL_SCHEDA_CODE
        };
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args){
        return new CursorLoader(getContext(), ItineraryNodeContract.URI, getProjection(),
                getSelection(), null, null);
    }
}
