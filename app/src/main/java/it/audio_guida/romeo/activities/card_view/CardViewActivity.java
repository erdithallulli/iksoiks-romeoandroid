package it.audio_guida.romeo.activities.card_view;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.data.contracts.ItineraryNodeContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.view_libs.MediaPlayerUIControlManager;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * An activity that shows details of a single card. Allows swiping between related cards defined by activity arguments.
 */
public class CardViewActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, ViewPager.OnPageChangeListener {

    public static final String ARG_SORT_ORDER = "sort";
    public static final String ARG_SELECTION_ARGS = "sel_args";
    public static final String ARG_SELECTION = "selection";
    public static final String ARG_PROJECTION = "projection";
    public static final String ARG_ID = "_id";
    public static final String ARG_POSITION = "position";

    public static final String ARG_MSEC = "msec";

    //if !=0 is a card from an itinerary
    public static final String ARG_ITINERARY = "itinerary";

    private CardsViewFragmentStatePagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    private int mCardId;

    // Position of the Cursor that must be showed when launching this activity
    private int mCardPosition;

    // Properties of the query used for swiping cards
    private String[] mProjection;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mSortOrder;

    private boolean itinerary;

    private MediaPlayerUIControlManager mMediaPlayerUIControlManager;
    private LocalBroadcastManager mBroadcaster;

    private boolean onStopSetted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCardId = getIntent().getIntExtra(ARG_ID, 1);
        mCardPosition = getIntent().getIntExtra(ARG_POSITION, 0);
        mProjection = getIntent().getStringArrayExtra(ARG_PROJECTION);
        mSelection = getIntent().getStringExtra(ARG_SELECTION);
        mSelectionArgs = getIntent().getStringArrayExtra(ARG_SELECTION_ARGS);
        mSortOrder = getIntent().getStringExtra(ARG_SORT_ORDER);

        itinerary = getIntent().getIntExtra(ARG_ITINERARY, 0) != 0;

        mBroadcaster = LocalBroadcastManager.getInstance(this);

        setContentView(R.layout.activity_card_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSectionsPagerAdapter = new CardsViewFragmentStatePagerAdapter(this, getSupportFragmentManager(), null);
        mMediaPlayerUIControlManager = new MediaPlayerUIControlManager(this, R.id.media_player_controls, findViewById(R.id.main_content), false);

        mMediaPlayerUIControlManager.onStart();

        onStopSetted = false;

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(this);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_card_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Joins the projection defined by mProjection with a set of mandatory columns.
     *
     * @return the projection defined mProjection with a set of mandatory columns.
     */
    private String[] getMergedProjection() {
        String[] baseProjection = {
                CardContract.Entry._ID,
                CardContract.Entry.COL_NAME,
                CardContract.Entry.COL_CITY,
                CardContract.Entry.COL_TEXT,
                CardContract.Entry.COL_PHOTO_1,
                CardContract.Entry.COL_PHOTO_2,
                CardContract.Entry.COL_PHOTO_3,
                CardContract.Entry.COL_LAT,
                CardContract.Entry.COL_LNG,
                CardContract.Entry.COL_VIDEO,
                ServiceContract.Entry.COL_NAME
        };
        if (mProjection == null) return baseProjection;

        Set<String> projectionSet = new LinkedHashSet<>(Arrays.asList(baseProjection));
        projectionSet.addAll(Arrays.asList(mProjection));

        String[] generatedProjection = new String[projectionSet.size()];
        return projectionSet.toArray(generatedProjection);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if(itinerary) {
            return new CursorLoader(this, ItineraryNodeContract.URI, mProjection, mSelection, null, null);
        }
        if (mSelection == null) mSelection = CardContract.Entry._ID + " = " + mCardId;
        return new CursorLoader(this, CardContract.URI, getMergedProjection(), mSelection, mSelectionArgs, mSortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mSectionsPagerAdapter.swapCursor(data);
        mViewPager.setCurrentItem(mCardPosition, false);

        // ViewPager don't call onPageSelected if page is not changed
        if (mCardPosition == 0) onPageSelected(0);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mSectionsPagerAdapter.swapCursor(null);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        int cardId = mSectionsPagerAdapter.getPageId(position);
        mCardId = cardId;
        setTitle(mSectionsPagerAdapter.getPageTitle(position));

        mMediaPlayerUIControlManager.changeCardIdControlled(cardId);
        sendLog();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void stopMediaPlayer(){
        Log.i("CardViewActivity", "stopMediaPlayer()");
        if(!onStopSetted) {
            mMediaPlayerUIControlManager.onStop();
            onStopSetted = true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyPreferences preferences = new MyPreferences(this);
        preferences.put(DataConstants.PREF_KEY_SERVICES_HIDDEN, new int[0]);
        if(onStopSetted) {
            mMediaPlayerUIControlManager.onStart();
            onStopSetted = false;
        }
    }

    @Override
    protected void onStop() {
        //mMediaPlayerUIControlManager.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        mMediaPlayerUIControlManager.onStop();
        super.onDestroy();
    }



    //isPlayEvent è true se il metodo è chiamato dopo il click su play e false se è solo visualizzazione
    private void sendLog(){
        Cursor query = getContentResolver().query(CardContract.URI, new String[]{CardContract.Entry._ID,
                        CardContract.Entry.COL_SERVICE_ID,CardContract.Entry.COL_CAT_ID, CardContract.Entry.COL_CITY,
                        CardContract.Entry.COL_AUDIO, CardContract.Entry.COL_TEXT, CardContract.Entry.COL_DATA_EXISTS},
                CardContract.Entry._ID +" = ?", new String[]{Integer.toString(mCardId)}, null);
        int service_id = 0;
        int cat_id = 0;
        String name = "";
        String audio = "";
        String text = "";
        String dataExists = "";
        if(query != null) {
            query.moveToFirst();
            int index = query.getColumnIndex(CardContract.Entry.COL_SERVICE_ID);
            service_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CAT_ID);
            cat_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CITY);
            name = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_AUDIO);
            audio = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_TEXT);
            text = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_DATA_EXISTS);
            dataExists = query.getString(index);
        }

        int flagPosition = 0;
        boolean audioExists = false;

        //serve per il path
        boolean textExists = false;
        if(dataExists!=null || !dataExists.isEmpty())
            for(int i=0; i<dataExists.length(); ++i){
                if(dataExists.charAt(i)=='0'||dataExists.charAt(i)=='1'){
                    flagPosition++;
                    if(flagPosition==1 && dataExists.charAt(i)=='1')
                        audioExists = true;
                    else if(flagPosition==2 && dataExists.charAt(i)=='1')
                        textExists = true;
                }
            }

        Intent i = new Intent(DataConstants.INTENT_ACTION_EVENT);
        i.putExtra(DataConstants.ARG_ID, mCardId);
        i.putExtra(DataConstants.ARG_COD_CAT, cat_id);
        i.putExtra(DataConstants.ARG_COD_SER, service_id);
        i.putExtra(DataConstants.ARG_CITY_NAME, name);
        i.putExtra(DataConstants.ARG_IS_ITINERARY, false);  //serve per invio log dal dettaglio degli itinerari
        i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_OPENED_CARD);
        i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getTextUrl(name, text));
        mBroadcaster.sendBroadcast(i);
    }
}