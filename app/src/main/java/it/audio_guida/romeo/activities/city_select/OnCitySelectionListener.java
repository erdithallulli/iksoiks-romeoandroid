package it.audio_guida.romeo.activities.city_select;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 09/08/16.
 */
public interface OnCitySelectionListener {
    void citySelected(String name, boolean inVisita, double latitudine, double longitudine);
}
