package it.audio_guida.romeo.activities.map;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.GridBasedAlgorithm;

import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.City;

import org.apache.commons.lang3.ArrayUtils;

import java.util.HashMap;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 19/08/16.
 */
public abstract class AbstractMapsActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        ClusterManager.OnClusterItemClickListener<MyItem>,
        GoogleMap.OnMapClickListener,
        OnMapReadyCallback {

    public static final String ARG_CITY_NAME = "city_name";
    private static final int LOADER_CARDS = 0;
    private static final int LOADER_CITY = 1;
    protected String mCityName;
    protected GoogleMap mMap;
    protected Cursor mCityCursor = null;
    protected Cursor mCardsCursor = null;
    protected HashMap<Integer, MyItem> mCardIdToMyItemHashMap = new HashMap<>(100);
    protected ClusterManager<MyItem> mClusterManager;
    private int[] mServicesHidden;
    private int[] mCategories;
    private boolean mMoveMapCameraExecuted = false;
    private boolean mMapDrawed = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCityName = getIntent().getStringExtra(ARG_CITY_NAME);
        if (mCityName == null) throw new IllegalArgumentException("Too few arguments");

        MyPreferences preferences = new MyPreferences(this);
        mServicesHidden = preferences.getIntArray(DataConstants.PREF_KEY_SERVICES_HIDDEN, new int[0]);

        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getSupportLoaderManager().initLoader(LOADER_CITY, null, this);
        getSupportLoaderManager().initLoader(LOADER_CARDS, null, this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && !mMapDrawed) {
            mMapDrawed = true;
            maybeIsReady();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException ignored) {
        }
        googleMap.setBuildingsEnabled(false);
        googleMap.setTrafficEnabled(false);
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setMapToolbarEnabled(true);

        mMap.getUiSettings().setMapToolbarEnabled(true);
        mClusterManager = new ClusterManager<>(this, mMap);
        MyClusterRenderer view = new MyClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(view);
        mClusterManager.setAlgorithm(new GridBasedAlgorithm<MyItem>());
        // Point the map's listeners at the listeners implemented by the cluster manager.
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setOnClusterItemClickListener(this);
        mMap.setOnMapClickListener(this);

        Cursor c = getContentResolver().query(CityContract.URI, new String[]{
                CityContract.Entry._ID,
                CityContract.Entry.COL_NAME,
                CityContract.Entry.COL_USER_MAP
        }, CityContract.Entry.COL_NAME+ " = ?", new String[]{mCityName}, null);

        c.moveToFirst();
        String userMap = c.getString(c.getColumnIndex(CityContract.Entry.COL_USER_MAP));

        if(userMap!=null && userMap.toLowerCase().equals("si"))
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        maybeIsReady();
    }

    protected void putCardsCursorToMap() {
        mMap.clear();
        mClusterManager.clearItems();
        mCardIdToMyItemHashMap.clear();
        Cursor c = mCardsCursor;

        int colId = c.getColumnIndex(CardContract.Entry._ID);
        int colLat = c.getColumnIndex(CardContract.Entry.COL_LAT);
        int colLng = c.getColumnIndex(CardContract.Entry.COL_LNG);
        int colName = c.getColumnIndex(CardContract.Entry.COL_NAME);
        int colCatId = c.getColumnIndex(CardContract.Entry.COL_CAT_ID);
        int colServiceId = c.getColumnIndex(CardContract.Entry.COL_SERVICE_ID);
        int colIcon = c.getColumnIndex(ServiceContract.Entry.COL_ICON);

        c.moveToPosition(-1);
        while (c.moveToNext()) {
            int catId = c.getInt(colCatId);
            int serviceId = c.getInt(colServiceId);
            if (!ArrayUtils.contains(mCategories, catId) || ArrayUtils.contains(mServicesHidden, serviceId)) {
                continue;
            }
            int id = c.getInt(colId);
            double lat = c.getDouble(colLat);
            double lng = c.getDouble(colLng);
            String icon = c.getString(colIcon);
            MyItem myItem = new MyItem(id, lat, lng, icon);
            mCardIdToMyItemHashMap.put(id, myItem);
            mClusterManager.addItem(myItem);
        }
        mClusterManager.cluster();
    }

    protected void moveMapCamera() {
        if (mMoveMapCameraExecuted) return;

        mMoveMapCameraExecuted = true;
        if (mCityCursor.moveToFirst()) {
            double neLat = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_NE_LAT));
            double neLng = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_NE_LON));
            double soLat = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_SO_LAT));
            double soLng = mCityCursor.getDouble(mCityCursor.getColumnIndex(CityContract.Entry.COL_SO_LON));
            LatLng neLatLng = new LatLng(neLat, neLng);
            LatLng soLatLng = new LatLng(soLat, soLng);
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(new LatLngBounds(soLatLng, neLatLng), 10));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mServicesHidden = data.getIntArrayExtra(LayersSelectActivity.ARG_RESULT_SERVICES_HIDDEN);
        MyPreferences preferences = new MyPreferences(this);
        preferences.put(DataConstants.PREF_KEY_SERVICES_HIDDEN, mServicesHidden);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_CITY:
                return new CursorLoader(this, CityContract.URI, new String[]{
                        CityContract.Entry._ID,
                        CityContract.Entry.COL_CATEGORIES,
                        CityContract.Entry.COL_SO_LAT,
                        CityContract.Entry.COL_SO_LON,
                        CityContract.Entry.COL_NE_LAT,
                        CityContract.Entry.COL_NE_LON,
                }, CityContract.Entry.COL_NAME + " = ?", new String[]{mCityName}, "");
            case LOADER_CARDS:
                return new CursorLoader(this, CardContract.URI, new String[]{
                        CardContract.Entry._ID,
                        CardContract.Entry.COL_NAME,
                        CardContract.Entry.COL_LAT,
                        CardContract.Entry.COL_LNG,
                        CardContract.Entry.COL_PHOTO_1,
                        CardContract.Entry.COL_SERVICE_ID,
                        CardContract.Entry.COL_CAT_ID,
                        ServiceContract.Entry.COL_NAME,
                        ServiceContract.Entry.COL_ICON,
                }, CardContract.Entry.COL_CITY + " = ?", new String[]{mCityName}, "");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_CITY:
                onCityLoadFinished(data);
                break;
            case LOADER_CARDS:
                onCardsLoadFinished(data);
                break;
        }
    }

    protected void onCityLoadFinished(Cursor data) {
        mCityCursor = data;
        if (data != null && data.moveToFirst()) {
            mCategories = City.getCategoriesFromEncodedString(
                    data.getString(data.getColumnIndex(CityContract.Entry.COL_CATEGORIES))
            );
        }
        maybeIsReady();
    }

    protected void onCardsLoadFinished(Cursor data) {
        mCardsCursor = data;
        maybeIsReady();
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        onLoadFinished(loader, null);
    }

    //TODO soluzione provvisoria con hiddenservices, da modificare
    @Override
    protected void onStart() {
        super.onStart();
        MyPreferences preferences = new MyPreferences(this);
        mServicesHidden = preferences.getIntArray(DataConstants.PREF_KEY_SERVICES_HIDDEN, new int[0]);
        maybeIsReady();
    }


    protected void startLayersSelectActivity() {
        Intent intent = new Intent(this, LayersSelectActivity.class);
        intent.putExtra(LayersSelectActivity.ARG_CITY_NAME, mCityName);
        intent.putExtra(LayersSelectActivity.ARG_CATEGORIES, mCategories);
        startActivityForResult(intent, 0);
    }

    @Override
    public abstract boolean onClusterItemClick(MyItem myItem);

    @Override
    public abstract void onMapClick(LatLng latLng);

    private void maybeIsReady() {
        if (isReady()) onReady();
    }

    protected boolean isReady() {
        return mMap != null && mMapDrawed && mCardsCursor != null && mCityCursor != null;
    }

    protected void onReady() {
        moveMapCamera();
        putCardsCursorToMap();
    }
}
