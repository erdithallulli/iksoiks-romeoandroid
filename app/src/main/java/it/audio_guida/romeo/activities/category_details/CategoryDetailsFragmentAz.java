package it.audio_guida.romeo.activities.category_details;

import it.audio_guida.romeo.data.contracts.CardContract;

public class CategoryDetailsFragmentAz extends AbstractCategoryDetailsFragment {

    @Override
    protected String getSortOrder() {
        return CardContract.Entry.COL_NAME + " ASC";
    }
}
