package it.audio_guida.romeo.activities.map;

import android.Manifest;
import android.content.*;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import it.audio_guida.romeo.ITourGuideInterface;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.card_view.CardViewActivity;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.network.VolleySingleton;
import it.audio_guida.romeo.tour_guide.TourGuideService;
import it.audio_guida.romeo.view_libs.MediaPlayerUIControlManager;

import static it.audio_guida.romeo.tour_guide.TourGuideService.MESSAGE_AUDIO_READY;
import static it.audio_guida.romeo.tour_guide.TourGuideService.MESSAGE_NEAR;

import it.audio_guida.romeo.runtime_info.ItineraryInfo;

public class MapsActivity extends AbstractMapsActivity implements View.OnClickListener, ServiceConnection {

    public static final String ARG_CARD_ID = "card_id";
    public static final String ARG_AUTO_START_SCHEDA = "card_autostart";
    public static final String ARG_IN_VISITA = "in_visita";

    private int mCardSelectedId;
    private CardView mCardView;
    private TextView mNameTextView;
    private NetworkImageView mImageView;
    private ImageLoader mImageLoader;
    private MediaPlayerUIControlManager mMediaPlayerUIControlManager;
    private int mEmpathizeCardAsyncDataCardId = -1;
    private MyItem mEmphatizedMyItem = null;
    private boolean autoplay;
    private boolean autostart;
    private CameraPosition cameraPosition;

    private boolean onStopPressed;

    private LocalBroadcastManager mBroadcaster;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String s = intent.getStringExtra(TourGuideService.ARG_MESSAGE);
            int cardId;
            switch (s) {
                case MESSAGE_NEAR:
                    cardId = intent.getIntExtra(TourGuideService.ARG_CARD_ID, -1);
                    if(cardId != -1)
                        sendLog(cardId, true, true);
                    empathizeCardAsync(cardId);
                    break;
                case MESSAGE_AUDIO_READY:
                    cardId = intent.getIntExtra(TourGuideService.ARG_CARD_ID, -1);
                    if(cardId != -1)
                        sendLog(cardId, false, true);
                    empathizeCardAsync(cardId);
                    break;
            }
        }
    };
    private boolean isSatelliteView = false;
    private ITourGuideInterface mITourGuideService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageLoader = VolleySingleton.getInstance().getImageLoader();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cameraPosition = null;

        autoplay = false;
        autostart = false;

        mCardView = (CardView) findViewById(R.id.card);
        mNameTextView = (TextView) findViewById(R.id.text_name);
        mImageView = (NetworkImageView) findViewById(R.id.image);
        mCardView.setOnClickListener(this);
        mMediaPlayerUIControlManager = new MediaPlayerUIControlManager(this, R.id.media_player_controls, mCardView, true);
        mMediaPlayerUIControlManager.onStart();
        onStopPressed = false;
        mBroadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_layers:
                startLayersSelectActivity();
                return true;
            case R.id.action_map_type:
                isSatelliteView = !isSatelliteView;
                mMap.setMapType(isSatelliteView ? GoogleMap.MAP_TYPE_SATELLITE : GoogleMap.MAP_TYPE_NORMAL);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void empathizeCardAsync(int cardId) {
        MyPreferences preferences = new MyPreferences(this);
        preferences.put(DataConstants.PREF_KEY_SERVICES_HIDDEN, new int[0]);
        if (isReady()) empathizeCard(cardId);
        else mEmpathizeCardAsyncDataCardId = cardId;
    }

    @Override
    protected void onReady() {
        super.onReady();
        Location currentLocation = getIntent().getParcelableExtra(MapsActivity.ARG_IN_VISITA);
        if(currentLocation != null){
            if(cameraPosition !=null)
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            else{
                Double lat = currentLocation.getLatitude();
                Double lng = currentLocation.getLongitude();
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 18));
            }
        }
        if (mEmpathizeCardAsyncDataCardId != -1) {
            empathizeCard(mEmpathizeCardAsyncDataCardId);
            mEmpathizeCardAsyncDataCardId = -1;
        }
    }

    private void empathizeCard(int cardId) {
        Cursor c = mCardsCursor;
        c.moveToPosition(-1);
        int colId = c.getColumnIndex(CardContract.Entry._ID);
        while (c.moveToNext()) {
            if (c.getInt(colId) == cardId) {
                double lat = c.getDouble(c.getColumnIndex(CardContract.Entry.COL_LAT));
                double lng = c.getDouble(c.getColumnIndex(CardContract.Entry.COL_LNG));
                String name = c.getString(c.getColumnIndex(CardContract.Entry.COL_NAME));
                String image = c.getString(c.getColumnIndex(CardContract.Entry.COL_PHOTO_1));

                unEmphatizeItem();
                mEmphatizedMyItem = setMyItemEmAndRedraw(mCardIdToMyItemHashMap.get(cardId), true);
                mCardIdToMyItemHashMap.put(cardId, mEmphatizedMyItem);
                mClusterManager.cluster();

                // Show card preview
                Resources r = getResources();
                int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, r.getDisplayMetrics());
                mMap.setPadding(0, 0, 0, px);
                mCardView.setVisibility(View.VISIBLE);
                mCardSelectedId = cardId;
                mMediaPlayerUIControlManager.changeCardIdControlled(cardId);
                mNameTextView.setText(name);
                mImageView.setImageUrl(Card.getPreviewImageUrl(mCityName, image), mImageLoader);

                // Animate camera to icon
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 18));
                break;
            }
        }
    }

    private void unEmphatizeItem() {
        if (mEmphatizedMyItem != null) {
            MyItem redraw = setMyItemEmAndRedraw(mEmphatizedMyItem, false);
            mCardIdToMyItemHashMap.put(redraw.getId(), redraw);
            mEmphatizedMyItem = null;
        }
    }

    private MyItem setMyItemEmAndRedraw(MyItem myItem1, boolean em) {
        mClusterManager.removeItem(myItem1);
        MyItem myItem2 = myItem1.clone();
        myItem2.setEm(em);
        mClusterManager.addItem(myItem2);
        return myItem2;
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, TourGuideService.class), this, BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,
                new IntentFilter(TourGuideService.INTENT_ACTION));
        if(onStopPressed) {
            mMediaPlayerUIControlManager.onStart();
            onStopPressed = false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        cameraPosition = mMap.getCameraPosition();
        //mMediaPlayerUIControlManager.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        unbindService(this);
    }

    @Override
    public boolean onClusterItemClick(MyItem myItem) {
        // Empathize the corresponding icon
        int id = myItem.getId();
        sendLog(id, false, false);
        empathizeCard(id);
        return false;
    }

    public void onBackPressed(){
        if(!onStopPressed) {
            mMediaPlayerUIControlManager.onStop();
            onStopPressed = true;
        }
        super.onBackPressed();
    }

    @Override
    public void onDestroy(){
        if(!onStopPressed) {
            mMediaPlayerUIControlManager.onStop();
            onStopPressed = true;
        }
        super.onDestroy();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mCardView.setVisibility(View.GONE);
        mMap.setPadding(0, 0, 0, 0);
        unEmphatizeItem();
        mClusterManager.cluster();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card:
                // Start Card View Activity
                Intent intent = new Intent(this, CardViewActivity.class);
                intent.putExtra(CardViewActivity.ARG_ID, mCardSelectedId);
                startActivity(intent);
                autostart = false;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        super.onMapReady(googleMap);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }
        else{
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }



        int cardId = getIntent().getIntExtra(MapsActivity.ARG_CARD_ID, -1);

        boolean autoStartScheda = getIntent().getBooleanExtra(ARG_AUTO_START_SCHEDA, false);
        if(autoStartScheda) {
            autostart = true;
            sendLog(cardId, false, false);
        }
        else
            autostart = false;
        if (cardId != -1) empathizeCardAsync(cardId);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mITourGuideService = ITourGuideInterface.Stub.asInterface(service);
        try {
            if (mITourGuideService.isPlaying()) {
                int cardAudioLoadedId = mITourGuideService.getCardAudioLoadedId();
                empathizeCardAsync(cardAudioLoadedId);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mITourGuideService = null;
    }

    private void sendLog(int mCardId, boolean auto, boolean branoAudio){
        if(auto)
            autoplay = true;

        Cursor query = getContentResolver().query(CardContract.URI, new String[]{CardContract.Entry._ID,
                        CardContract.Entry.COL_SERVICE_ID,CardContract.Entry.COL_CAT_ID, CardContract.Entry.COL_CITY,
                        CardContract.Entry.COL_AUDIO, CardContract.Entry.COL_TEXT, CardContract.Entry.COL_DATA_EXISTS},
                CardContract.Entry._ID +" = ?", new String[]{Integer.toString(mCardId)}, null);
        int service_id = 0;
        int cat_id = 0;
        String name = "";
        String audio = "";
        String text = "";
        String dataExists = "";
        if(query != null) {
            query.moveToFirst();
            int index = query.getColumnIndex(CardContract.Entry.COL_SERVICE_ID);
            service_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CAT_ID);
            cat_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CITY);
            name = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_AUDIO);
            audio = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_TEXT);
            text = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_DATA_EXISTS);
            dataExists = query.getString(index);
        }

        int flagPosition = 0;
        boolean audioExists = false;

        //serve per il path
        boolean textExists = false;
        if(dataExists!=null || !dataExists.isEmpty())
            for(int i=0; i<dataExists.length(); ++i){
                if(dataExists.charAt(i)=='0'||dataExists.charAt(i)=='1'){
                    flagPosition++;
                    if(flagPosition==1 && dataExists.charAt(i)=='1')
                        audioExists = true;
                    else if(flagPosition==2 && dataExists.charAt(i)=='1')
                        textExists = true;
                }
            }

        Intent i = new Intent(DataConstants.INTENT_ACTION_EVENT);
        i.putExtra(DataConstants.ARG_ID, mCardId);
        i.putExtra(DataConstants.ARG_COD_CAT, cat_id);
        i.putExtra(DataConstants.ARG_COD_SER, service_id);
        i.putExtra(DataConstants.ARG_CITY_NAME, name);
        i.putExtra(DataConstants.ARG_IS_ITINERARY, false);  //serve per invio log dal dettaglio degli itinerari

        if(branoAudio){
            if(auto) {
                i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_AUDIO_CARD_AUTO);
                if (audioExists)
                    i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getAudioUrl(name, audio));
                else
                    i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getTextUrl(name, text));
            }
            else{
                if(autoplay) {
                    autoplay = false;
                    return;
                }

                if(autostart) {
                    i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_AUDIO_CARD_AUTO);
                }
                else {
                    i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_AUDIO_CARD_PREVIEW);
                }
                if (audioExists)
                    i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getAudioUrl(name, audio));
                else
                    i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getTextUrl(name, text));

            }
            autostart = false;
        }
        else {
            i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_OPENED_CARD_PREVIEW);
            i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getTextUrl(name, text));
            autoplay = false;
        }
        mBroadcaster.sendBroadcast(i);
    }
}
