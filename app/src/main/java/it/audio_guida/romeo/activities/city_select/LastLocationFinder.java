package it.audio_guida.romeo.activities.city_select;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A class that find the last knows user location. Maybe it must be integrated with {@link it.audio_guida.romeo.tour_guide.TourGuideService}
 * Created by christian on 18/09/16.
 */
public class LastLocationFinder implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private final Context mContext;
    private final OnLastLocationFindListener mListener;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public LastLocationFinder(Context context, OnLastLocationFindListener listener) {
        mContext = context;
        mListener = listener;
    }

    protected void loadLastLocationAsync() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    /*private void selectCityBasedOnLastKnownLocation() {
        if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.M) || (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            //LocationServices.FusedLocationApi.
            if (location != null) {
                mListener.onLastLocationFind(location);
            } else {
                mListener.onLastLocationFindError("No last location info");
            }
        } else {
            mListener.onLastLocationFindError("No permission");
        }
    }
    */

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        selectCityBasedOnLastKnownLocation();
        mGoogleApiClient.disconnect();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mListener.onLastLocationFindError("Google API connection failed");
    }


    public interface OnLastLocationFindListener {
        void onLastLocationFind(Location location);

        void onLastLocationFindError(String message);
    }

    public void selectCityBasedOnLastKnownLocation() {
        Location location = null;
        try {

            LocationManager locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                mListener.onLastLocationFindError("No GPS found");
                // no network provider is enabled
            } else {
                if (isNetworkEnabled) {
                    if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.M) || (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                        location = LocationServices.FusedLocationApi.getLastLocation(
                                mGoogleApiClient);
                        if (location != null) {
                            mListener.onLastLocationFind(location);
                        } else {
                            mListener.onLastLocationFindError("No last location info");
                        }
                    }
                    else {
                        mListener.onLastLocationFindError("No permission");
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                mListener.onLastLocationFind(location);
                            } else {
                                mListener.onLastLocationFindError("No last location info");
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
