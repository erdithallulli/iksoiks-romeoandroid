package it.audio_guida.romeo.activities.card_view;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.itinerary.ItineraryActivity;
import it.audio_guida.romeo.activities.map.MapsActivity;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.data.contracts.ItineraryNodeContract;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.model.GetCardTextListener;
import it.audio_guida.romeo.sync.CityDataManagerFactory;

import java.util.ArrayList;

/**
 * A Fragment that shows a single detailed card.
 */
public class CardViewFragment extends Fragment implements View.OnClickListener, GetCardTextListener, OnMapReadyCallback {

    private TextView mTextViewCategoryName;
    private TextView mTextView;
    private ViewPager mViewPager;
    private PagerAdapter mPhotoPagerAdapter;
    private MapView mMapView;
    private int mCardId;
    private String mCityName;
    private View rootView;
    private CardViewActivity mCardViewActivity;

    public CardViewFragment() {
    }

    public static CardViewFragment newInstance(Bundle args, CardViewActivity cardViewActivity) {
        CardViewFragment fragment = new CardViewFragment();
        fragment.setArguments(args);
        fragment.setCardViewActivity(cardViewActivity);
        return fragment;
    }

    public void setCardViewActivity(CardViewActivity cardViewActivity){
        mCardViewActivity = cardViewActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle arguments = getArguments();
        mCardId = arguments.getInt(CardContract.Entry._ID);
        String textFileName = arguments.getString(CardContract.Entry.COL_TEXT);
        mCityName = arguments.getString(CardContract.Entry.COL_CITY);
        ArrayList<String> photosUrl = arguments.getStringArrayList(CardsViewFragmentStatePagerAdapter.PHOTOS_URLS);
        String video = arguments.getString(CardContract.Entry.COL_VIDEO);
        String serviceName = arguments.getString(ServiceContract.Entry.COL_NAME);
        String videoUrl = null;
        if(video!=null && !video.isEmpty()) videoUrl = Card.getVideoUrl(mCityName, video);

        rootView = inflater.inflate(R.layout.fragment_card_view, container, false);

        if(serviceName != null && !serviceName.isEmpty()) {
            mTextViewCategoryName = (TextView) rootView.findViewById(R.id.category_name);
            mTextViewCategoryName.setText(serviceName);
            mTextViewCategoryName.setVisibility(View.VISIBLE);
        }


        mTextView = (TextView) rootView.findViewById(R.id.text);
        mViewPager = (ViewPager) rootView.findViewById(R.id.photos_pager);

        mMapView = (MapView) rootView.findViewById(R.id.map);
        mMapView.getMapAsync(CardViewFragment.this);
        mMapView.onCreate(null);

        mPhotoPagerAdapter = new MyPhotoGalleryPagerAdapter(getContext(), photosUrl, videoUrl, mCardId);
        CityDataManagerFactory.getInstance(getContext(), mCityName).getCardTextAsync(textFileName, this);
        mViewPager.setAdapter(mPhotoPagerAdapter);


        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(mCardViewActivity != null)
                    mCardViewActivity.stopMediaPlayer();
                Bundle arguments = getArguments();

                if (arguments.getInt(CardViewActivity.ARG_ITINERARY, 0) == 0) {
                    Intent intent = new Intent(getContext(), MapsActivity.class);
                    intent.putExtra(MapsActivity.ARG_CARD_ID, mCardId);
                    intent.putExtra(MapsActivity.ARG_CITY_NAME, mCityName);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getContext(), ItineraryActivity.class);
                    Cursor query = getContext().getContentResolver().query(ItineraryNodeContract.URI,
                            new String[]{ItineraryNodeContract.Entry._ID,ItineraryNodeContract.Entry.COL_ID_ITINERARY
                            },  null, null, null);
                    int itineraryID = 0;
                    if(query != null){
                        query.moveToFirst();
                        itineraryID = query.getInt(query.getColumnIndex(ItineraryNodeContract.Entry.COL_ID_ITINERARY));
                    }

                    query = getContext().getContentResolver().query(ItineraryContract.URI,
                            new String[]{ItineraryContract.Entry._ID,ItineraryContract.Entry.COL_NAME
                            },  ItineraryContract.Entry._ID+" = ?" , new String[]{Integer.toString(itineraryID)}, null);
                    String name = "";
                    if(query != null){
                        query.moveToFirst();
                        name = query.getString(query.getColumnIndex(ItineraryContract.Entry.COL_NAME));
                    }

                    intent.putExtra(ItineraryActivity.ARG_ITINERARY_NAME, name);
                    intent.putExtra(ItineraryActivity.ARG_CARD_ID, mCardId);
                    intent.putExtra(MapsActivity.ARG_CITY_NAME, mCityName);
                    startActivity(intent);
                }
            }
        });

        Bundle arguments = getArguments();
        String name = arguments.getString(CardContract.Entry.COL_NAME);
        double lat = arguments.getDouble(CardContract.Entry.COL_LAT);
        double lng = arguments.getDouble(CardContract.Entry.COL_LNG);

        LatLng position = new LatLng(lat, lng);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));
        googleMap.addMarker(new MarkerOptions()
                .position(position)
                .title(name));

        /*
        if (arguments.getInt(CardViewActivity.ARG_ITINERARY, 0) != 0) {
            Cursor query = getContext().getContentResolver().query(ItineraryNodeContract.URI, new String[]{
                    ItineraryNodeContract.Entry.COL_LON, ItineraryNodeContract.Entry.COL_LAT
            }, null, null, null);
            query.moveToFirst();
            ArrayList<LatLng> latLngs = new ArrayList<LatLng>();
            while (!query.isAfterLast()) {
                latLngs.add(new LatLng(
                        query.getDouble(query.getColumnIndex(ItineraryNodeContract.Entry.COL_LAT)),
                        query.getDouble(query.getColumnIndex(ItineraryNodeContract.Entry.COL_LON))
                ));
                query.moveToNext();
            }
            for (LatLng latLng : latLngs) {
                googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(" "));
            }
        }
        */
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onCardTextResponse(String response) {
        mTextView.setText(response);
    }

    @Override
    public void onCardTextError(Exception message) {
        mTextView.setText(message.getLocalizedMessage());
    }

}