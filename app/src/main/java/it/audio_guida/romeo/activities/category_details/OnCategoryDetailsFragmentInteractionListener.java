package it.audio_guida.romeo.activities.category_details;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 21/08/16.
 */
interface OnCategoryDetailsFragmentInteractionListener {
    void cardSelected(int cardCursorPosition, AbstractCategoryDetailsFragment fragment);
    void cardSelected(int cardCursorPosition, int id, int cardId, AbstractCategoryDetailsFragment fragment);
}
