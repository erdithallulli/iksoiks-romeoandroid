package it.audio_guida.romeo.activities.welcome;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.city_select.CitySelectActivity;
import it.audio_guida.romeo.activities.splash.SplashActivity;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.contracts.ServiceContract;
import it.audio_guida.romeo.sync.SyncUtilities;

import static java.lang.Thread.sleep;

public class WelcomeActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {

    private Button mButton;
    private ProgressBar mProgressBar;
    private MyPreferences preferences;

    public void startLoader(){
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //SyncUtilities.forceSync(this);
        setContentView(R.layout.activity_welcome);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        //mButton = (Button) findViewById(R.id.button_next);
        //mButton.setOnClickListener(this);

        preferences = new MyPreferences(this);
        preferences.put(DataConstants.PREF_KEY_SYNC_CITIES, 1);

        SyncUtilities.forceSync(this);

        new Handler().postDelayed(new ThreadSecondario(this), 1000);

        //getLoaderManager().initLoader(0, null, this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, ServiceContract.URI, new String[]{ServiceContract.Entry._ID}, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.getCount() > 0) {
            mProgressBar.setVisibility(View.GONE);
            Intent intent = new Intent(this, CitySelectActivity.class);
            startActivity(intent);
            finish();
            //mButton.setEnabled(true);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_next:
                Intent intent = new Intent(this, CitySelectActivity.class);
                startActivity(intent);
                finish();
        }
    }

    private class ThreadSecondario implements Runnable{
        private WelcomeActivity welcomeActivity;

        public ThreadSecondario(WelcomeActivity welcomeActivity){
            this.welcomeActivity = welcomeActivity;
        }

        @Override
        public void run() {
            boolean sincronizzato = false;
            int key_sync;
            while (!sincronizzato) {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                key_sync = preferences.getInt(DataConstants.PREF_KEY_SYNC_CITIES, 0);
                sincronizzato = key_sync==0? true:false;
            }
            welcomeActivity.startLoader();
        }
    }
}
