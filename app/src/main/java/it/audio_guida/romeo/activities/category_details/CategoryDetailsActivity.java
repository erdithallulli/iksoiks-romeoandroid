package it.audio_guida.romeo.activities.category_details;

import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.activities.card_view.CardViewActivity;
import it.audio_guida.romeo.activities.itinerary_details.ItineraryDetailsActivity;
import it.audio_guida.romeo.activities.map.MapsActivity;
import it.audio_guida.romeo.activities.search.SearchActivity;
import it.audio_guida.romeo.data.contracts.ItineraryContract;
import it.audio_guida.romeo.model.Card;

import org.apache.commons.lang3.text.WordUtils;

/**
 * An activity that shows a list of cards with an order selected by the user.
 */
public class CategoryDetailsActivity extends AppCompatActivity implements OnCategoryDetailsFragmentInteractionListener, View.OnClickListener {

    public static String ARG_CITY_NAME = "city_name";
    public static String ARG_CATEGORY_ID = "cat_id";
    public static String ARG_CATEGORY_NAME = "cat_name";
    public static String ARG_CURRENT_LOCATION = "location";

    private CategoryDetailsSectionsAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private String mCityName;
    private int mCategoryId;
    private String mCategoryName;
    private Location mCurrentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        mCityName = intent.getStringExtra(ARG_CITY_NAME);
        mCategoryId = intent.getIntExtra(ARG_CATEGORY_ID, -1);
        mCategoryName = intent.getStringExtra(ARG_CATEGORY_NAME);
        if (mCityName == null || mCategoryId == -1 || mCategoryName == null)
            throw new RuntimeException("Activity extras insufficient");
        mCurrentLocation = intent.getParcelableExtra(ARG_CURRENT_LOCATION);

        setContentView(R.layout.activity_category_details);

        setTitle(getString(R.string.activity_category_details_title,
                mCategoryName, WordUtils.capitalizeFully(mCityName)));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new CategoryDetailsSectionsAdapter(
                this, getSupportFragmentManager(), mCityName, mCategoryId, mCurrentLocation);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        if(mCategoryId<-1) {
            tabLayout.setVisibility(View.GONE);
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setVisibility(View.GONE);
        }
        else {
            tabLayout.setupWithViewPager(mViewPager);
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(this);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category_details, menu);
        if(mCategoryId==-2)
            menu.findItem(R.id.action_search).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                SearchActivity.start(this, mCityName, mCategoryId);
                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void cardSelected(int cardCursorPosition, AbstractCategoryDetailsFragment fragment) {
        if(mCategoryId != -2){
            Intent intent = new Intent(this, CardViewActivity.class);
            intent.putExtra(CardViewActivity.ARG_POSITION, cardCursorPosition);
            intent.putExtra(CardViewActivity.ARG_PROJECTION, fragment.getProjection());
            intent.putExtra(CardViewActivity.ARG_SELECTION, fragment.getSelection());
            intent.putExtra(CardViewActivity.ARG_SELECTION_ARGS, fragment.getSelectionArgs());
            intent.putExtra(CardViewActivity.ARG_SORT_ORDER, fragment.getSortOrder());
            if(mCategoryId == -3)
                intent.putExtra(CardViewActivity.ARG_ITINERARY, 1);
            startActivity(intent);
        }else {
            Intent intent = new Intent(this, ItineraryDetailsActivity.class);
            //intent.putExtra(CategoryDetailsActivity.ARG_CITY_NAME, mCityName);
            intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_ID, -3);//-3 sono le tappe dell'itinerario
            Cursor c = getContentResolver().query(ItineraryContract.URI, new String[]{
                            ItineraryContract.Entry._ID,
                            ItineraryContract.Entry.COL_NAME,},
                    ItineraryContract.Entry._ID +" = ?", new String[]{Integer.toString(cardCursorPosition)}, null);
            c.moveToFirst();
            String name = c.getString(c.getColumnIndex(ItineraryContract.Entry.COL_NAME));
            intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_NAME, name);
            intent.putExtra(CategoryDetailsActivity.ARG_CITY_NAME, name);
            intent.putExtra(ItineraryDetailsActivity.ARG_ITINERARY_ID, cardCursorPosition);
            startActivity(intent);
        }
    }

    //TODO da eliminare
    public void cardSelected(int cardCursorPosition, int id, int cardId, AbstractCategoryDetailsFragment fragment) {
        if(mCategoryId != -2){
            Intent intent = new Intent(this, CardViewActivity.class);
            intent.putExtra(CardViewActivity.ARG_POSITION, cardCursorPosition);
            intent.putExtra(CardViewActivity.ARG_PROJECTION, fragment.getProjection());
            intent.putExtra(CardViewActivity.ARG_SELECTION, fragment.getSelection());
            intent.putExtra(CardViewActivity.ARG_SELECTION_ARGS, fragment.getSelectionArgs());
            intent.putExtra(CardViewActivity.ARG_SORT_ORDER, fragment.getSortOrder());
            if(mCategoryId == -3)
                intent.putExtra(CardViewActivity.ARG_ITINERARY, 1);
            startActivity(intent);
        }else {
            Intent intent = new Intent(this, CategoryDetailsActivity.class);
            intent.putExtra(CategoryDetailsActivity.ARG_CITY_NAME, mCityName);
            intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_ID, -3);
            intent.putExtra(CategoryDetailsActivity.ARG_CATEGORY_NAME, "Percorso classico");
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(this, MapsActivity.class);
                intent.putExtra(MapsActivity.ARG_CITY_NAME, mCityName);
                intent.putExtra(MapsActivity.ARG_IN_VISITA, mCurrentLocation);
                startActivity(intent);
        }
    }
}
