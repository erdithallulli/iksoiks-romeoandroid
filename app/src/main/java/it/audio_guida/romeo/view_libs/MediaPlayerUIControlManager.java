package it.audio_guida.romeo.view_libs;

import android.content.*;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.*;
import it.audio_guida.romeo.ITourGuideInterface;
import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.tour_guide.TourGuideService;

import static it.audio_guida.romeo.tour_guide.TourGuideService.*;

public class MediaPlayerUIControlManager implements View.OnClickListener, ServiceConnection, SeekBar.OnSeekBarChangeListener {

    private static final int SLEEP_TIME = 500;
    private final Context mContext;
    private final View mMediaPlayerControlView;
    private final ImageButton mPlayPauseButton;
    private final ProgressBar mProgressBar;
    private final SeekBar mSeekBar;
    private final TextView mTimeText;
    private final TextView mDurationText;
    private final int mPlayerId;
    private ITourGuideInterface mITourGuideService;
    private int mCardIdControlled;
    private boolean mPlaying;
    private boolean mIsLoading = false;
    private int mMsec;
    private boolean preview;
    private LocalBroadcastManager mBroadcaster;

    private MyAsyncTask mAsyncTask;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String s = intent.getStringExtra(TourGuideService.ARG_MESSAGE);
            switch (s) {
                case MESSAGE_AUDIO_READY:
                    int d = intent.getIntExtra(ARG_AUDIO_DURATION, -1);
                    setStatusReady(true);
                    setStatusDuration(d);
                    break;
                case MESSAGE_AUDIO_NOT_READY:
                    setStatusReady(false);
                    setStatusDuration(-1);
                    setStatusSeek(0);
                    break;
                case MESSAGE_PLAYING:
                    setStatusPlaying(true);
                    break;
                case MESSAGE_PAUSED:
                    setStatusPlaying(false);
                    break;
                case MESSAGE_STOPPED:
                    setStatusPlaying(false);
                    setStatusSeek(0);
                case MESSAGE_SEEK:
                    int seek = intent.getIntExtra(ARG_DATA, 0);
                    setStatusSeek(seek);
            }
        }
    };

    public MediaPlayerUIControlManager(Context context, int mediaPlayerControlViewId, View parentView, boolean preview) {
        mContext = context;
        mMediaPlayerControlView = parentView.findViewById(mediaPlayerControlViewId);
        mPlayerId = parentView.getId();

        mProgressBar = (ProgressBar) mMediaPlayerControlView.findViewById(R.id.progress_bar);
        mSeekBar = (SeekBar) mMediaPlayerControlView.findViewById(R.id.bar_seek);
        mTimeText = (TextView) mMediaPlayerControlView.findViewById(R.id.text_time);
        mDurationText = (TextView) mMediaPlayerControlView.findViewById(R.id.text_duration);

        mPlayPauseButton = (ImageButton) mMediaPlayerControlView.findViewById(R.id.play_pause_button);
        mPlayPauseButton.setOnClickListener(this);
        mSeekBar.setOnSeekBarChangeListener(this);
        this.preview = preview;
        mBroadcaster = LocalBroadcastManager.getInstance(mContext);

    }

    public void onStart() {
        mContext.bindService(new Intent(mContext, TourGuideService.class), this, BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReceiver,
                new IntentFilter(TourGuideService.INTENT_ACTION));
    }

    public void onStop() {
        try {
            if (mITourGuideService != null) mITourGuideService.onBlurAudioPlayer(mPlayerId);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReceiver);
        mContext.unbindService(this);
        if (mAsyncTask != null) mAsyncTask.cancel(true);
    }

    public void changeCardIdControlled(int cardId) {
        mCardIdControlled = cardId;
        try {
            if (mITourGuideService != null) {
                mITourGuideService.onFocusAudioPlayer(mCardIdControlled, mPlayerId);
            }
        } catch (RemoteException e) {
            handleRemoteException(e);
        }
    }

    @Override
    public void onClick(View v) {
        if (!v.isEnabled()) return;
        switch (v.getId()) {
            case R.id.play_pause_button:
                clickPlayPauseButton();
                break;
        }

    }

    public void clickPlayPauseButton(){
        try {
            if(mITourGuideService == null) return;
            if (mPlaying) {
                mITourGuideService.pauseCardAudio();
            } else if (mITourGuideService.getCardAudioLoadedId() != mCardIdControlled) {
                mIsLoading = true;
                mITourGuideService.loadCardAudio(mCardIdControlled, true);
                sendLog();
            } else {
                mITourGuideService.playCardAudio();
            }
        }
        catch (RemoteException e) {
            handleRemoteException(e);
        }
    }

    public void setStatusReady(boolean ready) {
        if (ready) mIsLoading = false;
        mPlayPauseButton.setEnabled(!mIsLoading);
        mPlayPauseButton.setVisibility(mIsLoading ? View.GONE : View.VISIBLE);
        mProgressBar.setVisibility(mIsLoading ? View.VISIBLE : View.GONE);
    }

    public void setStatusPlaying(boolean playing) {
        mPlaying = playing;
        mPlayPauseButton.setImageDrawable(mContext.getResources().getDrawable(playing
                ? R.drawable.ic_pause
                : R.drawable.ic_play_arrow));
        if (playing && (mAsyncTask == null || mAsyncTask.getStatus() != AsyncTask.Status.RUNNING)) {
            mAsyncTask = new MyAsyncTask();
            mAsyncTask.execute();
        }
    }

    public int getmMsec(){
        return mMsec;
    }

    public void setStatusSeek(int msec) {
        mMsec = msec;
        int sec = msec / 1000;
        mSeekBar.setProgress(msec);
        mTimeText.setText(String.format("%d:%02d", sec / 60, sec % 60));
    }

    private void setStatusDuration(int mSec) {
        int sec = mSec / 1000;
        mSeekBar.setMax(mSec);
        mDurationText.setText(String.format("%d:%02d", sec / 60, sec % 60));
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mITourGuideService = ITourGuideInterface.Stub.asInterface(service);
        try {
            setStatusReady(mITourGuideService.isAudioReady());
            setStatusPlaying(mITourGuideService.isPlaying());
            setStatusSeek(mITourGuideService.getCurrentPositionMSec());
            setStatusDuration(mITourGuideService.getAudioDurationMSec());
            if (mCardIdControlled != 0) mITourGuideService.onFocusAudioPlayer(mCardIdControlled, mPlayerId);
        } catch (RemoteException e) {
            handleRemoteException(e);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mITourGuideService = null;
    }

    private void handleRemoteException(RemoteException e) {
        Toast.makeText(mContext, "Remote exception", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (!fromUser) return;
        try {
            mITourGuideService.seekCardAudio(progress);
        } catch (RemoteException e) {
            handleRemoteException(e);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void sendLog(){
        if(preview)
            return;
        Cursor query = mContext.getContentResolver().query(CardContract.URI, new String[]{CardContract.Entry._ID,
                        CardContract.Entry.COL_SERVICE_ID,CardContract.Entry.COL_CAT_ID, CardContract.Entry.COL_CITY,
                        CardContract.Entry.COL_AUDIO, CardContract.Entry.COL_TEXT, CardContract.Entry.COL_DATA_EXISTS},
                CardContract.Entry._ID +" = ?", new String[]{Integer.toString(mCardIdControlled)}, null);
        int service_id = 0;
        int cat_id = 0;
        String name = "";
        String audio = "";
        String text = "";
        String dataExists = "";
        if(query != null) {
            query.moveToFirst();
            int index = query.getColumnIndex(CardContract.Entry.COL_SERVICE_ID);
            service_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CAT_ID);
            cat_id = Integer.parseInt(query.getString(index));
            index = query.getColumnIndex(CardContract.Entry.COL_CITY);
            name = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_AUDIO);
            audio = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_TEXT);
            text = query.getString(index);
            index = query.getColumnIndex(CardContract.Entry.COL_DATA_EXISTS);
            dataExists = query.getString(index);
        }

        int flagPosition = 0;
        boolean audioExists = false;

        //serve per il path
        boolean textExists = false;
        if(dataExists!=null || !dataExists.isEmpty())
            for(int i=0; i<dataExists.length(); ++i){
                if(dataExists.charAt(i)=='0'||dataExists.charAt(i)=='1'){
                    flagPosition++;
                    if(flagPosition==1 && dataExists.charAt(i)=='1')
                        audioExists = true;
                    else if(flagPosition==2 && dataExists.charAt(i)=='1')
                        textExists = true;
                }
            }

        Intent i = new Intent(DataConstants.INTENT_ACTION_EVENT);
        i.putExtra(DataConstants.ARG_ID, mCardIdControlled);
        i.putExtra(DataConstants.ARG_COD_CAT, cat_id);
        i.putExtra(DataConstants.ARG_COD_SER, service_id);
        i.putExtra(DataConstants.ARG_CITY_NAME, name);
        i.putExtra(DataConstants.ARG_IS_ITINERARY, false);


        i.putExtra(DataConstants.ARG_MESSAGE, DataConstants.MESSAGE_AUDIO_CARD);

        if (audioExists)
            i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getAudioUrl(name, audio));
        else
            i.putExtra(DataConstants.ARG_AUDIO_NAME, Card.getTextUrl(name, text));
        mBroadcaster.sendBroadcast(i);
    }

    private class MyAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void[] params) {
            try {
                while (true) {
                    Thread.sleep(SLEEP_TIME);
                    if (!mPlaying) break;
                    this.publishProgress();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void[] values) {
            try {
                setStatusSeek(mITourGuideService.getCurrentPositionMSec());
            } catch (RemoteException e) {
                handleRemoteException(e);
            }
        }
    }
}


