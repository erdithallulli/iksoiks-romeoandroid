package it.audio_guida.romeo.model;

import android.net.Uri;
import it.audio_guida.romeo.R;
import org.apache.commons.lang3.ArrayUtils;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public class City {

    public static String getImageUrl(String imageFileName) {
        return "http://195.120.231.180/romeo/ImageCitta/" + Uri.encode(imageFileName);
    }

    public static int[] getCategoriesFromEncodedString(String s) {
        String[] cSs = s.split("-[^;]*;");
        Set<Integer> iSs = new LinkedHashSet<>(cSs.length);
        for (String str : cSs)
            iSs.add(Integer.parseInt(str));
        Integer[] categories = new Integer[iSs.size()];
        return ArrayUtils.toPrimitive(iSs.toArray(categories));
    }

    public static String[] getSupportedLanguagesFromEncodedString(String s) {
        String[] lSs = s.split("-[^;]*;");
        Set<String> iSs = new LinkedHashSet<>(lSs.length);
        for (String str : lSs)
            iSs.add(str);
        String[] languages = new String[iSs.size()];
        return iSs.toArray(languages);
    }

    public static int getFlagResourceId(String flag) {
        flag = "flag_" + flag;
        try {
            Field idField = R.drawable.class.getDeclaredField(flag);
            return idField.getInt(idField);
        } catch (Exception e) {
            return R.drawable.flag_en;
        }
    }

}
