package it.audio_guida.romeo.model;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 19/08/16.
 */
public interface GetCardTextListener {

    void onCardTextResponse(String response);

    void onCardTextError(Exception message);
}
