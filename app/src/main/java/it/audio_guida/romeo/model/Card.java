package it.audio_guida.romeo.model;

import android.net.Uri;
import android.util.Log;

import java.util.Locale;
import java.util.Map;

import it.audio_guida.romeo.RomeoApplication;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.network.ServerRequests;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 03/08/16.
 */
public class Card {

    public static String URL_ARCHIVIO_CITTA = "http://195.120.231.180/Palmipedo/ArchivioCitta/";

    public static String getPreviewImageUrl(String city, String image) {
        return "http://195.120.231.180/Palmipedo/ArchivioCitta/" + Uri.encode(city) + "/fotoanteprime/" + Uri.encode(image);
    }

    public static String getImageUrl(String city, String image) {
        if (image.isEmpty()) return null;
        return "http://195.120.231.180/Palmipedo/ArchivioCitta/" + Uri.encode(city) + "/foto/" + Uri.encode(image);
    }

    public static String getVideoUrl(String city, String video) {
        String language = ServerRequests.languageSchede();

        String parlato;
        switch (language){
            case "ENG":
                parlato = "inglese";
                break;
            case "FRA":
                parlato = "francese";
                break;
            case "DEU":
                parlato = "tedesco";
                break;
            case "SPA":
                parlato = "spagnolo";
                break;
            case "RUS":
                parlato = "russo";
                break;
            default:
                parlato = "italiano";
        }
        return "http://195.120.231.180/Palmipedo/ArchivioCitta/" + Uri.encode(city) + "/video/"+ parlato +"/"+ Uri.encode(video);
    }

    //TODO usare variabili e costanti meglio
    public static String getAudioUrl(String city, String audio) {

        String language = ServerRequests.languageSchede();

        String parlato;
        switch (language){
            case "ENG":
                parlato = "inglese";
                break;
            case "FRA":
                parlato = "francese";
                break;
            case "DEU":
                parlato = "tedesco";
                break;
            case "SPA":
                parlato = "spagnolo";
                break;
            case "RUS":
                parlato = "russo";
                break;
            default:
                parlato = "italiano";
        }

        String url = "http://195.120.231.180/Palmipedo/ArchivioCitta/" + Uri.encode(city)
                + "/parlato/"+parlato+"/" + Uri.encode(audio);

        Log.i("Url dell'audio", url);

        return url;
    }

    public static String getTextUrl(String city, String textFileName) {
        return String.format("%s/%s/testi/%s/%s", URL_ARCHIVIO_CITTA, Uri.encode(city), ServerRequests.languageSchedeEstese(), Uri.encode(textFileName));
    }
}
