package it.audio_guida.romeo.model;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public class Coordinates {

    private final double latNE;
    private final double lonNE;
    private final double latSW;
    private final double lonSW;

    public Coordinates(double latNE, double lonNE, double latSW, double lonSW) {
        this.latNE = latNE;
        this.lonNE = lonNE;
        this.latSW = latSW;
        this.lonSW = lonSW;
    }

    public double getLatNE() {
        return latNE;
    }

    public double getLonNE() {
        return lonNE;
    }

    public double getLatSW() {
        return latSW;
    }

    public double getLonSW() {
        return lonSW;
    }
}
