package it.audio_guida.romeo.model;

import it.audio_guida.romeo.R;

import java.lang.reflect.Field;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 18/08/16.
 */
public class CategoryService {

    public static int getIconResourceId(String icon) {
        icon = "map_" + icon.toLowerCase().replace(".png", "");
        try {
            Field idField = R.drawable.class.getDeclaredField(icon);
            return idField.getInt(idField);
        } catch (Exception e) {
            return R.drawable.map_100manca;
        }
    }
}
