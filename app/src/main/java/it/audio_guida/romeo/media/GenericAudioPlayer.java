package it.audio_guida.romeo.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import it.audio_guida.romeo.model.GetCardTextListener;
import it.audio_guida.romeo.network.ServerRequests;
import it.audio_guida.romeo.sync.CityDataManagerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.util.Locale;

import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 08/08/16.
 */
public class GenericAudioPlayer implements Closeable, TextToSpeech.OnInitListener, MediaPlayer.OnPreparedListener, GetCardTextListener, MediaPlayer.OnBufferingUpdateListener, AudioManager.OnAudioFocusChangeListener {

    public static final int STATUS_PLAYING = 0;
    public static final int STATUS_PAUSED = 1;
    public static final int STATUS_STOPPED = 2;
    public static final int STATUS_SEEK = 3;
    public static final int STATUS_FINISHED = 4;

    private final Context mContext;
    private final TextToSpeech mTextToSpeech;
    private final GenericAudioPlayerListener mListener;
    private final MediaPlayer mMediaPlayer;

    private boolean mMediaPlayerPrepared = false;
    private boolean mTextToSpeechPrepared = false;

    private boolean mTextToSpeechInUse;
    private String mTextLoaded;
    private AudioManager am;

    public GenericAudioPlayer(Context context, GenericAudioPlayerListener listener) {
        mContext = context;
        mListener = listener;
        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        mTextToSpeech = new TextToSpeech(context, this);
        mTextToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {

            }

            @Override
            public void onDone(String utteranceId) {
                mListener.onAudioPlayerStatusChange(STATUS_STOPPED, 0);
                mListener.onAudioPlayerStatusChange(STATUS_FINISHED, 0);
            }

            @Override
            public void onError(String utteranceId) {

            }
        });
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnBufferingUpdateListener(this);
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mListener.onAudioPlayerStatusChange(STATUS_STOPPED, 0);
                mListener.onAudioPlayerStatusChange(STATUS_FINISHED, 0);
            }
        });
    }

    public void loadSpeech(String city, String textFileName, boolean textExists) {
        this.pause();
        if(!textExists)
            mTextToSpeech.setLanguage(new Locale("it"));
        else
            mTextToSpeech.setLanguage(new Locale(ServerRequests.languageSchede()));
        mTextToSpeechInUse = true;
        mListener.onAudioPlayerReset();
        CityDataManagerFactory.getInstance(mContext, city).getCardTextAsync(textFileName, this);
    }

    public void loadAudioUri(Uri uri) throws IOException {
        this.pause();
        mTextToSpeechInUse = false;
        mMediaPlayerPrepared = false;
        mMediaPlayer.reset();
        mListener.onAudioPlayerReset();
        mMediaPlayer.setDataSource(mContext, uri);
        mMediaPlayer.prepareAsync();
    }

    @Override
    public void onInit(int status) {
        mTextToSpeechPrepared = true;
        if (mTextToSpeechInUse) mListener.onAudioPlayerReady();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        // Do nothing, responsibility transferred to onBufferingUpdate
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        if (mMediaPlayerPrepared) return;
        if (percent == 100) {
            mMediaPlayerPrepared = true;
            if (!mTextToSpeechInUse) mListener.onAudioPlayerReady();
        }
    }

    public boolean isReady() {
        if (mTextToSpeechInUse) return mTextToSpeechPrepared;
        return mMediaPlayerPrepared;
    }

    public void play() {
        // Request audio focus for playback
        int result = am.requestAudioFocus(this,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            startPlayback();
        }
    }

    private void startPlayback() {
        if (mTextToSpeechInUse) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mTextToSpeech.speak(mTextLoaded, TextToSpeech.QUEUE_FLUSH, null, "1");
            } else {
                mTextToSpeech.speak(mTextLoaded, TextToSpeech.QUEUE_FLUSH, null);
            }
        } else {
            mMediaPlayer.start();
        }
        mListener.onAudioPlayerStatusChange(STATUS_PLAYING, 0);
    }

    public void pause() {
        am.abandonAudioFocus(this);
        pausePlayback();
    }

    private void pausePlayback() {
        if (mTextToSpeechInUse) {
            mTextToSpeech.stop();
            mListener.onAudioPlayerStatusChange(STATUS_STOPPED, 0);
        } else {
            if (mMediaPlayer.isPlaying()) mMediaPlayer.pause();
            mListener.onAudioPlayerStatusChange(STATUS_PAUSED, 0);
        }
    }

    public void stop() {
        am.abandonAudioFocus(this);
        stopPlayback();
    }

    private void stopPlayback() {
        if (mTextToSpeechInUse) {
            mTextToSpeech.stop();
        } else {
            if (mMediaPlayerPrepared && mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
                mMediaPlayer.seekTo(0);
            }
        }
        mListener.onAudioPlayerStatusChange(STATUS_STOPPED, 0);
    }

    public void close() {
        mMediaPlayerPrepared = false;
        mTextToSpeechPrepared = false;
        mTextToSpeech.shutdown();
        mMediaPlayer.release();
    }

    public boolean isPlaying() {
        if (mTextToSpeechInUse) return mTextToSpeech.isSpeaking();
        return mMediaPlayer.isPlaying();
    }

    public int getAudioDurationMSec() {
        if (mTextToSpeechInUse) return -1;
        if (!mMediaPlayerPrepared) return -1;
        return mMediaPlayer.getDuration();
    }

    public void seekTo(int msec) {
        if (mTextToSpeechInUse) return;
        if (!mMediaPlayerPrepared) return;

        mMediaPlayer.seekTo(msec);
        mListener.onAudioPlayerStatusChange(STATUS_SEEK, msec);
    }

    public int getCurrentPositionMSec() {
        if (mTextToSpeechInUse) return -1;
        if (!mMediaPlayerPrepared) return -1;
        return mMediaPlayer.getCurrentPosition();
    }

    @Override
    public void onCardTextResponse(String response) {
        mTextLoaded = response;
        if (mTextToSpeechPrepared) mListener.onAudioPlayerReady();
    }

    @Override
    public void onCardTextError(Exception message) {
        mTextLoaded = "Errore";
        if (mTextToSpeechPrepared) mListener.onAudioPlayerReady();
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        if (focusChange == AUDIOFOCUS_LOSS_TRANSIENT) {
            pausePlayback();
        } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
            startPlayback();
        } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
//            am.unregisterMediaButtonEventReceiver(RemoteControlReceiver);
            am.abandonAudioFocus(this);
            stopPlayback();
        }
    }

    public interface GenericAudioPlayerListener {
        void onAudioPlayerReady();

        void onAudioPlayerStatusChange(int status, int data);

        void onAudioPlayerReset();
    }
}
