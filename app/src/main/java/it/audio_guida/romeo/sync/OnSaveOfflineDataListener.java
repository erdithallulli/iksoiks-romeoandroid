package it.audio_guida.romeo.sync;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 19/08/16.
 */
public interface OnSaveOfflineDataListener {
    void onProgressSaveOfflineData(int percent);

    void onFinishedSaveOfflineData();

    void onErrorSaveOfflineData(Exception errorMessage);
}
