package it.audio_guida.romeo.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;

import static it.audio_guida.romeo.data.DataConstants.AUTHORITY;
import static it.audio_guida.romeo.sync.Authenticator.ACCOUNT;
import static it.audio_guida.romeo.sync.Authenticator.ACCOUNT_TYPE;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 02/08/16.
 */
public class SyncUtilities {

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account createSyncAccount(Context context) {
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        Account account;
        if (accounts.length == 0) {
            account = new Account(ACCOUNT, ACCOUNT_TYPE);
            accountManager.addAccountExplicitly(account, null, null);
        } else {
            account = accounts[0];
        }
        return account;
    }

    public static void forceSync(Context context) {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
//        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        Account mAccount = createSyncAccount(context.getApplicationContext());
        ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
    }
}
