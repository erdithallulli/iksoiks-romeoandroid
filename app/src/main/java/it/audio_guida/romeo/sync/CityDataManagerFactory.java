package it.audio_guida.romeo.sync;

import android.content.Context;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 19/08/16.
 */
public class CityDataManagerFactory {

    private static CityDataManager sInstance;
    private static String sCityName;

    public static CityDataManager getInstance(Context context, String cityName) {
        if (!cityName.equals(sCityName) || sInstance == null) {
            sInstance = new CityDataManager(context.getApplicationContext(), cityName);
            sCityName = cityName;
        }
        return sInstance;
    }
}
