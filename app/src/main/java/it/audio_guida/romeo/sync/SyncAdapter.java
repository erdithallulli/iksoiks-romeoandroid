package it.audio_guida.romeo.sync;

import android.accounts.Account;
import android.content.*;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import it.audio_guida.romeo.R;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.data.RomeoProvider;
import it.audio_guida.romeo.data.contracts.*;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.network.ServerRequests;
import it.audio_guida.romeo.runtime_info.RuntimeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    public static final String TAG = "ROMEO";
    private final ContentResolver mContentResolver;
    private final MyPreferences myPreferences;

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
        myPreferences = new MyPreferences(mContentResolver);
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
        myPreferences = new MyPreferences(mContentResolver);
    }

    //TODO decidere bene come gestire la sincronizzazione
    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {

        long timeNow = (new Date()).getTime();
        long timeLast = myPreferences.getLong(DataConstants.PREF_KEY_LAST_SYNC_TIME, 0);

        String cityNow = myPreferences.getString(DataConstants.PREF_KEY_CITY, null);
        String cityLast = myPreferences.getString(DataConstants.PREF_KEY_LAST_SYNCED_CITY, "");

        //// TODO: usare costanti per leggibilità codice
        boolean toSync = myPreferences.getInt(DataConstants.PREF_KEY_SYNC_CITIES, 0)==1? true:false;

        int idItinerary = myPreferences.getInt(DataConstants.LAST_OPENED_ITINERARY, -1);
        if (idItinerary != -1) {
            syncItineraryNode(Integer.toString(idItinerary));
            myPreferences.put(DataConstants.LAST_OPENED_ITINERARY, -1);
        } else {
            //if ((timeNow - timeLast) > 31536000000L) {

            if(toSync){
                try {
                    mContentResolver.delete(ItineraryContract.URI, null, null);

                    Log.i(TAG, "Syncing city types...");
                    syncCityTypes();
                    Log.i(TAG, "Synced city types");
                    Log.i(TAG, "Syncing cities...");
                    syncCities();
                    Log.i(TAG, "Synced cities");

                    //syncItineraryNode(Integer.toString(idItinerary));


                    Log.i(TAG, "Syncing categories...");
                    syncCategories();
                    Log.i(TAG, "Synced categories");
                    Log.i(TAG, "Syncing services...");
                    syncServices();
                    Log.i(TAG, "Synced services");
                    myPreferences.put(DataConstants.PREF_KEY_LAST_SYNC_TIME, timeNow);
                } catch (ExecutionException | InterruptedException | JSONException e) {
                    //Log.e(TAG, e.getMessage());
                    //e.printStackTrace();
                }
                //}
                //if (!cityLast.equals(cityNow)) {
                try {
                    Log.i(TAG, "Syncing cards...");
                    syncCards(cityNow);
                    Log.i(TAG, "Synced cards");

                    syncItinerary(cityNow);
                    myPreferences.put(DataConstants.PREF_KEY_LAST_SYNCED_CITY, cityNow);
                } catch (ExecutionException | InterruptedException | JSONException e) {
                    //Log.e(TAG, e.getMessage());
                    //e.printStackTrace();
                }

                myPreferences.put(DataConstants.PREF_KEY_SYNC_CITIES, 0);
            }
        }
    }

    private void syncList(JSONArray response, Uri uri, String[] cols, String[] keys) throws JSONException {
        if(uri.equals(ItineraryContract.URI))
            Log.i("Count ObjectJSON", ""+ response.length());
        int objectsCount = response.length();
        ContentValues[] values = new ContentValues[objectsCount];
        for (int i = 0; i < objectsCount; i++) {
            if(uri.equals(ItineraryContract.URI))
                Log.i("valore della i", ""+i);
            JSONObject jsonObject = response.getJSONObject(i);
            values[i] = new ContentValues(cols.length);
            for (int j = 0; j < cols.length; j++) {
                String string;
                try {
                    string = jsonObject.getString(keys[j]);
                    if(uri.equals(ItineraryContract.URI))
                        Log.i("valore - key", string+" - "+ keys[j]);
                } catch (JSONException e) {
                    string = "";
                }
                values[i].put(cols[j], string);
            }
        }
        mContentResolver.bulkInsert(uri, values);

    }

    private void syncCities() throws ExecutionException, InterruptedException, JSONException {
        JSONArray response = ServerRequests.citiesList().get();
        syncList(response, CityContract.URI, CityContract.COLS, CityContract.KEYS);
        getContext().getContentResolver().notifyChange(CityContract.URI, null, false);
    }

    private void syncCityTypes() throws ExecutionException, InterruptedException, JSONException {
        JSONArray response = ServerRequests.cityTypesList().get();
        JSONObject nearst = new JSONObject();
        nearst.put("code", 100);
        nearst.put("order", -2);
        String lan = myPreferences.getString(DataConstants.PREF_KEY_LAN, Locale.getDefault().getISO3Language().toUpperCase());
        String title;
        if(lan.equals(Locale.ITALIAN.getISO3Language().toUpperCase()))
            title = "Intorno a te";
        else if(lan.equals(Locale.GERMAN.getISO3Language().toUpperCase()))
            title = "Um Sie herum";
        else if(lan.equals(Locale.FRENCH.getISO3Language().toUpperCase()))
            title = "Autour de vous";
        else if(lan.equals((new Locale("es")).getISO3Language().toUpperCase()))
            title = "A su alrededor";
        else if(lan.equals((new Locale("ru")).getISO3Language().toUpperCase()))
            title = "Вокруг вас";
        else
            title = "Around you";
        //nearst.put("name", getContext().getResources().getString(R.string.around_you));
        nearst.put("name", title);
        response.put(nearst);
        syncList(response, CityTypeContract.URI, CityTypeContract.COLS, CityTypeContract.KEYS);
        getContext().getContentResolver().notifyChange(CityTypeContract.URI, null, false);
    }

    private void syncCategories() throws ExecutionException, InterruptedException, JSONException {
        JSONArray response = ServerRequests.categoriesList().get();
        syncList(response, CategoryContract.URI, CategoryContract.COLS, CategoryContract.KEYS);
        getContext().getContentResolver().notifyChange(CategoryContract.URI, null, false);
    }

    private void syncServices() throws ExecutionException, InterruptedException, JSONException {
        JSONArray response = ServerRequests.servicesList().get();
        syncList(response, ServiceContract.URI, ServiceContract.COLS, ServiceContract.KEYS);
        getContext().getContentResolver().notifyChange(ServiceContract.URI, null, false);
    }

    private void syncCards(String cityName) throws ExecutionException, InterruptedException, JSONException {
        if (cityName == null) return;

        //RomeoProvider romeoProvider = new RomeoProvider();
        //Cursor cursor = romeoProvider.query(CityContract.URI, new String[]{CityContract.Entry.COL_LANGUAGES}, CityContract.Entry.COL_NAME +" = "+cityName, null, null);

        JSONArray response = ServerRequests.cardsList(cityName).get();
        syncList(response, CardContract.URI, CardContract.COLS, CardContract.KEYS);
        getContext().getContentResolver().notifyChange(CardContract.URI, null, false);
    }

    private void syncItinerary(String cityName) {
        try {

            JSONArray response = ServerRequests.itinerariesList(cityName).get();
            syncList(response, ItineraryContract.URI, ItineraryContract.COLS, ItineraryContract.KEYS);
            getContext().getContentResolver().notifyChange(ItineraryContract.URI, null, false);

            /*JSONArray response = new JSONArray();
            JSONObject percorso_classico = new JSONObject();
            percorso_classico.put("_id", 121);
            percorso_classico.put("name", new String("Percorso classico"));
            percorso_classico.put("autore", new String("PALMIPEDO"));
            percorso_classico.put("comune", new String("BOLOGNA"));
            percorso_classico.put("lunghezza", 4109);
            percorso_classico.put("ndurata", 99);
            percorso_classico.put("hdurata", new String("01:38:37"));
            percorso_classico.put("nsiti", 38);
            percorso_classico.put("image", new String("BO-PALAZZO-DEL-PODESTA_01.jpg"));
            percorso_classico.put("tipo", new String("A piedi"));
            percorso_classico.put("indirizzopartenza", new String(""));
            percorso_classico.put("indirizzoarrivo", new String(""));

            JSONObject percorso_torri = new JSONObject();
            percorso_torri.put("_id", 122);
            percorso_torri.put("name", new String("Percorso delle torri"));
            percorso_torri.put("autore", new String("PALMIPEDO"));
            percorso_torri.put("comune", new String("BOLOGNA"));
            percorso_torri.put("lunghezza", 1835);
            percorso_torri.put("ndurata", 44);
            percorso_torri.put("hdurata", new String("00:44:02"));
            percorso_torri.put("nsiti", 24);
            percorso_torri.put("image", new String("BO-LE-DUE-TORRI_01.jpg"));
            percorso_torri.put("tipo", new String("A piedi"));
            percorso_torri.put("indirizzopartenza", new String(""));
            percorso_torri.put("indirizzoarrivo", new String(""));

            response.put(0, percorso_classico);
            response.put(1, percorso_torri);
            syncList(response, ItineraryContract.URI, ItineraryContract.COLS, ItineraryContract.KEYS);
            getContext().getContentResolver().notifyChange(ItineraryContract.URI, null, false);
            */
        } catch (Exception e) {

        }
    }
    private void syncItineraryNode(String id){
        try {

            JSONArray response = ServerRequests.itineraryNodesList(id).get();
            syncList(response, ItineraryNodeContract.URI, ItineraryNodeContract.COLS, ItineraryNodeContract.KEYS);
            getContext().getContentResolver().notifyChange(ItineraryNodeContract.URI, null, false);

            /*
            JSONArray response = new JSONArray();
            JSONObject tappa = new JSONObject();
            tappa.put("_id", 1);
            tappa.put("name", new String("Palazzo Del Podestà"));
            tappa.put("lat", 44.4939291032133);
            tappa.put("lon", 11.3431795045995);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10530);
            tappa.put("ndistanza", 0);
            tappa.put("hdurata", new String("00:00:00"));
            response.put(0, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 2);
            tappa.put("name", new String("Piazza Maggiore"));
            tappa.put("lat", 44.4935475133217);
            tappa.put("lon", 11.3430609023359);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10448);
            tappa.put("ndistanza", 43);
            tappa.put("hdurata", new String("00:01:02"));
            response.put(1, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 3);
            tappa.put("name", new String("Basilica Di San Petronio"));
            tappa.put("lat", 44.4935171465586);
            tappa.put("lon", 11.3432860747494);
            tappa.put("catId", 17);
            tappa.put("serviceId", 1);
            tappa.put("schedaCode", 10369);
            tappa.put("ndistanza", 18);
            tappa.put("hdurata", new String("00:00:26"));
            response.put(2, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 4);
            tappa.put("name", new String("Bologna"));
            tappa.put("lat", 44.4934827690909);
            tappa.put("lon", 11.3435009339225);
            tappa.put("catId", 15);
            tappa.put("serviceId", 1);
            tappa.put("schedaCode", 10527);
            tappa.put("ndistanza", 17);
            tappa.put("hdurata", new String("00:00:24"));
            response.put(3, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 5);
            tappa.put("name", new String("Palazzo Dei Banchi"));
            tappa.put("lat", 44.4934598507791);
            tappa.put("lon", 11.3436722483033);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10532);
            tappa.put("ndistanza", 14);
            tappa.put("hdurata", new String("00:00:20"));
            response.put(4, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 17);
            tappa.put("name", new String("Palazzo Dell'Archiginnasio"));
            tappa.put("lat", 44.4922428884222);
            tappa.put("lon", 11.343331911373);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10533);
            tappa.put("ndistanza", 140);
            tappa.put("hdurata", new String("00:03:22"));
            response.put(5, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 21);
            tappa.put("name", new String("Piazza Galvani"));
            tappa.put("lat", 44.4918985407874);
            tappa.put("lon", 11.3432327896744);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10461);
            tappa.put("ndistanza", 39);
            tappa.put("hdurata", new String("00:00:56"));
            response.put(6, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 37);
            tappa.put("name", new String("Basilica Di San Domenico"));
            tappa.put("lat", 44.4897803158188);
            tappa.put("lon", 11.343396082646);
            tappa.put("catId", 17);
            tappa.put("serviceId", 1);
            tappa.put("schedaCode", 10363);
            tappa.put("ndistanza", 292);
            tappa.put("hdurata", new String("00:07:00"));
            response.put(7, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 52);
            tappa.put("name", new String("Palazzo Di Residenza Della Cassa Di Risparmio Di Bologna"));
            tappa.put("lat", 44.4915026269509);
            tappa.put("lon", 11.3459067837043);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10555);
            tappa.put("ndistanza", 421);
            tappa.put("hdurata", new String("00:10:06"));
            response.put(8, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 65);
            tappa.put("name", new String("Complesso Di Santo Stefano"));
            tappa.put("lat", 44.4918733306444);
            tappa.put("lon", 11.3484014419443);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10371);
            tappa.put("ndistanza", 326);
            tappa.put("hdurata", new String("00:07:49"));
            response.put(9, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 67);
            tappa.put("name", new String("Piazza Santo Stefano"));
            tappa.put("lat", 44.4921070974248);
            tappa.put("lon", 11.348175696573);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10453);
            tappa.put("ndistanza", 32);
            tappa.put("hdurata", new String("00:00:46"));
            response.put(10, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 71);
            tappa.put("name", new String("Palazzo Isolani"));
            tappa.put("lat", 44.49264625071);
            tappa.put("lon", 11.3477093089278);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10554);
            tappa.put("ndistanza", 71);
            tappa.put("hdurata", new String("00:01:42"));
            response.put(11, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 76);
            tappa.put("name", new String("Torre Alberici"));
            tappa.put("lat", 44.4935268868411);
            tappa.put("lon", 11.3468401319526);
            tappa.put("catId", 17);
            tappa.put("serviceId", 2);
            tappa.put("schedaCode", 10416);
            tappa.put("ndistanza", 119);
            tappa.put("hdurata", new String("00:02:51"));
            response.put(12, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 77);
            tappa.put("name", new String("Palazzo Della Mercanzia"));
            tappa.put("lat", 44.4936718451633);
            tappa.put("lon", 11.3466630879939);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10534);
            tappa.put("ndistanza", 21);
            tappa.put("hdurata", new String("00:00:30"));
            response.put(13, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 81);
            tappa.put("name", new String("Le Due Torri: Garisenda E Degli Asinelli"));
            tappa.put("lat", 44.4941582863314);
            tappa.put("lon", 11.3465020868534);
            tappa.put("catId", 17);
            tappa.put("serviceId", 2);
            tappa.put("schedaCode", 10409);
            tappa.put("ndistanza", 59);
            tappa.put("hdurata", new String("00:01:25"));
            response.put(14, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 83);
            tappa.put("name", new String("Piazza Di Porta Ravegnana"));
            tappa.put("lat", 44.4942803263417);
            tappa.put("lon", 11.3465662581265);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10462);
            tappa.put("ndistanza", 15);
            tappa.put("hdurata", new String("00:00:22"));
            response.put(15, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 89);
            tappa.put("name", new String("Via Zamboni"));
            tappa.put("lat", 44.4947570272273);
            tappa.put("lon", 11.3470120192911);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10457);
            tappa.put("ndistanza", 65);
            tappa.put("hdurata", new String("00:01:34"));
            response.put(16, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 100);
            tappa.put("name", new String("Palazzo Magnani Salem"));
            tappa.put("lat", 44.4953930103799);
            tappa.put("lon", 11.3484140470158);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10546);
            tappa.put("ndistanza", 132);
            tappa.put("hdurata", new String("00:03:10"));
            response.put(17, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 102);
            tappa.put("name", new String("Basilica Di San Giacomo Maggiore"));
            tappa.put("lat", 44.495448587286);
            tappa.put("lon", 11.3485721833672);
            tappa.put("catId", 17);
            tappa.put("serviceId", 1);
            tappa.put("schedaCode", 10376);
            tappa.put("ndistanza", 14);
            tappa.put("hdurata", new String("00:00:20"));
            response.put(18, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 110);
            tappa.put("name", new String("Palazzo Malvezzi De' Medici"));
            tappa.put("lat", 44.4955488549001);
            tappa.put("lon", 11.3488305873328);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10547);
            tappa.put("ndistanza", 95);
            tappa.put("hdurata", new String("00:02:17"));
            response.put(19, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 140);
            tappa.put("name", new String("Palazzo Ronzani"));
            tappa.put("lat", 44.4942705860592);
            tappa.put("lon", 11.3462986868362);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10538);
            tappa.put("ndistanza", 298);
            tappa.put("hdurata", new String("00:07:09"));
            response.put(20, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 146);
            tappa.put("name", new String("Via Rizzoli"));
            tappa.put("lat", 44.4945330007294);
            tappa.put("lon", 11.3447110207859);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10470);
            tappa.put("ndistanza", 129);
            tappa.put("hdurata", new String("00:03:06"));
            response.put(21, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 152);
            tappa.put("name", new String("Torre Lambertini"));
            tappa.put("lat", 44.4944298683263);
            tappa.put("lon", 11.3435307277279);
            tappa.put("catId", 17);
            tappa.put("serviceId", 2);
            tappa.put("schedaCode", 10418);
            tappa.put("ndistanza", 115);
            tappa.put("hdurata", new String("00:02:46"));
            response.put(22, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 153);
            tappa.put("name", new String("Palazzo Re Enzo"));
            tappa.put("lat", 44.4944487759335);
            tappa.put("lon", 11.3433966556038);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10529);
            tappa.put("ndistanza", 11);
            tappa.put("hdurata", new String("00:00:16"));
            response.put(23, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 156);
            tappa.put("name", new String("Piazza Del Nettuno"));
            tappa.put("lat", 44.4945140931221);
            tappa.put("lon", 11.342881566546);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10463);
            tappa.put("ndistanza", 78);
            tappa.put("hdurata", new String("00:01:52"));
            response.put(24, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 157);
            tappa.put("name", new String("Fontana Del Nettuno"));
            tappa.put("lat", 44.4943301736699);
            tappa.put("lon", 11.3428065090748);
            tappa.put("catId", 17);
            tappa.put("serviceId", 2);
            tappa.put("schedaCode", 10441);
            tappa.put("ndistanza", 21);
            tappa.put("hdurata", new String("00:00:30"));
            response.put(25, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 158);
            tappa.put("name", new String("Palazzo D'Accursio O Comunale"));
            tappa.put("lat", 44.4943645511376);
            tappa.put("lon", 11.3425114358103);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10528);
            tappa.put("ndistanza", 24);
            tappa.put("hdurata", new String("00:00:35"));
            response.put(26, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 160);
            tappa.put("name", new String("Canton Dè Fiori"));
            tappa.put("lat", 44.4948618785038);
            tappa.put("lon", 11.3426563941325);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10467);
            tappa.put("ndistanza", 56);
            tappa.put("hdurata", new String("00:01:21"));
            response.put(27, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 162);
            tappa.put("name", new String("Fontana Vecchia"));
            tappa.put("lat", 44.4950343388001);
            tappa.put("lon", 11.341776903917);
            tappa.put("catId", 17);
            tappa.put("serviceId", 2);
            tappa.put("schedaCode", 10447);
            tappa.put("ndistanza", 72);
            tappa.put("hdurata", new String("00:01:44"));
            response.put(28, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 166);
            tappa.put("name", new String("Dottor Balanzone"));
            tappa.put("lat", 44.4952830024832);
            tappa.put("lon", 11.3402534091397);
            tappa.put("catId", 17);
            tappa.put("serviceId", 6);
            tappa.put("schedaCode", 10524);
            tappa.put("ndistanza", 124);
            tappa.put("hdurata", new String("00:02:59"));
            response.put(29, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 168);
            tappa.put("name", new String("Via Ugo Bassi"));
            tappa.put("lat", 44.4954285337632);
            tappa.put("lon", 11.3392553166606);
            tappa.put("catId", 17);
            tappa.put("serviceId", 5);
            tappa.put("schedaCode", 10468);
            tappa.put("ndistanza", 80);
            tappa.put("hdurata", new String("00:01:55"));
            response.put(30, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 172);
            tappa.put("name", new String("Palazzo Ghisilieri"));
            tappa.put("lat", 44.4956542791344);
            tappa.put("lon", 11.3377209356853);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10537);
            tappa.put("ndistanza", 125);
            tappa.put("hdurata", new String("00:03:00"));
            response.put(31, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 178);
            tappa.put("name", new String("Chiesa Di San Francesco"));
            tappa.put("lat", 44.4951718486709);
            tappa.put("lon", 11.3355752087425);
            tappa.put("catId", 17);
            tappa.put("serviceId", 1);
            tappa.put("schedaCode", 10367);
            tappa.put("ndistanza", 231);
            tappa.put("hdurata", new String("00:05:33"));
            response.put(32, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 190);
            tappa.put("name", new String("Chiesa Di Ss. Salvatore"));
            tappa.put("lat", 44.4941542756268);
            tappa.put("lon", 11.3386382411153);
            tappa.put("catId", 17);
            tappa.put("serviceId", 1);
            tappa.put("schedaCode", 10373);
            tappa.put("ndistanza", 451);
            tappa.put("hdurata", new String("00:10:49"));
            response.put(33, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 197);
            tappa.put("name", new String("Torre Agresti"));
            tappa.put("lat", 44.4938099279919);
            tappa.put("lon", 11.3409667415947);
            tappa.put("catId", 17);
            tappa.put("serviceId", 2);
            tappa.put("schedaCode", 10411);
            tappa.put("ndistanza", 192);
            tappa.put("hdurata", new String("00:04:36"));
            response.put(34, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 200);
            tappa.put("name", new String("Torre Accursi O Dell'Orologio"));
            tappa.put("lat", 44.4936105386792);
            tappa.put("lon", 11.3419430616776);
            tappa.put("catId", 17);
            tappa.put("serviceId", 2);
            tappa.put("schedaCode", 10420);
            tappa.put("ndistanza", 84);
            tappa.put("hdurata", new String("00:02:01"));
            response.put(35, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 201);
            tappa.put("name", new String("Palazzo Dei Notai"));
            tappa.put("lat", 44.4935498051529);
            tappa.put("lon", 11.3423131924132);
            tappa.put("catId", 17);
            tappa.put("serviceId", 3);
            tappa.put("schedaCode", 10531);
            tappa.put("ndistanza", 30);
            tappa.put("hdurata", new String("00:00:43"));
            response.put(36, tappa);

            tappa = new JSONObject();
            tappa.put("_id", 203);
            tappa.put("name", new String("Lucio Dalla"));
            tappa.put("lat", 44.4938746722228);
            tappa.put("lon", 11.3424741935537);
            tappa.put("catId", 17);
            tappa.put("serviceId", 6);
            tappa.put("schedaCode", 10519);
            tappa.put("ndistanza", 38);
            tappa.put("hdurata", new String("00:00:55"));
            response.put(37, tappa);

            syncList(response, ItineraryNodeContract.URI, ItineraryNodeContract.COLS, ItineraryNodeContract.KEYS);
            getContext().getContentResolver().notifyChange(ItineraryNodeContract.URI, null, false);
            */

        }catch (Exception e){}
    }

}