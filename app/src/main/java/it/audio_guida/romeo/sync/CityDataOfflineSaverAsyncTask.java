package it.audio_guida.romeo.sync;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import com.android.volley.toolbox.RequestFuture;
import it.audio_guida.romeo.data.contracts.CardContract;
import it.audio_guida.romeo.data.contracts.CityContract;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.network.ServerRequests;

import java.io.File;
import java.util.concurrent.ExecutionException;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 19/08/16.
 */
class CityDataOfflineSaverAsyncTask extends AsyncTask<Void, Integer, Integer> {

    public static final int RESULT_OK = 0;
    public static final int RESULT_ERROR = 1;
    private final OnSaveOfflineDataListener mListener;
    private final String mCityName;
    private final ContentResolver mContentResolver;
    private final File mFilesDir;

    CityDataOfflineSaverAsyncTask(OnSaveOfflineDataListener listener, String cityName, ContentResolver contentResolver, File filesDir) {
        super();
        mListener = listener;
        mCityName = cityName;
        mContentResolver = contentResolver;
        mFilesDir = filesDir;
    }

    private static boolean saveFileFromUrl(String fileName, File dir, String remoteUrl) {
        if (fileName.isEmpty()) return true;
        RequestFuture<Integer> future = ServerRequests.saveFile(remoteUrl, new File(dir, fileName));
        try {
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        // Retrieving city data
        Cursor cityCursor = mContentResolver.query(CityContract.URI, new String[]{
                CityContract.Entry.COL_IMAGE,
        }, CityContract.Entry.COL_NAME + " = ?", new String[]{mCityName}, null);

        // Retrieving cards data
        Cursor cardsCursor = mContentResolver.query(CardContract.URI, new String[]{
                CardContract.Entry.COL_TEXT,
                CardContract.Entry.COL_PHOTO_1,
                CardContract.Entry.COL_PHOTO_2,
                CardContract.Entry.COL_PHOTO_3,
                CardContract.Entry.COL_AUDIO,
        }, CardContract.Entry.COL_CITY + " = ?", new String[]{mCityName}, null);

        if (cityCursor == null || cardsCursor == null) return RESULT_ERROR;
        int maxIterations = cardsCursor.getCount() * 3 + 1;
        int nIterations = 0;

        // Saving city image
        File cityImagesDir = getGlobalResourcesDir(CityDataManager.FOLDER_LOCAL_CITY_IMAGES);
        int cityImageColumnIndex = cityCursor.getColumnIndex(CityContract.Entry.COL_IMAGE);
        if (cityCursor.moveToFirst()) {
            String cityImageFileName = cityCursor.getString(cityImageColumnIndex);
            if (!saveFileFromUrl(cityImageFileName, cityImagesDir, City.getImageUrl(cityImageFileName)))
                return RESULT_ERROR;
            publishProgress(++nIterations * 100 / maxIterations);
        }

        // Saving cards texts
        File textsDir = getCityResourcesDir(CityDataManager.FOLDER_LOCAL_CARD_TEXTS);
        int textColumnIndex = cardsCursor.getColumnIndex(CardContract.Entry.COL_TEXT);

        cardsCursor.moveToPosition(-1);
        while (cardsCursor.moveToNext()) {
            String textFileName = cardsCursor.getString(textColumnIndex);
            if (!saveFileFromUrl(textFileName, textsDir, Card.getTextUrl(mCityName, textFileName))) return RESULT_ERROR;
            publishProgress(++nIterations * 100 / maxIterations);
        }

        // Saving cards images
        File photosDir = getCityResourcesDir(CityDataManager.FOLDER_LOCAL_CARD_PHOTOS);
        int photo1ColumnIndex = cardsCursor.getColumnIndex(CardContract.Entry.COL_PHOTO_1);
        int photo2ColumnIndex = cardsCursor.getColumnIndex(CardContract.Entry.COL_PHOTO_2);
        int photo3ColumnIndex = cardsCursor.getColumnIndex(CardContract.Entry.COL_PHOTO_3);

        cardsCursor.moveToPosition(-1);
        while (cardsCursor.moveToNext()) {
            String photo1FileName = cardsCursor.getString(photo1ColumnIndex);
            String photo2FileName = cardsCursor.getString(photo2ColumnIndex);
            String photo3FileName = cardsCursor.getString(photo3ColumnIndex);
            if (!saveFileFromUrl(photo1FileName, photosDir, Card.getImageUrl(mCityName, photo1FileName)))
                return RESULT_ERROR;
            if (!saveFileFromUrl(photo2FileName, photosDir, Card.getImageUrl(mCityName, photo2FileName)))
                return RESULT_ERROR;
            if (!saveFileFromUrl(photo3FileName, photosDir, Card.getImageUrl(mCityName, photo3FileName)))
                return RESULT_ERROR;
            publishProgress(++nIterations * 100 / maxIterations);
        }

        // Saving cards audios
        File audioDir = getCityResourcesDir(CityDataManager.FOLDER_LOCAL_CARD_AUDIOS);
        int audioColumnIndex = cardsCursor.getColumnIndex(CardContract.Entry.COL_AUDIO);

        cardsCursor.moveToPosition(-1);
        while (cardsCursor.moveToNext()) {
            String audioFileName = cardsCursor.getString(audioColumnIndex);
            if (!saveFileFromUrl(audioFileName, audioDir, Card.getAudioUrl(mCityName, audioFileName)))
                return RESULT_ERROR;
            publishProgress(++nIterations * 100 / maxIterations);
        }

        // Freeing up resources
        cityCursor.close();
        cardsCursor.close();
        return RESULT_OK;
    }

    private File getCityResourcesDir(String dirName) {
        return getDir(getDir(mFilesDir, mCityName), dirName);
    }

    private File getGlobalResourcesDir(String dirName) {
        return getDir(mFilesDir, dirName);
    }

    private File getDir(File baseDir, String dirName) {
        File theDir = new File(baseDir, dirName);
        boolean result = true;
        if (!theDir.exists()) {
            result = theDir.mkdir();
        }
        if (result) return theDir;
        else return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Integer percent = values[0];
        mListener.onProgressSaveOfflineData(percent);
    }

    @Override
    protected void onPostExecute(Integer status) {
        if (status == RESULT_OK) mListener.onFinishedSaveOfflineData();
        else mListener.onErrorSaveOfflineData(new Exception()); // TODO more comprehensive error
    }
}
