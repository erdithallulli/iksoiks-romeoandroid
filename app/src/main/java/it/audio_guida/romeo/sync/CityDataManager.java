package it.audio_guida.romeo.sync;

import android.content.Context;
import it.audio_guida.romeo.model.GetCardTextListener;
import it.audio_guida.romeo.network.ServerRequests;

import java.io.File;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 19/08/16.
 */
public class CityDataManager {

    public final static String FOLDER_LOCAL_CITY_IMAGES = "cityImages";
    public final static String FOLDER_LOCAL_CARD_PHOTOS = "photos";
    public final static String FOLDER_LOCAL_CARD_TEXTS = "texts";
    public final static String FOLDER_LOCAL_CARD_AUDIOS = "audio";
    private final static String BASE_URL_ROMEO = "http://195.120.231.180/romeo";
    private final static String BASE_URL_NODE_JS = "http://192.168.1.6:3000";
    private final static String BASE_URL_PALMIPEDO = "http://195.120.231.180/Palmipedo/ArchivioCitta";
    private final static String URL_CITY_IMAGES = "/ImageCitta";
    private final static String URL_CARD_PREVIEWS = "/fotoanteprime";
    private final static String URL_CARD_PHOTOS = "/foto";
    private final static String URL_CARD_AUDIO = "/parlato/italiano/";
    private final Context mContext;
    private final String mCityName;
    private final File mFilesDir;

    CityDataManager(Context context, String cityName) {
        mContext = context;
        mCityName = cityName;
        mFilesDir = context.getFilesDir();
    }

    public void saveOffline(OnSaveOfflineDataListener listener) {
        CityDataOfflineSaverAsyncTask task = new CityDataOfflineSaverAsyncTask(listener, mCityName, mContext.getContentResolver(), mFilesDir);
        task.execute();
    }

    public void deleteOffline() {

    }

    public void updateOffline() {

    }

    public boolean isAvailableOffline() {
        return false;
    }

    public void getCardTextAsync(String textFileName, GetCardTextListener listener) {
        ServerRequests.getCardTextAsync(mCityName, textFileName, listener);
    }
}

