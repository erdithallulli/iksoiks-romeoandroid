package it.audio_guida.romeo.data.contracts;

import android.net.Uri;
import android.provider.BaseColumns;
import it.audio_guida.romeo.data.DataConstants;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public final class CityTypeContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY, " +
                    Entry.COL_ORDER + " INTEGER, " +
                    Entry.COL_NAME + " TEXT" +
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;

    public static final String[] KEYS = new String[]{
            "code",
            "name",
            "order"
    };

    public static final String[] COLS = new String[]{
            Entry._ID,
            Entry.COL_NAME,
            Entry.COL_ORDER,
    };

    public static final String[] SQL_TYPES = new String[]{
            "INTEGER PRIMARY KEY",
            "TEXT",
            "INTEGER",
    };

    public static final String MIME_DIR = DataConstants.VND_ANDROID_CURSOR_DIR_VND + DataConstants.AUTHORITY + ".city_type";
    public static final String MIME_ITEM = DataConstants.VND_ANDROID_CURSOR_ITEM_VND + DataConstants.AUTHORITY + ".city_type";

    public static final String PATH = "city_types";
    public static final Uri URI = Uri.parse("content://" + DataConstants.AUTHORITY + "/" + PATH);

    private CityTypeContract() {
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "city_types";
        public static final String COL_NAME = "name";
        public static final String COL_ORDER = "order_";
    }

}
