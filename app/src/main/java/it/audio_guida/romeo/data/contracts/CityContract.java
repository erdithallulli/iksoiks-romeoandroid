package it.audio_guida.romeo.data.contracts;

import android.net.Uri;
import android.provider.BaseColumns;
import it.audio_guida.romeo.data.DataConstants;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public final class CityContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY, " +
                    Entry.COL_CITY_TYPE + " INTEGER, " +
                    Entry.COL_N_CARDS + " INTEGER, " +
                    Entry.COL_N_PHOTOS + " INTEGER, " +
                    Entry.COL_N_AUDIO + " INTEGER, " +
                    Entry.COL_SO_LAT + " REAL, " +
                    Entry.COL_SO_LON + " REAL, " +
                    Entry.COL_NE_LAT + " REAL, " +
                    Entry.COL_NE_LON + " REAL, " +
                    Entry.COL_C_LAT + " REAL, " +
                    Entry.COL_C_LON + " REAL, " +
                    Entry.COL_NAME + " TEXT UNIQUE, " +
                    Entry.COL_LENGTH_AUDIO + " TEXT, " +
                    Entry.COL_IMAGE + " TEXT, " +
                    Entry.COL_LANGUAGES + " TEXT, " +
                    Entry.COL_N_ITINERARIES + " INTEGER,"+
                    Entry.COL_CATEGORIES + " TEXT," +
                    Entry.COL_RAGGIO + " INTEGER, " +
                    Entry.COL_USER_MAP + " TEXT" +
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;

    public static final String[] KEYS = new String[]{
            "cityType",
            "name",
            "image",
            "catsInMap",
            "nSchede",
            "nPhotos",
            "nAudio",
            "lengthAudio",
            "langs",
            "nItinerari",
            "SO_Lat",
            "SO_Lng",
            "NE_Lat",
            "NE_Lng",
            "latC",
            "lonC",
            "raggio",
            "userMap"
    };

    public static final String[] COLS = new String[]{
            Entry.COL_CITY_TYPE,
            Entry.COL_NAME,
            Entry.COL_IMAGE,
            Entry.COL_CATEGORIES,
            Entry.COL_N_CARDS,
            Entry.COL_N_PHOTOS,
            Entry.COL_N_AUDIO,
            Entry.COL_LENGTH_AUDIO,
            Entry.COL_LANGUAGES,
            Entry.COL_N_ITINERARIES,
            Entry.COL_SO_LAT,
            Entry.COL_SO_LON,
            Entry.COL_NE_LAT,
            Entry.COL_NE_LON,
            Entry.COL_C_LAT,
            Entry.COL_C_LON,
            Entry.COL_RAGGIO,
            Entry.COL_USER_MAP
    };

    public static final String MIME_DIR = DataConstants.VND_ANDROID_CURSOR_DIR_VND + DataConstants.AUTHORITY + ".city";
    public static final String MIME_ITEM = DataConstants.VND_ANDROID_CURSOR_ITEM_VND + DataConstants.AUTHORITY + ".city";

    public static final String PATH = "cities";
    public static final Uri URI = Uri.parse("content://" + DataConstants.AUTHORITY + "/" + PATH);

    private CityContract() {
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "cities";
        public static final String COL_NAME = "name";
        public static final String COL_IMAGE = "image";
        public static final String COL_CITY_TYPE = "city_type";
        public static final String COL_CATEGORIES = "cats";
        public static final String COL_N_CARDS = "n_cards";
        public static final String COL_N_PHOTOS = "n_photos";
        public static final String COL_N_AUDIO = "n_audio";
        public static final String COL_LENGTH_AUDIO = "l_audio";
        public static final String COL_LANGUAGES = "languages";
        public static final String COL_N_ITINERARIES = "nItinerari";
        public static final String COL_RAGGIO = "raggio";
        public static final String COL_USER_MAP = "userMap";

        public static final String COL_SO_LAT = "so_lat";
        public static final String COL_SO_LON = "so_lon";
        public static final String COL_NE_LAT = "ne_lat";
        public static final String COL_NE_LON = "ne_lon";

        public static final String COL_C_LAT = "c_lat";
        public static final String COL_C_LON = "c_lon";
    }

}
