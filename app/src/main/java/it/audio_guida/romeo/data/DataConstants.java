package it.audio_guida.romeo.data;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 02/08/16.
 */
public class DataConstants {
    public static final String AUTHORITY = "it.audio_guida.romeo.provider";
    public static final String VND_ANDROID_CURSOR_DIR_VND = "vnd.android.cursor.dir/vnd.";
    public static final String VND_ANDROID_CURSOR_ITEM_VND = "vnd.android.cursor.item/vnd.";

    public static final String PREF_KEY_CITY = "city";
    public static final String PREF_KEY_LAST_SYNC_TIME = "lastSyncTime";
    public static final String PREF_KEY_LAST_SYNCED_CITY = "lastSyncedCity";
    public static final String PREF_KEY_SERVICES_HIDDEN = "servicesHiddenMap";

    public static final String PREF_KEY_SYNC_CITIES = "syncCities";

    //TODO verificare se conviene usare SharedPreferences
    public static final String PREF_KEY_LAN = "language";
    public static final String PREF_KEY_CITY_LAN = "cityLanguages";

    //TODO verificare se conviene costruire modello o aggiungere campo nel db
    public static final String LAST_OPENED_ITINERARY = "itinerary";

    public static final String ARG_IS_ITINERARY = "isItinerary";

    public static final String INTENT_ACTION_EVENT = DataConstants.class.getPackage() + ".event_message";
    public static final String ARG_MESSAGE = "message";
    public static final String ARG_ID = "id";
    public static final String ARG_CITY_NAME = "cityName";
    public static final String ARG_COD_CAT = "arg_cod_cat";
    public static final String ARG_COD_SER = "arg_cod_ser";
    public static final String ARG_AUDIO_NAME = "arg_audio_name";

    public static final int MESSAGE_OPENED_CARD = 1;
    public static final int MESSAGE_AUDIO_CARD = 2;
    public static final int MESSAGE_OPENED_VIDEO = 3;
    public static final int MESSAGE_START_VIDEO = 4;
    public static final int MESSAGE_OPENED_CARD_PREVIEW = 5;
    public static final int MESSAGE_AUDIO_CARD_PREVIEW = 6;
    public static final int MESSAGE_AUDIO_CARD_AUTO = 7;

    public static final int MESSAGE_LOG_IN = 8;

    public static final int MASSAGE_SEARCH = 9;
}
