package it.audio_guida.romeo.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import it.audio_guida.romeo.data.contracts.PreferencesContract;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class MyPreferences {

    private final ContentResolver contentResolver;
    private String value;

    public MyPreferences(Context context) {
        contentResolver = context.getContentResolver();
    }

    public MyPreferences(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    public void put(String key, String value) {
        ContentValues values = new ContentValues();
        values.put(PreferencesContract.Entry.COL_KEY, key);
        values.put(PreferencesContract.Entry.COL_VALUE, value);
        contentResolver.insert(PreferencesContract.URI, values);
    }

    public void put(String key, int value) {
        put(key, String.valueOf(value));
    }

    public void put(String key, long value) {
        put(key, String.valueOf(value));
    }

    public void put(String key, double value) {
        put(key, String.valueOf(value));
    }

    public void put(String key, int[] values) {
        put(key, StringUtils.join(ArrayUtils.toObject(values), ","));
    }

    public String getString(String key, String defaultValue) {
        Uri uri = Uri.withAppendedPath(PreferencesContract.URI, "/key/" + key);
        Cursor query = contentResolver.query(uri, new String[]{PreferencesContract.Entry.COL_VALUE}, null, null, null);
        if (query == null) return defaultValue;
        String stringFromCursor = getStringFromCursor(defaultValue, query);
        query.close();
        return stringFromCursor;
    }

    private String getStringFromCursor(String defaultValue, Cursor query) {
        if (!query.moveToFirst()) return defaultValue;
        String string = query.getString(query.getColumnIndex(PreferencesContract.Entry.COL_VALUE));
        if (string == null) return defaultValue;
        return string;
    }

    public int getInt(String key, int defaultValue) {
        value = getString(key, null);
        if (value == null) return defaultValue;
        return Integer.parseInt(value);
    }

    public long getLong(String key, long defaultValue) {
        value = getString(key, null);
        if (value == null) return defaultValue;
        return Long.parseLong(value);
    }

    public int[] getIntArray(String key, int[] defaultValue) {
        value = getString(key, null);
        if (value == null) return defaultValue;
        try {
            String[] split = value.split(",");
            int length = split.length;
            int[] ints = new int[length];
            for (int i = 0; i < length; i++) {
                ints[i] = Integer.parseInt(split[i]);
            }
            return ints;
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
