package it.audio_guida.romeo.data.contracts;

import android.net.Uri;
import android.provider.BaseColumns;
import it.audio_guida.romeo.data.DataConstants;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public final class ServiceContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY, " +
                    Entry.COL_CAT_ID + " INTEGER, " +
                    Entry.COL_NAME + " TEXT, " +
                    Entry.COL_ICON + " TEXT" +
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;

    public static final String[] KEYS = new String[]{
            "serviceCode",
            "catId",
            "name",
            "icoMap"
    };

    public static final String[] COLS = new String[]{
            Entry._ID,
            Entry.COL_CAT_ID,
            Entry.COL_NAME,
            Entry.COL_ICON,
    };

    public static final String MIME_DIR = DataConstants.VND_ANDROID_CURSOR_DIR_VND + DataConstants.AUTHORITY + ".service";
    public static final String MIME_ITEM = DataConstants.VND_ANDROID_CURSOR_ITEM_VND + DataConstants.AUTHORITY + ".service";

    public static final String PATH = "services";
    public static final Uri URI = Uri.parse("content://" + DataConstants.AUTHORITY + "/" + PATH);

    private ServiceContract() {
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "services";
        public static final String _ID = "sId";
        public static final String COL_NAME = "sName";
        public static final String COL_CAT_ID = "sCatId";
        public static final String COL_ICON = "sIcon";
    }

}
