package it.audio_guida.romeo.data.contracts;

import android.net.Uri;
import android.provider.BaseColumns;
import it.audio_guida.romeo.data.DataConstants;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public final class CardContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY, " +
                    Entry.COL_CAT_ID + " INTEGER, " +
                    Entry.COL_SERVICE_ID + " INTEGER, " +
                    Entry.COL_RADIUS + " INTEGER, " +
                    Entry.COL_LAT + " REAL, " +
                    Entry.COL_LNG + " REAL, " +
                    Entry.COL_PRIORITY + " INTEGER, " +
                    Entry.COL_N_LISTENS + " INTEGER, " +
                    Entry.COL_N_VISITS + " INTEGER, " +
                    Entry.COL_N_CLICKS + " INTEGER, " +
                    Entry.COL_NAME + " TEXT, " +
                    Entry.COL_TEXT + " TEXT, " +
                    Entry.COL_PHOTO_1 + " TEXT, " +
                    Entry.COL_PHOTO_2 + " TEXT, " +
                    Entry.COL_PHOTO_3 + " TEXT, " +
                    Entry.COL_AUDIO + " TEXT, " +
                    Entry.COL_CITY + " TEXT, " +
                    Entry.COL_ADDRESS + " TEXT, " +
                    Entry.COL_DATA_EXISTS + " TEXT, "+
                    Entry.COL_VIDEO + " TEXT"+
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;

    public static final String[] KEYS = new String[]{
            "_id",
            "catId",
            "serviceId",
            "radius",
            "lat",
            "lon",
            "name",
            "text",
            "comune",
            "address",
            "priority",
            "photo1",
            "photo2",
            "photo3",
            "audio",
            "nListens",
            "nVisits",
            "nClicks",
            "dataExists",
            "video"
    };

    public static final String[] COLS = new String[]{
            Entry._ID,
            Entry.COL_CAT_ID,
            Entry.COL_SERVICE_ID,
            Entry.COL_RADIUS,
            Entry.COL_LAT,
            Entry.COL_LNG,
            Entry.COL_NAME,
            Entry.COL_TEXT,
            Entry.COL_CITY,
            Entry.COL_ADDRESS,
            Entry.COL_PRIORITY,
            Entry.COL_PHOTO_1,
            Entry.COL_PHOTO_2,
            Entry.COL_PHOTO_3,
            Entry.COL_AUDIO,
            Entry.COL_N_LISTENS,
            Entry.COL_N_VISITS,
            Entry.COL_N_CLICKS,
            Entry.COL_DATA_EXISTS,
            Entry.COL_VIDEO
    };

    public static final String MIME_DIR = DataConstants.VND_ANDROID_CURSOR_DIR_VND + DataConstants.AUTHORITY + ".card";
    public static final String MIME_ITEM = DataConstants.VND_ANDROID_CURSOR_ITEM_VND + DataConstants.AUTHORITY + ".card";

    public static final String PATH = "cards";
    public static final Uri URI = Uri.parse("content://" + DataConstants.AUTHORITY + "/" + PATH);

    public static final String JOIN_CATEGORIES = " LEFT OUTER JOIN " + CategoryContract.Entry.TABLE_NAME
            + " ON " + Entry.TABLE_NAME + "." + Entry.COL_CAT_ID
            + " = " + CategoryContract.Entry.TABLE_NAME + "." + CategoryContract.Entry._ID;

    public static final String JOIN_SERVICES = " LEFT OUTER JOIN " + ServiceContract.Entry.TABLE_NAME
            + " ON " + Entry.COL_SERVICE_ID
            + " = " + ServiceContract.Entry._ID;

    private CardContract() {
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "cards";
        public static final String COL_NAME = "cName";
        public static final String COL_PHOTO_1 = "cPhoto1";
        public static final String COL_PHOTO_2 = "cPhoto2";
        public static final String COL_PHOTO_3 = "cPhoto3";
        public static final String COL_CAT_ID = "cCatId";
        public static final String COL_SERVICE_ID = "cServiceId";
        public static final String COL_CITY = "cCity";
        public static final String COL_ADDRESS = "cAddress";
        public static final String COL_LAT = "cLat";
        public static final String COL_LNG = "cLon";
        public static final String COL_TEXT = "cText";
        public static final String COL_RADIUS = "cRadius";
        public static final String COL_PRIORITY = "cPriority";
        public static final String COL_AUDIO = "cAudio";
        public static final String COL_N_LISTENS = "cNListens";
        public static final String COL_N_VISITS = "cNVisits";
        public static final String COL_N_CLICKS = "cNClicks";
        public static final String COL_DATA_EXISTS = "dataExists";
        public static final String COL_VIDEO = "video";

        public static final String CALCULATED_DISTANCE = "cCDistance";

        public static String getFieldCalculatedDistance(double lat, double lng) {
            return String.format("((%s - %s) * (%s - %s) + (%s - %s) * (%s - %s)) %s", lat, COL_LAT, lat, COL_LAT, lng, COL_LNG, lng, COL_LNG, CALCULATED_DISTANCE);
        }
    }


}
