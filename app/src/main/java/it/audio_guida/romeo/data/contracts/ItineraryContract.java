package it.audio_guida.romeo.data.contracts;

import android.net.Uri;
import android.provider.BaseColumns;
import it.audio_guida.romeo.data.DataConstants;

public class ItineraryContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY, " +
                    Entry.COL_NAME + " TEXT, " +
                    Entry.COL_AUTORE + " TEXT, " +
                    Entry.COL_COMUNE + " TEXT, " +
                    Entry.COL_LUNGHEZZA + " INTEGER, " +
                    Entry.COL_N_DURATA + " INTEGER, " +
                    Entry.COL_H_DURATA + " TEXT, " +
                    Entry.COL_N_SITI + " INTEGER, " +
                    Entry.COL_IMAGE + " TEXT, " +
                    Entry.COL_TIPO + " TEXT, " +
                    Entry.COL_INDIRIZZO_PARTENZA + " TEXT, " +
                    Entry.COL_INDIRIZZO_ARRIVO + " TEXT" +
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;

    public static final String[] KEYS = new String[]{
            "_id",
            "name",
            "autore",
            "comune",
            "lunghezza",
            "nDurata",
            "hDurata",
            "nsiti",
            "image",
            "tipo",
            "indirizzopartenza",
            "indirizzoarrivo"
    };

    public static final String[] COLS = new String[]{
            Entry._ID,
            Entry.COL_NAME,
            Entry.COL_AUTORE,
            Entry.COL_COMUNE,
            Entry.COL_LUNGHEZZA,
            Entry.COL_N_DURATA,
            Entry.COL_H_DURATA,
            Entry.COL_N_SITI,
            Entry.COL_IMAGE,
            Entry.COL_TIPO,
            Entry.COL_INDIRIZZO_PARTENZA,
            Entry.COL_INDIRIZZO_ARRIVO
    };

    public static final String MIME_DIR = DataConstants.VND_ANDROID_CURSOR_DIR_VND + DataConstants.AUTHORITY + ".itinerary";
    public static final String MIME_ITEM = DataConstants.VND_ANDROID_CURSOR_ITEM_VND + DataConstants.AUTHORITY + ".itinerary";

    public static final String PATH = "itinerary";
    public static final Uri URI = Uri.parse("content://" + DataConstants.AUTHORITY + "/" + PATH);

    private ItineraryContract() {
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "itinerary";
        public static final String _ID = "_id";
        public static final String COL_NAME = "itName";
        public static final String COL_AUTORE = "itAutore";
        public static final String COL_COMUNE = "itComune";
        public static final String COL_LUNGHEZZA = "itLunghezza";
        public static final String COL_N_DURATA = "itNDurata";
        public static final String COL_H_DURATA = "itHDurata";
        public static final String COL_N_SITI = "itNSiti";
        public static final String COL_IMAGE = "itImage";
        public static final String COL_TIPO = "itTipo";
        public static final String COL_INDIRIZZO_PARTENZA = "itIndirizzoPartenza";
        public static final String COL_INDIRIZZO_ARRIVO = "itIndirizzoArrivo";
    }

}
