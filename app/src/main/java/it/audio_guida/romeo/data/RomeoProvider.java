package it.audio_guida.romeo.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import it.audio_guida.romeo.data.contracts.*;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public class RomeoProvider extends ContentProvider {

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(DataConstants.AUTHORITY, CityContract.PATH, 1);
        sUriMatcher.addURI(DataConstants.AUTHORITY, CityContract.PATH + "/#", 2);
        sUriMatcher.addURI(DataConstants.AUTHORITY, CityTypeContract.PATH, 3);
        sUriMatcher.addURI(DataConstants.AUTHORITY, CityTypeContract.PATH + "/#", 4);
        sUriMatcher.addURI(DataConstants.AUTHORITY, CardContract.PATH, 5);
        sUriMatcher.addURI(DataConstants.AUTHORITY, CardContract.PATH + "/#", 6);
        sUriMatcher.addURI(DataConstants.AUTHORITY, CategoryContract.PATH, 7);
        sUriMatcher.addURI(DataConstants.AUTHORITY, CategoryContract.PATH + "/#", 8);
        sUriMatcher.addURI(DataConstants.AUTHORITY, ServiceContract.PATH, 9);
        sUriMatcher.addURI(DataConstants.AUTHORITY, ServiceContract.PATH + "/#", 10);

        sUriMatcher.addURI(DataConstants.AUTHORITY, ItineraryContract.PATH, 11);
        sUriMatcher.addURI(DataConstants.AUTHORITY, ItineraryContract.PATH + "/#", 12);
        sUriMatcher.addURI(DataConstants.AUTHORITY, ItineraryNodeContract.PATH, 13);
        sUriMatcher.addURI(DataConstants.AUTHORITY, ItineraryNodeContract.PATH + "/#", 14);

        sUriMatcher.addURI(DataConstants.AUTHORITY, PreferencesContract.PATH, 101);
        sUriMatcher.addURI(DataConstants.AUTHORITY, PreferencesContract.PATH + "/key/*", 103);
        sUriMatcher.addURI(DataConstants.AUTHORITY, PreferencesContract.PATH + "/#", 102);

        sUriMatcher.addURI(DataConstants.AUTHORITY, CategoryUnionServiceViewContract.PATH, 201);
        sUriMatcher.addURI(DataConstants.AUTHORITY, CategoryUnionServiceViewContract.PATH + "/#", 202);
    }

    private RomeoDatabaseHelper mOpenHelper;

    @Override
    public boolean onCreate() {
        mOpenHelper = new RomeoDatabaseHelper(getContext());
        return true;
    }

    public Cursor query(
            @NonNull Uri uri,
            String[] projection,
            String selection,
            String[] selectionArgs,
            String sortOrder) {

        String table;
        switch (sUriMatcher.match(uri)) {
            case 101:
                if (TextUtils.isEmpty(sortOrder)) sortOrder = "_ID ASC";
                table = PreferencesContract.Entry.TABLE_NAME;
                break;

            case 102:
                table = PreferencesContract.Entry.TABLE_NAME;
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            case 103:
                table = PreferencesContract.Entry.TABLE_NAME;
                selection = PreferencesContract.Entry.COL_KEY + " = ?";
                selectionArgs = new String[]{uri.getLastPathSegment()};
                break;

            case 1:
                if (TextUtils.isEmpty(sortOrder)) sortOrder = "_ID ASC";
                table = CityContract.Entry.TABLE_NAME;
                break;

            case 2:
                table = CityContract.Entry.TABLE_NAME;
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            case 3:
                if (TextUtils.isEmpty(sortOrder)) sortOrder = "_ID ASC";
                table = CityTypeContract.Entry.TABLE_NAME;
                break;

            case 4:
                table = CityTypeContract.Entry.TABLE_NAME;
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            case 5:
                if (TextUtils.isEmpty(sortOrder)) sortOrder = "_ID ASC";
                table = CardContract.Entry.TABLE_NAME;
                if (ArrayUtils.contains(projection, CategoryContract.Entry.COL_NAME)) {
                    table += CardContract.JOIN_CATEGORIES;
                }
                if (ArrayUtils.contains(projection, ServiceContract.Entry.COL_NAME)) {
                    table += CardContract.JOIN_SERVICES;
                }
                break;

            case 6:
                table = CardContract.Entry.TABLE_NAME;
                if (ArrayUtils.contains(projection, CategoryContract.Entry.COL_NAME)) {
                    table += CardContract.JOIN_CATEGORIES;
                }
                if (ArrayUtils.contains(projection, ServiceContract.Entry.COL_NAME)) {
                    table += CardContract.JOIN_SERVICES;
                }
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            case 7:
                table = CategoryContract.Entry.TABLE_NAME;
                break;

            case 8:
                table = CategoryContract.Entry.TABLE_NAME;
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            case 9:
                table = ServiceContract.Entry.TABLE_NAME;
                break;

            case 10:
                table = ServiceContract.Entry.TABLE_NAME;
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            case 11:
                table = ItineraryContract.Entry.TABLE_NAME;
                break;

            case 12:
                table = ItineraryContract.Entry.TABLE_NAME;
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            case 13:
                table = ItineraryNodeContract.Entry.TABLE_NAME;
                break;

            case 14:
                table = ItineraryNodeContract.Entry.TABLE_NAME;
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            case 201:
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = CategoryUnionServiceViewContract.Entry.COL_CAT_ID + " ASC, "
                            + CategoryUnionServiceViewContract.Entry.COL_TYPE + " ASC";
                }
                table = CategoryUnionServiceViewContract.Entry.TABLE_NAME;
                break;

            case 202:
                table = CategoryUnionServiceViewContract.Entry.TABLE_NAME;
                selection = selection + "_ID = " + uri.getLastPathSegment();
                break;

            default:
                throw new IllegalArgumentException("Received an invalid content URI");
        }
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(table, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case 101:
                return PreferencesContract.MIME_DIR;
            case 102:
                return PreferencesContract.MIME_ITEM;
            case 103:
                return PreferencesContract.MIME_ITEM;
            case 1:
                return CityContract.MIME_DIR;
            case 2:
                return CityContract.MIME_ITEM;
            case 3:
                return CityTypeContract.MIME_DIR;
            case 4:
                return CityTypeContract.MIME_ITEM;
            case 5:
                return CardContract.MIME_DIR;
            case 6:
                return CardContract.MIME_ITEM;
            case 7:
                return CategoryContract.MIME_DIR;
            case 8:
                return CategoryContract.MIME_ITEM;
            case 9:
                return ServiceContract.MIME_DIR;
            case 10:
                return ServiceContract.MIME_ITEM;
            case 11:
                return ItineraryContract.MIME_DIR;
            case 12:
                return ItineraryContract.MIME_ITEM;
            case 13:
                return ItineraryNodeContract.MIME_DIR;
            case 14:
                return ItineraryNodeContract.MIME_ITEM;
            default:
                throw new IllegalArgumentException("Received an invalid content URI");
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        return insert(uri, values, db);
    }

    private Uri insert(@NonNull Uri uri, ContentValues values, SQLiteDatabase db) {
        if (values.size() == 0) throw new IllegalArgumentException("Received empty ContentValues");

        String table;
        switch (sUriMatcher.match(uri)) {
            case 101:
                table = PreferencesContract.Entry.TABLE_NAME;
                break;

            case 1:
                table = CityContract.Entry.TABLE_NAME;
                break;

            case 3:
                table = CityTypeContract.Entry.TABLE_NAME;
                break;

            case 5:
                table = CardContract.Entry.TABLE_NAME;
                break;

            case 7:
                table = CategoryContract.Entry.TABLE_NAME;
                break;

            case 9:
                table = ServiceContract.Entry.TABLE_NAME;
                break;

            case 11:
                table = ItineraryContract.Entry.TABLE_NAME;
                break;

            case 13:
                table = ItineraryNodeContract.Entry.TABLE_NAME;
                break;

            default:
                throw new IllegalArgumentException("Received an invalid content URI");
        }

        long newRowId;
        newRowId = db.replace(table, null, values);
        if (newRowId == -1) throw new IllegalArgumentException("Cannot insert new row to table '" + table + "'");
        return ContentUris.withAppendedId(uri, newRowId);
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        if(sUriMatcher.match(uri) == 1){
            db.execSQL(CityTypeContract.SQL_DELETE_ENTRIES);
            db.execSQL(CategoryContract.SQL_DELETE_ENTRIES);
            db.execSQL(ServiceContract.SQL_DELETE_ENTRIES);
            db.execSQL(CityContract.SQL_DELETE_ENTRIES);
            db.execSQL(CardContract.SQL_DELETE_ENTRIES);
            db.execSQL(CategoryUnionServiceViewContract.SQL_DELETE_ENTRIES);
            db.execSQL(ItineraryContract.SQL_DELETE_ENTRIES);
            db.execSQL(ItineraryNodeContract.SQL_DELETE_ENTRIES);

            db.execSQL(CityTypeContract.SQL_CREATE_ENTRIES);
            db.execSQL(CategoryContract.SQL_CREATE_ENTRIES);
            db.execSQL(ServiceContract.SQL_CREATE_ENTRIES);
            db.execSQL(CityContract.SQL_CREATE_ENTRIES);
            db.execSQL(CardContract.SQL_CREATE_ENTRIES);
            db.execSQL(CategoryUnionServiceViewContract.SQL_CREATE_ENTRIES);
            db.execSQL(ItineraryContract.SQL_CREATE_ENTRIES);
            db.execSQL(ItineraryNodeContract.SQL_CREATE_ENTRIES);

        }
        else {
            db.execSQL(ItineraryContract.SQL_DELETE_ENTRIES);
            db.execSQL(ItineraryContract.SQL_CREATE_ENTRIES);
            db.execSQL(ItineraryNodeContract.SQL_DELETE_ENTRIES);
            db.execSQL(ItineraryNodeContract.SQL_CREATE_ENTRIES);
        }
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {
            case 1:
                db.execSQL(CityContract.SQL_DELETE_ENTRIES);
                db.execSQL(CityContract.SQL_CREATE_ENTRIES);
                break;

            case 3:
                db.execSQL(CityTypeContract.SQL_DELETE_ENTRIES);
                db.execSQL(CityTypeContract.SQL_CREATE_ENTRIES);
                break;

            case 5:
                db.execSQL(CardContract.SQL_DELETE_ENTRIES);
                db.execSQL(CardContract.SQL_CREATE_ENTRIES);
                break;

            case 11:
                db.execSQL(ItineraryContract.SQL_DELETE_ENTRIES);
                db.execSQL(ItineraryContract.SQL_CREATE_ENTRIES);
                break;

            case 13:
                db.execSQL(ItineraryNodeContract.SQL_DELETE_ENTRIES);
                db.execSQL(ItineraryNodeContract.SQL_CREATE_ENTRIES);
                break;

        }

        int insertCount = 0;
        db.beginTransaction();
        try {
            for (ContentValues value : values) {
                insert(uri, value, db);
                insertCount++;
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return insertCount;
    }
}