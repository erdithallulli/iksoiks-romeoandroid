package it.audio_guida.romeo.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import it.audio_guida.romeo.data.contracts.*;

import java.util.Locale;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public class RomeoDatabaseHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 41;
    public static final String DATABASE_NAME = "_romeo.db";

    public RomeoDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CityTypeContract.SQL_CREATE_ENTRIES);
        db.execSQL(CategoryContract.SQL_CREATE_ENTRIES);
        db.execSQL(ServiceContract.SQL_CREATE_ENTRIES);
        db.execSQL(CityContract.SQL_CREATE_ENTRIES);
        db.execSQL(CardContract.SQL_CREATE_ENTRIES);
        db.execSQL(PreferencesContract.SQL_CREATE_ENTRIES);
        db.execSQL(CategoryUnionServiceViewContract.SQL_CREATE_ENTRIES);

        db.execSQL(ItineraryContract.SQL_CREATE_ENTRIES);
        db.execSQL(ItineraryNodeContract.SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(CityTypeContract.SQL_DELETE_ENTRIES);
        db.execSQL(CategoryContract.SQL_DELETE_ENTRIES);
        db.execSQL(ServiceContract.SQL_DELETE_ENTRIES);
        db.execSQL(CityContract.SQL_DELETE_ENTRIES);
        db.execSQL(CardContract.SQL_DELETE_ENTRIES);
        db.execSQL(PreferencesContract.SQL_DELETE_ENTRIES);
        db.execSQL(CategoryUnionServiceViewContract.SQL_DELETE_ENTRIES);

        db.execSQL(ItineraryContract.SQL_DELETE_ENTRIES);
        db.execSQL(ItineraryNodeContract.SQL_DELETE_ENTRIES);

        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
