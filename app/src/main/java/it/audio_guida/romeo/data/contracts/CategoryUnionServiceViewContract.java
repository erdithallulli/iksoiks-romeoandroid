package it.audio_guida.romeo.data.contracts;

import android.net.Uri;
import android.provider.BaseColumns;
import it.audio_guida.romeo.data.DataConstants;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public final class CategoryUnionServiceViewContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE VIEW " + Entry.TABLE_NAME + " AS SELECT "
                    + ServiceContract.Entry._ID + " " + Entry._ID + ", "
                    + ServiceContract.Entry.COL_CAT_ID + " " + Entry.COL_CAT_ID + ", "
                    + ServiceContract.Entry.COL_NAME + " " + Entry.COL_NAME + ", "
                    + ServiceContract.Entry.COL_ICON + " " + Entry.COL_ICON + ", "
                    + Entry.TYPE_SERVICE + " " + Entry.COL_TYPE
                    + " FROM " + ServiceContract.Entry.TABLE_NAME + " UNION SELECT "
                    + CategoryContract.Entry._ID + ", "
                    + CategoryContract.Entry._ID + ", "
                    + CategoryContract.Entry.COL_NAME + ", "
                    + CategoryContract.Entry.COL_ICON + ", "
                    + Entry.TYPE_CATEGORY
                    + " FROM " + CategoryContract.Entry.TABLE_NAME;

    public static final String SQL_DELETE_ENTRIES =
            "DROP VIEW IF EXISTS " + Entry.TABLE_NAME;

    public static final String MIME_DIR = DataConstants.VND_ANDROID_CURSOR_DIR_VND + DataConstants.AUTHORITY + ".categoryUnionService";
    public static final String MIME_ITEM = DataConstants.VND_ANDROID_CURSOR_ITEM_VND + DataConstants.AUTHORITY + ".categoryUnionService";

    public static final String PATH = "categoryUnionServices";
    public static final Uri URI = Uri.parse("content://" + DataConstants.AUTHORITY + "/" + PATH);

    private CategoryUnionServiceViewContract() {
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "categoryServices";
        public static final String COL_NAME = "name";
        public static final String COL_CAT_ID = "catId";
        public static final String COL_ICON = "icon";
        public static final String COL_TYPE = "type";

        public static final int TYPE_CATEGORY = 1;
        public static final int TYPE_SERVICE = 2;
    }

}
