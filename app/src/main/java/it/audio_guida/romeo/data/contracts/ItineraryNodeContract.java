package it.audio_guida.romeo.data.contracts;

import android.net.Uri;
import android.provider.BaseColumns;

import it.audio_guida.romeo.data.DataConstants;

public class ItineraryNodeContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY, " +
                    Entry.COL_NAME + " TEXT, " +
                    Entry.COL_LAT + " REAL," +
                    Entry.COL_LON + " REAL," +
                    Entry.COL_CAT_ID + " INTEGER," +
                    Entry.COL_SERVICE_ID + " INTEGER," +
                    Entry.COL_SCHEDA_CODE + " INTEGER," +
                    Entry.COL_N_DISTANZA + " INTEGER," +
                    Entry.COL_ID_ITINERARY + " INTEGER," +
                    Entry.COL_H_DURATA + " TEXT" +
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;

    public static final String[] KEYS = new String[]{
            "_id",
            "name",
            "lat",
            "lon",
            "catId",
            "serviceId",
            "schedaCode",
            "ndistanza",
            "hdurata",
            "itinerarioId"
    };

    public static final String[] COLS = new String[]{
            Entry._ID,
            Entry.COL_NAME,
            Entry.COL_LAT,
            Entry.COL_LON,
            Entry.COL_CAT_ID,
            Entry.COL_SERVICE_ID,
            Entry.COL_SCHEDA_CODE,
            Entry.COL_N_DISTANZA,
            Entry.COL_H_DURATA,
            Entry.COL_ID_ITINERARY
    };

    public static final String MIME_DIR = DataConstants.VND_ANDROID_CURSOR_DIR_VND + DataConstants.AUTHORITY + ".itinerarynode";
    public static final String MIME_ITEM = DataConstants.VND_ANDROID_CURSOR_ITEM_VND + DataConstants.AUTHORITY + ".itinerarynode";

    public static final String PATH = "itinerarynode";
    public static final Uri URI = Uri.parse("content://" + DataConstants.AUTHORITY + "/" + PATH);

    private ItineraryNodeContract() {
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "itinerarynode";
        public static final String _ID = "_id";
        public static final String COL_NAME = "itnoName";
        public static final String COL_LAT = "itnoLat";
        public static final String COL_LON = "itnoLon";
        public static final String COL_CAT_ID = "itnoCatId";
        public static final String COL_SERVICE_ID = "itnoServiceId";
        public static final String COL_SCHEDA_CODE = "itnoSchedaCode";
        public static final String COL_N_DISTANZA = "itnoNDistanza";
        public static final String COL_H_DURATA = "itnoHDurata";
        public static final String COL_ID_ITINERARY = "itnoIdItinerary";
    }

}
