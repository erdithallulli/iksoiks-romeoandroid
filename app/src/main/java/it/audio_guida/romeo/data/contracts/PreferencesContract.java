package it.audio_guida.romeo.data.contracts;

import android.net.Uri;
import android.provider.BaseColumns;
import it.audio_guida.romeo.data.DataConstants;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public final class PreferencesContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY, " +
                    Entry.COL_KEY + " TEXT UNIQUE, " +
                    Entry.COL_VALUE + " TEXT" +
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;

    public static final String MIME_DIR = DataConstants.VND_ANDROID_CURSOR_DIR_VND + DataConstants.AUTHORITY + ".preference";
    public static final String MIME_ITEM = DataConstants.VND_ANDROID_CURSOR_ITEM_VND + DataConstants.AUTHORITY + ".preference";

    public static final String PATH = "preferences";
    public static final Uri URI = Uri.parse("content://" + DataConstants.AUTHORITY + "/" + PATH);

    private PreferencesContract() {
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "preferences";
        public static final String COL_KEY = "key";
        public static final String COL_VALUE = "value";
    }

}
