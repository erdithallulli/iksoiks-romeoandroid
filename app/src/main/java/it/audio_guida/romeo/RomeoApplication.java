package it.audio_guida.romeo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Locale;

public class RomeoApplication extends Application {

    private static RomeoApplication mInstance;
    private static Context mAppContext;

    public static RomeoApplication getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return mAppContext;
    }

    public void setAppContext(Context mAppContext) {
        RomeoApplication.mAppContext = mAppContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        this.setAppContext(getApplicationContext());
    }

}