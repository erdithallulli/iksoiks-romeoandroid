package it.audio_guida.romeo.network;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;

import it.audio_guida.romeo.RomeoApplication;
import it.audio_guida.romeo.data.DataConstants;
import it.audio_guida.romeo.data.MyPreferences;
import it.audio_guida.romeo.model.Card;
import it.audio_guida.romeo.model.City;
import it.audio_guida.romeo.model.GetCardTextListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Locale;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public class ServerRequests {

    public static final String BASE_URL = "http://www.audio-guida.it:3333";

    //TODO aggiornare
    public static String getLanguageCode() {
        //return Locale.FRENCH.getISO3Language().toUpperCase();
        String result;


        MyPreferences preferences = new MyPreferences(RomeoApplication.getAppContext());
        result = preferences.getString(DataConstants.PREF_KEY_LAN, Locale.getDefault().getISO3Language().toUpperCase());

        return result;
    }

    public static RequestFuture<JSONArray> citiesList() {
        //String url = "http://195.120.231.180/PalmipedoWSRest/api/v1/comune/ListaComuni?lingua=" + getLanguageCode();
        String url = BASE_URL + "/ws/v1/listaComuni?lang=" + getLanguageCode();
        return getList(url);
    }

    public static RequestFuture<JSONArray> cityTypesList() {
        String url = BASE_URL + "/ws/v1/listaTipiCitta?lang=" + getLanguageCode();
        return getList(url);
    }

    public static RequestFuture<JSONArray> categoriesList() {
        String url = BASE_URL + "/ws/v1/listaCategorie?lang=" + getLanguageCode();
        return getList(url);
    }

    public static RequestFuture<JSONArray> servicesList() {
        String url = BASE_URL + "/ws/v1/listaServizi?lang=" + getLanguageCode();
        return getList(url);
    }


    public static RequestFuture<JSONArray> itinerariesList(String city) {
        String url = BASE_URL + "/ws/v1/listaItinerari?comune=" + Uri.encode(city) + "&lang=";

        String language = languageSchede();
        url += language;
        return getList(url);
    }

    public static RequestFuture<JSONArray> itineraryNodesList(String id){
        String url = BASE_URL + "/ws/v1/estrazioneItinerario?id=" + Uri.encode(id) + "&lang=";

        String language = languageSchede();
        url += language;

        return getList(url);
    }

    public static RequestFuture<JSONArray> cardsList(String city) {
        String url = BASE_URL + "/ws/v1/listaSchede?cat=&comune=" + Uri.encode(city) + "&lang=";

        String language = languageSchede();
        url += language;
        return getList(url);
    }

    private static RequestFuture<JSONArray> getList(String url) {
        RequestFuture<JSONArray> future = RequestFuture.newFuture();
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, future, future);
        request.setRetryPolicy(new DefaultRetryPolicy(20000, 3, 1.1f));

        VolleySingleton.getInstance().getRequestQueue().add(request);
        return future;
    }

    public static void getCardTextAsync(String city, String textFileName, final GetCardTextListener listener) {

        //String language = languageSchede();
        //String url = String.format("http://195.120.231.180/Palmipedo/ArchivioCitta/rieti/Testi/Italiano/RIE-0170037370_26122014_154628.txt");
        String url = String.format("%s/%s/testi/%s/%s", Card.URL_ARCHIVIO_CITTA, Uri.encode(city), ServerRequests.languageSchedeEstese(), Uri.encode(textFileName));


        //String url = String.format("%s/ws/v1/testi/%s/%s/%s", BASE_URL, language, Uri.encode(city), Uri.encode(textFileName));
        Log.i("Testo chiamato", url);
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(ServerRequests.languageSchedeEstese() != "russo") {
                    listener.onCardTextResponse(response);
                    return;
                }
                String newStr = response;
                try {
                    newStr = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                listener.onCardTextResponse(newStr);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onCardTextError(error);
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

    public static RequestFuture<String> getCardTextSync(String city, String textFileName) {
        String language = languageSchede();

        RequestFuture<String> future = RequestFuture.newFuture();
        String url = String.format("%s/ws/v1/testi/%s/%s/%s", BASE_URL, language, Uri.encode(city), Uri.encode(textFileName));
        StringRequest request = new StringRequest(Request.Method.GET, url, future, future);
        VolleySingleton.getInstance().getRequestQueue().add(request);
        return future;
    }

    public static RequestFuture<Integer> saveFile(String remote, File local) {
        RequestFuture<Integer> future = RequestFuture.newFuture();
        SaveFileRequest request = new SaveFileRequest(remote, local, future, future);
        VolleySingleton.getInstance().getRequestQueue().add(request);
        return future;
    }

    public static void anonymousLogin(
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, String liveId, String deviceId, String nomeApp, boolean utenteTrial, String versionePrg, String comune,
            String terminaleTipo, int terminaleFirmware, String linguaSistema, String linguaSelezionata,
            String lastDataOra, String lastLatitudine, String lastLongitudine) {
        String url = String.format("%s/ws/v1/anonymousLogin", BASE_URL);


        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("LiveID", liveId);
            jsonRequest.put("DeviceID", deviceId);
            jsonRequest.put("NomeApp", nomeApp);
            jsonRequest.put("UtenteTrial", utenteTrial);
            jsonRequest.put("VersionePRG", versionePrg);
            jsonRequest.put("Comune", comune);
            jsonRequest.put("TerminaleTipo", terminaleTipo);
            jsonRequest.put("TerminaleFirmware", terminaleFirmware);
            jsonRequest.put("LinguaSistema", linguaSistema);
            jsonRequest.put("LinguaSelezionata", linguaSelezionata);
            jsonRequest.put("LastDataOra", lastDataOra);
            jsonRequest.put("LastLatitudine", lastLatitudine);
            jsonRequest.put("LastLongitudine", lastLongitudine);
        } catch (JSONException ignored) {
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, listener, errorListener);
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

    public static void sendLog(
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, int idUtente,
            String latitudine, String longitudine, String precisione, int codiceCategoria, String comune,
            int codiceServizio, int codiceScheda, String nomeFileMp3, String operazione) {
        String url = String.format("%s/ws/v1/sendLog", BASE_URL);
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("IdUtente", idUtente);
            jsonRequest.put("Latitudine", latitudine);
            jsonRequest.put("Longitudine", longitudine);
            jsonRequest.put("Precisione", precisione);
            jsonRequest.put("Comune", comune);
            jsonRequest.put("CodiceCategoria", codiceCategoria);
            jsonRequest.put("CodiceServizio", codiceServizio);
            jsonRequest.put("CodiceScheda", codiceScheda);
            jsonRequest.put("NomeFileMP3", nomeFileMp3);
            jsonRequest.put("Operazione", operazione);
        } catch (JSONException ignored) {
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, listener, errorListener);
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

    public static String languageSchede(){
        MyPreferences preferences = new MyPreferences(RomeoApplication.getAppContext());
        String result = preferences.getString(DataConstants.PREF_KEY_CITY_LAN, Locale.getDefault().getISO3Language().toUpperCase());
        String[] supportedLanguage = City.getSupportedLanguagesFromEncodedString(result);

        String language = getLanguageCode();
        String ausiliar_lan;
        boolean isPresent = false;
        for(String lan: supportedLanguage) {
            ausiliar_lan = (new Locale(lan.toLowerCase())).getISO3Language().toUpperCase();
            if (ausiliar_lan.equals(language))
                isPresent = true;
        }


        if(!isPresent){
            language = Locale.ENGLISH.getISO3Language().toUpperCase();
            for(String lan: supportedLanguage) {
                ausiliar_lan = (new Locale(lan.toLowerCase())).getISO3Language().toUpperCase();
                if (ausiliar_lan.equals(language))
                    isPresent = true;
            }
        }


        if(!isPresent)
            language = Locale.ITALIAN.getISO3Language().toUpperCase();

        return language;
    }

    public static String languageSchedeEstese(){
        String languageISO3 = ServerRequests.languageSchede();
        String languageEsteso;
        switch(languageISO3){
            case "ITA":
                languageEsteso = "italiano";
                break;
            case "FRA":
                languageEsteso = "francese";
                break;
            case "SPA":
                languageEsteso = "spagnolo";
                break;
            case "DEU":
                languageEsteso = "tedesco";
                break;
            case "RUS":
                languageEsteso = "russo";
                break;
            default:
                languageEsteso = "inglese";
                break;
        }
        return languageEsteso;
    }

    public static void sendPosition(
            Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, int idUtente, int visibile,
            double latitudine, double longitudine) {
        String url = String.format("http://195.120.231.180/PalmipedoWSRest/api/v1/gestione/InviaPosizione");
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("IdUtente", idUtente);
            jsonRequest.put("Visible", visibile);
            jsonRequest.put("Latitudine", latitudine);
            jsonRequest.put("Longitudine", longitudine);
            jsonRequest.put("Precisione", 0);
            jsonRequest.put("LatAgg", 0);
            jsonRequest.put("LonAgg", 0);
        } catch (JSONException ignored) {
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, listener, errorListener);
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

}
