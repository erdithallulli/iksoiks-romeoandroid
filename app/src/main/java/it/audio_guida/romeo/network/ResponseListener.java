package it.audio_guida.romeo.network;

import com.android.volley.VolleyError;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 01/08/16.
 */
public interface ResponseListener<E> {

    void onResponse(int source, E values);

    void onErrorResponse(int source, VolleyError error);
}
