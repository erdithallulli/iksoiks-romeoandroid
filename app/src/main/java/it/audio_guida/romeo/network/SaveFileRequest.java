package it.audio_guida.romeo.network;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Tutti i diritti riservati a Christian Micocci
 * Created by christian on 30/08/16.
 */
class SaveFileRequest extends Request<byte[]> {

    private final Response.Listener<Integer> mListener;
    private final File mLocalFile;

    public SaveFileRequest(String remote, File local, Response.Listener<Integer> listener, Response.ErrorListener errorListener) {
        super(Method.GET, remote, errorListener);
        mListener = listener;
        mLocalFile = local;
    }

    @Override
    protected Response<byte[]> parseNetworkResponse(NetworkResponse response) {
        return Response.success(response.data, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(byte[] response) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(mLocalFile);
            fileOutputStream.write(response);
            fileOutputStream.close();
        } catch (IOException e) {
            getErrorListener().onErrorResponse(new VolleyError(e));
        }
        mListener.onResponse(response.length);
    }
}
