package it.audio_guida.romeo.runtime_info;

import java.util.LinkedList;

public class ItineraryInfo {

    private static boolean itineraryRunning = false;

    public static int id = 121;
    public static String nome = new String("Percorso classico");
    public static String autore = new String("Palmipedo");
    public static String comune = new String("Bologna");
    public static int lunghezzaMetri = 4109;
    public static String durata = new String("01:38:37");
    public static int numeroSitiIncontrati = 38;
    public static String immagine = new String("Itin-121.jpg");
    public static LinkedList<NodoItinerario> tappe;

    public static void setItineraryRunning(boolean itineraryRunning){
        ItineraryInfo.itineraryRunning = itineraryRunning;
    }

    public static boolean isItineraryRunning(){
        return itineraryRunning;
    }

    public static void inserisciTappe(){
        tappe = new LinkedList<NodoItinerario>();
        tappe.add(new NodoItinerario("Palazzo del podestà", 44.4939291032133, 11.3431795045995));
        tappe.add(new NodoItinerario("Piazza maggiore", 44.4935475133217, 11.3430609023359));
        tappe.add(new NodoItinerario("Basilica di San Petronio", 44.4935171465586, 11.3432860747494));
        tappe.add(new NodoItinerario("Bologna", 44.4934827690909, 11.3435009339225));
        tappe.add(new NodoItinerario("Palazzo dei Banchi", 44.4934598507791, 11.3436722483033));
        tappe.add(new NodoItinerario("", 44.4933756259832, 11.3436510488649));
        tappe.add(new NodoItinerario("", 44.4932954118919, 11.3436350060466));
        tappe.add(new NodoItinerario("", 44.4932375431546, 11.3436189632283));
        tappe.add(new NodoItinerario("", 44.493138421456, 11.343589169423));
        tappe.add(new NodoItinerario("", 44.4930616451115, 11.3435731266047));
        tappe.add(new NodoItinerario("", 44.4929774203156, 11.3435490623773));
        tappe.add(new NodoItinerario("", 44.4928954873509, 11.3435278629389));
        tappe.add(new NodoItinerario("", 44.4928187110063, 11.3435032257537));
        tappe.add(new NodoItinerario("", 44.492651980288, 11.34345223251));
        tappe.add(new NodoItinerario("", 44.4925488478848, 11.3434201468734));
        tappe.add(new NodoItinerario("", 44.4924073273094, 11.3433771750388));
        tappe.add(new NodoItinerario("Palazzo Dell'Archiginnasio", 44.4922428884222, 11.343331911373));
        tappe.add(new NodoItinerario("", 44.4921088162982, 11.3432940961585));
        tappe.add(new NodoItinerario("", 44.4920417802361, 11.3432700319311));
        tappe.add(new NodoItinerario("", 44.4919959436125, 11.3432648753109));
        tappe.add(new NodoItinerario("Piazza Galvani", 44.4918985407874, 11.3432327896744));
    }

    public static class NodoItinerario{

        private String nome;
        private double coordinataX;
        private double coordinataY;

        public NodoItinerario(String nome, double coordinataX, double coordinataY){
            this.nome = nome;
            this.coordinataX = coordinataX;
            this.coordinataY = coordinataY;
        }

        public String getNome(){
            return nome;
        }

        public double getCoordinataX(){
            return coordinataX;
        }

        public double getCoordinataY(){
            return coordinataY;
        }
    }

}
