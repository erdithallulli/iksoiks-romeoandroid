package it.audio_guida.romeo.runtime_info;

import android.util.Log;

import java.util.HashSet;

public class RuntimeInfo {
    //non c'è il remove, i comuni visitati vengono memorizzati fino al prossimo accesso all'app
    private HashSet<Integer> codiciComuni;
    private boolean itineraryMode;
    private boolean canceled;
    private String lastCity;

    private static RuntimeInfo ourInstance = null;

    public static RuntimeInfo getInstance() {
        if(ourInstance==null)
            synchronized(RuntimeInfo.class) {
                if( ourInstance == null )
                    ourInstance = new RuntimeInfo();
            }
        return ourInstance;
    }

    private RuntimeInfo() {
        codiciComuni = new HashSet<>();
        itineraryMode = false;
        canceled = true;
        lastCity = null;
    }

    //non serve il lock su codiciComuni perché per come è impostato il programma la lettura e la
    //scrittura non generano conflitti
    public void addComune(int codiceComune) {
        codiciComuni.add(codiceComune);
    }

    public boolean isPresent(int codiceComune){
        return codiciComuni.contains(codiceComune);
    }

    public boolean isItineraryMode(){
        synchronized (this) {
            return itineraryMode;
        }
    }

    public void setItineraryMode(boolean itineraryMode){
        this.itineraryMode = itineraryMode;

    }

    public boolean isCanceled() {
        return canceled;
    }
    public void setCanceled(boolean canceled){
        synchronized (this) {
            this.canceled = canceled;
        }
    }

    public void setLastCity(String lastCity){
        this.lastCity = lastCity;
    }

    public String getLastCity(){
        return lastCity;
    }
}
