// ITourGuideInterface.aidl
package it.audio_guida.romeo;

// Declare any non-default types here with import statements

interface ITourGuideInterface {

    void pauseTourGuide();
    void unpauseTourGuide();
    boolean isVisitingCity();
    String getVisitingCityName();

    boolean isNearACard();
    int getNearCardId();
    String getNearCardName();

    void onFocusAudioPlayer(int cardId, int playerId);
    void onBlurAudioPlayer(int playerId);

    void loadCardAudio(int cardId, boolean autoPlay);
    int getCardAudioLoadedId();
    String getCardAudioLoadedName();

    void playCardAudio();
    void pauseCardAudio();
    void seekCardAudio(int msec);
    int getAudioDurationMSec();
    int getCurrentPositionMSec();
    boolean isPlaying();
    boolean isAudioReady();
}
